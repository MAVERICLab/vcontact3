![vConTACT3 logo](logo.png)

[![Anaconda-Server Badge](https://anaconda.org/bioconda/vcontact3/badges/version.svg)](https://anaconda.org/bioconda/vcontact3)
[![Anaconda-Server Badge](https://anaconda.org/bioconda/vcontact3/badges/latest_release_date.svg)](https://anaconda.org/bioconda/vcontact3)
[![Anaconda-Server Badge](https://anaconda.org/bioconda/vcontact3/badges/platforms.svg)](https://anaconda.org/bioconda/vcontact3)
[![Anaconda-Server Badge](https://anaconda.org/bioconda/vcontact3/badges/license.svg)](https://anaconda.org/bioconda/vcontact3)
[![Anaconda-Server Badge](https://anaconda.org/bioconda/vcontact3/badges/downloads.svg)](https://anaconda.org/bioconda/vcontact3)

Welcome to the vConTACT3 documentation!

THIS TOOL IS CURRENTLY IN BETA AND BEING ACTIVELY DEVELOPED. NO GUARANTEES CAN BE MADE REGARDING INSTALLATION OR 
RESULTS.

(If you are using this tool and find issues, please report them. If you are using this tool and would like to publish 
results, please take note of the version... and if you could consult with the authors, that would be great! This 
project is being actively worked on and updates are released *nearly* every day (well, as much as possible). We're 
striving for a useful tool that can be used by the virus community.)

# vConTACT3

vConTACT3 is the latest iteration in the vConTACT family. Each version improves upon the foundational philosophy: more 
related virus genomes should share more genes - **V**iral **CONT**ig **A**utomatic **C**lustering and **T**axonomy.

vConTACT3 is faster, more scalable, more accurate, and features more outputs than prior versions. Databases are now handled
more efficiently and are more portable than before. 

## Installation

There are several ways to install vConTACT3. There are also numerous required dependencies. Please check out 
[setup.py](setup.py) or the [requirements.txt](requirements.txt) file for a complete listing.

In addition to these python packages, [MMSeqs2](https://github.com/soedinglab/MMseqs2) is also required, but should only be needed if a pip-only install was used.

### Easiest (Bioconda via Mamba/Anaconda)

!!! Now available !!!

The easiest way to install vConTACT3 is using the Anaconda-based package handlers of [Mamba](https://github.com/conda-forge/miniforge) or 
[Anaconda](https://docs.anaconda.com/free/anaconda/install/index.html)

```bash
mamba install -c bioconda vcontact3
```

There is a known [issue](https://github.com/etetoolkit/ete/issues/354) when installing ete3 through conda. Unfortunately, 
the bug persists on some systems (none of which I have access to). So, if you want to export dendrogram figures, 
you will need to install PyQt5 manually as well. This *does not* affect Newick-formatted dendrograms exports.

The other methods appear to be working.

```bash
python -m pip install PyQt5
```

### Using Pip

For this, you need to install python and pip, both of which you will have after installing Mamba or Conda (see above)

```bash
git clone https://bitbucket.org/MAVERICLab/vcontact3.git
cd vcontact3
python -m pip install .
```

Additionally, since MMSeqs2 is not available on PyPi, you need to install it for your system. Please follow their 
[instructions](https://github.com/soedinglab/MMseqs2#installation). In the case of Apple Silicon (M1 and M2), you will 
need to install their OSX-universal binaries and then move that to a location in your $PATH.

### Using the requirements file

```bash
git clone https://bitbucket.org/MAVERICLab/vcontact3.git
cd vcontact
mamba install -c bioconda --file requirements.txt
python -m pip install --no-deps .
```

These installs were verified to work on Linux x86_64. They are expected to work on Apple Intel Macs. For Apple Silicon,
you may need to emulate a terminal with Rosetta support or build [failed] packages using "pip git+" and installing from 
source.

## Database Setup

[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.10035619.svg)](https://doi.org/10.5281/zenodo.10035619)

[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.10935513.svg)](https://doi.org/10.5281/zenodo.10935513)

vConTACT3 databases are now available through Zenodo. vConTACT3 can list and retrieve the latest versions.

```bash
vcontact3 prepare_databases --list-versions
```

To download a specific version from the list:

```bash
vcontact3 prepare_databases --get-version "latest"
```

By default, the latest version of the database is downloaded. You can select any available version from the 
"--list-versions" command.

Additionally, you can specify the download location:

```bash
vcontact3 prepare_databases --get-version "latest" --set-location /path/to/download/location
```

When vConTACT3 downloads the database, it will download it as a {version}.tar.gz file and then decompress it into the 
specified directory. *If no download location is specified*, it will download into the current directory.

Database downloads are structured as follows:

```bash
v220.tar.gz
v220/
220.json
```

The "220.json" file contains the relative paths to the necessary files within the v220 directory. The *goal* of this 
organization is to allow users to specify the same download location with every new version. vConTACT3 is smart enough 
to identify the latest versions from the downloaded files. For example:

```bash
vcontact3 prepare_databases --get-version "220" --set-location /path/to/download/location
vcontact3 prepare_databases --get-version "221" --set-location /path/to/download/location
vcontact3 prepare_databases --get-version "222" --set-location /path/to/download/location
vcontact3 prepare_databases --get-version "223" --set-location /path/to/download/location
```

The folder (/path/to/download/location) will look like:

```bash
v220.tar.gz
v221.tar.gz
v222.tar.gz
v223.tar.gz
v220/
v221/
v222/
v223/
220.json
221.json
222.json
223.json
```

**In the next section**, you can specify the folder where you downloaded the database. vCONTACT3 will select the latest 
database, unless you specify the version.

## Running vConTACT3

### Inputs

vConTACT3 now handles all aspects of input file processing, so no need to create "Gene-to-Genome mapping files." All that 
is required is a single FASTA-formatted nucleotide file.

(During the beta release, user input of gene-to-genome and protein files is unstable and not supported. However, v3 is very
robust regarding restarts, so these files (among others) will be identified and continued with.)

```bash
vcontact3 run --nucleotide virus_genomes.fasta --output output_directory
```

### Database

As mentioned above, users can specify which version of the database they wish to use, as well as the path they downloaded 
the database to. Also, users need to specify which domain of life (more below) they wish to use during classification.

```bash
vcontact3 run --db-domain "prokaryotes" --db-version 220 --db-path /path/to/download/location
```

The above will classify based on "prokaryotes", and use db version 220 available at the specified location.

### Other options

vConTACT3 now handles most of the parameterization internally, but does expose a few options in case users 
wish to customize their runs.

*--min-shared-genes*: will set the minimum number of shared genes to use in the underlying shared gene network. This 
probably shouldn't be changed unless a lot of incorrect connections are being made between genomes. The trade-off is a 
balance between distant (or small) genomes being connected vs unrelated genomes being taxonomically classified as related. 

*--distance-metric*: will change the distance metric used in the gene sharing network. This can have drastic differences 
depending on the type of genomes used as input.

## Example Run

```bash
vcontact3 run --nucleotide VIRSorter_cat-1.fasta --output output_directory --db-domain "prokaryotes" --db-version 220 --db-path /path/to/download/location
```

## Results and Outputs

vConTACT3 generates numerous outputs, and more depending on exports specified via "--exports".

### With all runs

**final_assignments.csv**. A table containing the predictions (and references, where identified) of the input genomes.
Headers are the following: GenomeName, RefSeqID, Proteins, References, Size. Then realm, phylum, class, order, family, subfamily and genus for both Reference (if available) and prediction. 

**genome_support.csv**. (Alpha and beta versions only. *Will be removed upon full release.*) An "internally"-used file 
that keeps track of clusters within a realm + rank + adjustments. The values - while having meaning during assignments - 
should not be used by humans to evaluate "support" of a particular prediction.

**performance_metrics.csv**. A table containing performance metrics (naming skills are top-notch) that uses reference 
data as an internal calibration. Each of the 5 virus realms, along with their 6 ranks, has their vConTACT3-predicted 
rank/groups compared against the references within the provided database. The major metrics are:

* Clustering-wise PPV
* Clustering-wise Sensitivity
* Clustering-wise Accuracy
* Clustering-wise Separation
* Complex-wise Separation
* Separation

Additionally, two network/clustering based approaches are included.

* Adjusted Rand Index (ARI)
* Normalized Mutual Info Score (NMI)

And finally, "Compo Score."

### If "d3js" specified

UNDER ACTIVE DEVELOPMENT

These are network/graph files suitable for rendering and interaction in a browser.

**graph.html & d3_graph.json**: The "main" d3js HTML file with the underlying D3 data. This file can be exceedingly large
(for a browser rendering graphics) and be less responsive, given the amount of memory available on the system.

**graph.bin_#.html & graph.bin_#.d3.json**: Network components binned, roughly trying to keep equal numbers of genome/nodes 
in each file. This rarely happens, as the large Duplornaviria connects large swaths of genome sequence space. These files 
are subsets of the main d3js network and are likely to be easier to navigate and interact with.

More forthcoming...

### If "tree" specified

Newick-formatted trees at 30% clustering identity, 1 per connected component, grouped by realm. Depending on the scale 
of your dataset, NUMEROUS trees could be created.

More forthcoming...

### If "cytoscape" specified

vConTACT3 can write files suitable for [Cytoscape](https://cytoscape.org/). Technically, they're just json files, but 
they're in a format that Cytoscape expects.

To load/use these files, open Cytoscape and go to "File --> Import --> Network from file..."

It may ask if you'd like to create a view for your network. You can select Yes or No, it doesn't matter. If you select 
"No" then the network view will be empty. If you select "Yes", there will be a large blue rectangle. As long as the 
"network selected" window on the left panel says some number >5000 and next to it, a number >450,000, the full network 
has been loaded. (If you've loaded a partial network, just ensure that there are numbers there)

Afterward, go to "Layout --> Apply Preferred Layout". Alternatively, use any layout that seems suitable. The network 
layout might take a while to "layout". After it finishes, there should be a large network along with many smaller 
networks adjacent to it in the network view window.

*Layouts are not provided as they can require exceedingly long times to calculate positions.*

### Checkpoint files

Checkpoint files are generated during the course of every run. They are comprised of file paths, parameters, and other 
data processing components that are required between the last checkpoint file and then next. The presence of a 
checkpoint file, generally speaking, indicates that the run has completed successfully up to that point. Failure "downstream" 
of that checkpoint file will allow subsequent runs to restart from that point.

 * *.resolver.pkl.gz

 * *.profile.pkl.gz

 * *.performance.pkl.gz

 * *.gbga.pkl.gz

 * *.exports.pkl.gz

There is one caveat to checkpoint files. Since they often contain most (if not all) of the information/data required to 
move to the next step, any errors or issues that survived to the checkpoint *will be propagated* to the rest of the processing.

For example, if a genome is mis-named or duplicated within the dataset, vConTACT3 may not identify it. The genes, while technically 
being from two different genomes, will be connected via the duplicated name. If the user notices this issue and delete the 
last checkpoint file, it won't matter, because all the checkpoints will have incorporated the same issue. 


## FAQs and Known Issues

* **ImportError: cannot import name 'TreeStyle'**. This is a known issue with the ETE toolkit installed through Conda/Anaconda/Miniconda/etc. To fix, try the following:

```bash
python -m pip install PyQt5
```

* **'/usr/bin/clang' failed with exit code 1**: This is sorta generic, and there could be a lot of reasons. For THIS software specifically, it can occur if you try to install the packages (via pip) on Apple Silicon (M1 and m2). It might be for scikit-bio or numba or pandas. You can try the following:

```bash
pip3 install --upgrade pip
python3 -m pip install --upgrade setuptools
```

* **Re-running my dataset from scratch yielded slightly different results than resuming a previous run using the same dataset.** This
is the result of the non-deterministic nature of MMSeqs2 clustering. Everything "post"-clustering (i.e. after the profiles are generated) 
will result in identical results. Of course, though the absolute "value" of the taxonomy will be different, meaning the name and/or number 
of the novel group, the comparison will be symmetrical. So, genomes A, B and C might be "novel_X" in one run, but "novel_Y" in another.

## Major Updates

* [2024-12-30] Apply default realm to final predictions. Was used and calculated, but ignored while writing the final file.
* [2024-11-02] Addition of *Monodnaviria* for eukaryotes
* [2024-10-31] Addition of *Monodnaviria* for prokaryotes

## Future Work

These are planned updates and/or features. **There is no guarantee that these will be implemented, nor should be considered expectation of**, 
but *are* actively being worked on.

* Better newick export handling
* More "useful" interactive network/graph
* <del>Faster performance... continued</del>
* Improved memory handling
* <del>Reducing verbosity from developer-centric to public facing (i.e. less info, more debug)</del>
* Improving documentation for outputs and expected results
* Improving documentation for the extensive parameters associated with running the tool
* <del>Bioconda integration</del>

## Citation

If you find vConTACT3 useful, please cite it. At the moment, please cite this GitHub repository. 

## Other acknowledgements

vConTACT3 wouldn't be at its current state without feedback from the community. Below is a list of those who helped 
contribute via email and/or in-person suggestions/discussions:

* Placeholder, but coming soon!

Thank you!

# Funding acknowledgements

vConTACT3 wouldn't even exist without funding to support both its development and its developers' time. Stay tuned for 
official funding notices!
