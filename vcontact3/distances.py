#!/usr/bin/env python
import numpy as np

# Manually verified optimal distances to achieve maximum accuracy + NMI
distances = {
    'archaea': {
        'Monodnaviria': {
            'realm': 0,
            'kingdom': 0,
            'phylum': 0,
            'class': 0,
            'order': {
                'cluster identity': 0.3,
                'PairwiseDistanceCutoff': 0.99
            },
            'family': {
                'cluster identity': 0.3,
                'PairwiseDistanceCutoff': 0.91
            },
            'subfamily': {
                'cluster identity': 0.5,
                'PairwiseDistanceCutoff': 0.93
            },
            'genus': {
                'cluster identity': 0.7,
                'PairwiseDistanceCutoff': 0.55
            },
        },
        'Riboviria': {
            'realm': 0,
            'kingdom': 0,
            'phylum': 0,
            'class': 0,
            'order': 0,
            'family': 0,
            'subfamily': 0,
            'genus': 0,
        },
        'Ribozyviria': {
            'realm': 0,
            'kingdom': 0,
            'phylum': 0,
            'class': 0,
            'order': 0,
            'family': 0,
            'subfamily': 0,
            'genus': 0,
        },
        'Duplodnaviria': {
            'realm': 0,
            'kingdom': 0,
            'phylum': 0,
            'class': 0,
            'order': {
                # Only ONE set of identity + min shared genes (all metrics but Jaccard)
                'cluster identity': 0.3,
                'PairwiseDistanceCutoff': 0.99  # 0.944 Acc, 0.8938 ARI, 0.881 NMI @ 1 gene, NOTHING close (-6% drop)
                # 0.98 ONLY shorter for 0.8807 Acc, 0.851 ARI, 0.807 NMI @ 1 gene
                # 0.99, 0.98 and 0.97 varying genes and metrics, but Acc plumments to 0.8
            },
            'family': {
                # Selected over 0.92 @ 4 gene and sqroot/virclust as -0.1/0.2 in ARI/NMI and only +0.01 in Acc
                # Shorter is not found
                # 0.96, 0.98 for 0.9735 Acc, 0.9748 ARI, 0.9585 NMI @ 1-2 genes
                # 0.91 for 0.9732 Acc, 0.964 ARI, 0.934 NMI @ 1-3 genes
                # Acc effectively remains ~0.97 until 0.90, then 0.968 until 0.88
                'cluster identity': 0.3,
                'PairwiseDistanceCutoff': 0.91  # Update from 0.96 to more cleanly distinguish families
            },
            'subfamily': {
                # Rare instance where Jaccard is present. 0.85 for 0.964 Acc, 0.996 ARI, 0.972 NMI @ 1-3 genes
                # 0.74 for 0.964 Acc, 0.996 ARI, 0.972 NMI (0.0001 diff) @ 1-3 genes
                # 0.68 - 0.71 (w/ 0.71-0.72 sprinkled in) for Acc 0.963-0.963, 0.995-0.996 ARI, 0.969-0.971 NMI
                # 'cluster identity': 0.3,
                'cluster identity': 0.5,
                # 'PairwiseDistanceCutoff': 0.74
                'PairwiseDistanceCutoff': 0.92
                # For 0.5 identity, 0.971 Acc, 0.996 ARI, 0.975 NMI @ 1-4 gene
            },
            'genus': {
                # Except for Jaccard for 0.50-0.53... 0.33-0.34 for Acc 0.9666-0.9667, 0.9979 ARI, 0.962-0.965 NMI
                # Above @ 30%, but consistently high is the same @ 40%, but 0.34-0.35
                # Shorter is superior at all levels! (but only ~+0.001 Acc)
                # 'cluster identity': 0.3,
                'cluster identity': 0.7,
                # 'PairwiseDistanceCutoff': 0.33
                'PairwiseDistanceCutoff': 0.55
                # However, 0.7 identity @ 1-5 genes for 0.55 is 0.978 Acc, 0.9991 ARI, 0.981 NMI
            },
        },
        # Adnaviria and Varidnaviria cannot be separated using minimum shared genes and thus cannot be distinguished
        'Adnaviria': {
            'realm': 0,
            'kingdom': 0,
            'phylum': 0,
            'class': 0,
            'order': {
                'cluster identity': 0.3,
                'PairwiseDistanceCutoff': 0.99  # ONLY @ 1 gene. 0.904 Acc, 0.901 ARI, 0.799 NMI
            },
            'family': {
                'cluster identity': 0.3,
                # 0.955 Acc, 0.968 ARI, 0.932 NMI
                'PairwiseDistanceCutoff': 0.89
                # 0.90, 0.91, 0.92 (=), 0.93 (-1), 0.94 (+0), 0.95 (2), 0.96 (3), 0.97 (3), 0.98 (3)
            },
            'subfamily': {
                'cluster identity': np.nan,
                'PairwiseDistanceCutoff': np.nan
            },
            'genus': {
                'cluster identity': 0.3,
                # 0.965 Acc, 0.9223 ARI, 0.894 NMI @ 0.86, but may be too close to family
                'PairwiseDistanceCutoff': 0.71  # Formerly 0.86 --> selected to avoid family
                # 0.67 - 0.70 ONLY shorter @ 1-3 genes. 0.71 - 0.86 all. 0.95986 Acc, 0.9269 ARI, 0.894 NMI
            },
        },
        'Varidnaviria': {
            'realm': 0,
            'kingdom': 0,
            'phylum': 0,
            'class': 0,
            'order': {
                'cluster identity': 0.3,
                # 'PairwiseDistanceCutoff': 0.95  # Why 0.95?
                'PairwiseDistanceCutoff': 0.99  # ONLY @ 1 gene. 0.904 Acc, 0.901 ARI, 0.799 NMI
            },
            'family': {
                'cluster identity': 0.3,
                # 0.955 Acc, 0.968 ARI, 0.932 NMI
                'PairwiseDistanceCutoff': 0.89
                # 0.90, 0.91, 0.92 (=), 0.93 (-1), 0.94 (+0), 0.95 (2), 0.96 (3), 0.97 (3), 0.98 (3)
            },
            'subfamily': {
                'cluster identity': np.nan,
                'PairwiseDistanceCutoff': np.nan
            },
            'genus': {
                'cluster identity': 0.3,
                # 0.965 Acc, 0.9223 ARI, 0.894 NMI @ 0.86, but may be too close to family
                'PairwiseDistanceCutoff': 0.71  # Formerly 0.86 --> selected to avoid family
                # 0.67 - 0.70 ONLY shorter @ 1-3 genes. 0.71 - 0.86 all. 0.95986 Acc, 0.9269 ARI, 0.894 NMI
            },
        }
    },
    'bacteria': {
        'Adnaviria|Varidnaviria': {
            'realm': 0,
            'kingdom': 0,
            'phylum': 0,
            'class': 0,
            'order': {
                'cluster identity': 0.3,
                'PairwiseDistanceCutoff': 0.99
            },
            'family': {
                'cluster identity': 0.3,
                'PairwiseDistanceCutoff': 0.89
            },
            'subfamily': {
                'cluster identity': np.nan,
                'PairwiseDistanceCutoff': np.nan
            },
            'genus': {
                'cluster identity': 0.3,
                'PairwiseDistanceCutoff': 0.71
            },
        },
        'Monodnaviria': {
            'realm': 0,
            'kingdom': 0,
            'phylum': 0,
            'class': 0,
            'order': {
                # Nothing >95% ACC, only ONE @0.3, 1 min shared, 0.99 => 93.4% ACC, then next plummets to 88.5%
                # Acc decreased: 1 min shared for 0.99 to 93.4%, 2 min shared for 0.99 to 88.2%, 3 min to 85.5%
                'cluster identity': 0.3,  # No other option as 85% Acc is max
                'PairwiseDistanceCutoff': 0.99  # Only real option at ANY level
            },
            'family': {
                # Only @0.3-0.4 >95% ACC (0.3 top "half" of ranks, 0.4 bottom half)
                # @0.3, any min shared (1-5), 0.88-0.92 96.4-96.9% ACC (max!) w/ peak 0.91,0.92,0.90
                # SqRoot > VirClust >> Shorter
                'cluster identity': 0.3,
                'PairwiseDistanceCutoff': 0.91
            },
            'subfamily': {
                # @0.6-0.7, no peak, only increase. @0.5, slight peak >0.92
                # Only @0.4, 0.5 viable
                'cluster identity': 0.5,
                'PairwiseDistanceCutoff': 0.93
            },
            'genus': {
                # 0.7 >= 0.6 > 0.5, but w/in 0.5% ACC, ARIs all 0.999+
                # 97.4 -> 97.7 @ 0.7, ~97.4 @ 0.6 w/ short lower,
                # 0.3 looks most stable for all ranks, but all other ranks suffer as --> 0.7
                # Shorter > SqRoot > VirClust
                # ACC slope peaks ~0.3 @ 0.3, but gently shifts towards 0.55 @ 0.7
                # Overall, >97% ACC, 0.999 ARI w/ 1-5 min shared, @0.7+0.6, metrics, at +/-0.075 cutoff, <97% is 0.3-0.5
                'cluster identity': 0.7,
                'PairwiseDistanceCutoff': 0.55  # At any min shared, but 3 is 2nd overall
            },
        },
        'Riboviria': {
            # Jaccard peaks around same as others (except genus, where it's straight bad), but lags heavily below until
            # approaching peak
            'realm': 0,
            'kingdom': 0,
            'phylum': 0,
            'class': 0,
            'order': 0,
            # None >95% Acc
            'family': {
                # 96.1-96.9% Acc @ 0.3 min shared 1-3 (few/no 4,5), w/ @0.4 seen 96%-93.3%, PPVs perfect
                # @0.3 > @0.4 >> @0.5, w/ peak 0.91, from 0.88-0.94
                'cluster identity': 0.3,
                'PairwiseDistanceCutoff': 0.91
            },
            'subfamily': {
                # @0.5 > @0.4 >> @0.3, min shared 1-3 (more 4,5), metrics equiv
                # @0.6-0.7 no real peak, only approach, @0.5 w/ peak, smaller, w/ @0.4,0.3 broadening & lower cutoff
                # Mostly 97.1-97.2% Acc w/ peak 0.92, from 0.92-0.96
                # Min shared 4-5 (nearly identical) always behind 1-3 (which are ~equivalent)
                'cluster identity': 0.5,  # 97.2%, but could also 0.4 (97.0%), but 0.3 overall lower Acc (96.4%)
                'PairwiseDistanceCutoff': 0.93
            },
            'genus': {
                # Shorter & Jaccard > SqRoot & VirClust
                # Jaccard is extremely shifted 1:1 vs other metrics, w/ 0.55-0.58 all others, Jaccard = 0.71
                # 97.4-97.7% Acc @0.7, 97.4% Acc (1st highest block, then 97.2%) @ 0.6
                # @0.7 & @0.6 only viable
                'cluster identity': 0.7,
                'PairwiseDistanceCutoff': 0.58
            },
        },
        'Ribozyviria': {
            'realm': 0,
            'kingdom': 0,
            'phylum': 0,
            'class': 0,
            'order': 0,
            'family': 0,
            'subfamily': 0,
            'genus': 0,
        },
        'Duplodnaviria': {
            'realm': 0,
            'kingdom': 0,
            'phylum': 0,
            'class': 0,
            'order': {
                # Only ONE set of identity + min shared genes (all metrics but Jaccard)
                'cluster identity': 0.3,
                'PairwiseDistanceCutoff': 0.99  # 0.944 Acc, 0.8938 ARI, 0.881 NMI @ 1 gene, NOTHING close (-6% drop)
                # 0.98 ONLY shorter for 0.8807 Acc, 0.851 ARI, 0.807 NMI @ 1 gene
                # 0.99, 0.98 and 0.97 varying genes and metrics, but Acc plumments to 0.8
            },
            'family': {
                # Selected over 0.92 @ 4 gene and sqroot/virclust as -0.1/0.2 in ARI/NMI and only +0.01 in Acc
                # Shorter is not found
                # 0.96, 0.98 for 0.9735 Acc, 0.9748 ARI, 0.9585 NMI @ 1-2 genes
                # 0.91 for 0.9732 Acc, 0.964 ARI, 0.934 NMI @ 1-3 genes
                # Acc effectively remains ~0.97 until 0.90, then 0.968 until 0.88
                'cluster identity': 0.3,
                'PairwiseDistanceCutoff': 0.91  # Update from 0.96 to more cleanly distinguish families
            },
            'subfamily': {
                # Rare instance where Jaccard is present. 0.85 for 0.964 Acc, 0.996 ARI, 0.972 NMI @ 1-3 genes
                # 0.74 for 0.964 Acc, 0.996 ARI, 0.972 NMI (0.0001 diff) @ 1-3 genes
                # 0.68 - 0.71 (w/ 0.71-0.72 sprinkled in) for Acc 0.963-0.963, 0.995-0.996 ARI, 0.969-0.971 NMI
                # 'cluster identity': 0.3,
                'cluster identity': 0.5,
                # 'PairwiseDistanceCutoff': 0.74
                'PairwiseDistanceCutoff': 0.92
                # For 0.5 identity, 0.971 Acc, 0.996 ARI, 0.975 NMI @ 1-4 gene
            },
            'genus': {
                # Except for Jaccard for 0.50-0.53... 0.33-0.34 for Acc 0.9666-0.9667, 0.9979 ARI, 0.962-0.965 NMI
                # Above @ 30%, but consistently high is the same @ 40%, but 0.34-0.35
                # Shorter is superior at all levels! (but only ~+0.001 Acc)
                # 'cluster identity': 0.3,
                'cluster identity': 0.7,
                # 'PairwiseDistanceCutoff': 0.33
                'PairwiseDistanceCutoff': 0.55
                # However, 0.7 identity @ 1-5 genes for 0.55 is 0.978 Acc, 0.9991 ARI, 0.981 NMI
            },
        },
        # Adnaviria and Varidnaviria cannot be separated using minimum shared genes and thus cannot be distinguished
        'Adnaviria': {
            'realm': 0,
            'kingdom': 0,
            'phylum': 0,
            'class': 0,
            'order': {
                'cluster identity': 0.3,
                # 'PairwiseDistanceCutoff': 0.95  # Why 0.95?
                'PairwiseDistanceCutoff': 0.99  # ONLY @ 1 gene. 0.904 Acc, 0.901 ARI, 0.799 NMI
            },
            'family': {
                'cluster identity': 0.3,
                # 0.955 Acc, 0.968 ARI, 0.932 NMI
                'PairwiseDistanceCutoff': 0.89
                # 0.90, 0.91, 0.92 (=), 0.93 (-1), 0.94 (+0), 0.95 (2), 0.96 (3), 0.97 (3), 0.98 (3)
            },
            'subfamily': {
                'cluster identity': np.nan,
                'PairwiseDistanceCutoff': np.nan
            },
            'genus': {
                'cluster identity': 0.3,
                # 0.965 Acc, 0.9223 ARI, 0.894 NMI @ 0.86, but may be too close to family
                'PairwiseDistanceCutoff': 0.71  # Formerly 0.86 --> selected to avoid family
                # 0.67 - 0.70 ONLY shorter @ 1-3 genes. 0.71 - 0.86 all. 0.95986 Acc, 0.9269 ARI, 0.894 NMI
            },
        },
        'Varidnaviria': {
            'realm': 0,
            'kingdom': 0,
            'phylum': 0,
            'class': 0,
            'order': {
                'cluster identity': 0.3,
                # 'PairwiseDistanceCutoff': 0.95  # Why 0.95?
                'PairwiseDistanceCutoff': 0.99  # ONLY @ 1 gene. 0.904 Acc, 0.901 ARI, 0.799 NMI
            },
            'family': {
                'cluster identity': 0.3,
                # 0.955 Acc, 0.968 ARI, 0.932 NMI
                'PairwiseDistanceCutoff': 0.89
                # 0.90, 0.91, 0.92 (=), 0.93 (-1), 0.94 (+0), 0.95 (2), 0.96 (3), 0.97 (3), 0.98 (3)
            },
            'subfamily': {
                'cluster identity': np.nan,
                'PairwiseDistanceCutoff': np.nan
            },
            'genus': {
                'cluster identity': 0.3,
                # 0.965 Acc, 0.9223 ARI, 0.894 NMI @ 0.86, but may be too close to family
                'PairwiseDistanceCutoff': 0.71  # Formerly 0.86 --> selected to avoid family
                # 0.67 - 0.70 ONLY shorter @ 1-3 genes. 0.71 - 0.86 all. 0.95986 Acc, 0.9269 ARI, 0.894 NMI
            },
        }
    },
    'eukaryota': {
        'Monodnaviria': {
            'realm': 0,
            'kingdom': 0,
            'phylum': 0,
            'class': 0,
            'order': {
                # Only 0.3 viable >90%, ~96.5% at 0.99, 94.8% at 0.98, ~91-92% at 0.97, SqRoot=VirClust~>Shorter
                'cluster identity': 0.3,
                'PairwiseDistanceCutoff': 0.99
            },
            'family': {
                # No family >95% Acc, only @0.3>90% (except shorter 0.99 @0.4)
                # 94.7% Acc 0.98, 94.5% 0.99, 93.3% 0.97, 91.7% 0.96
                'cluster identity': 0.3,
                'PairwiseDistanceCutoff': 0.98
            },
            'subfamily': {
                # Highest Acc of all ranks, any metric, 1-5 min shared, @0.3 all 99.2% Acc (@0.4 99.0%)
                'cluster identity': 0.3,
                'PairwiseDistanceCutoff': 0.94  # 0.93-0.97 (-0.96?) all 99.2%
            },
            'genus': {
                # Shorter > others (=), 0.6 > 0.5 ~ 0.4 (w/ shorter superior, i.e. short @ 0.5 = non-short @ 0.6)
                # @0.6 96.7-97.0% Acc, 1-3 min shared, any metric, 0.98-0.99
                # 96.5+% @0.5s, 0.98 (0.80 with shorter!), 96.4+% @ 0.4s
                # Basically, slight plateau @ 0.3, broadening towards @0.5, narrowing @0.6, just a peak @0.7
                # @0.4-0.5 only "real" maximum
                'cluster identity': 0.4,
                'PairwiseDistanceCutoff': 0.74  # Since lots of plateaus w/ no clear max, selected middlepoint
            },
        },
        'Riboviria': {
            # TODO, evaluate table
            'realm': 0,
            'kingdom': 0,
            'phylum': 0,
            'class': 0,
            'order': 0,
            'family': 0,
            'subfamily': 0,
            'genus': 0,
        },
        'Ribozyviria': {
            'realm': 0,
            'kingdom': 0,
            'phylum': 0,
            'class': 0,
            'order': 0,
            'family': 0,
            'subfamily': 0,
            'genus': 0,
        },
        'Duplodnaviria': {
            'realm': 0,
            'kingdom': 0,
            'phylum': 0,
            'class': 0,
            'order': {
                'cluster identity': 0.3,
                # 0.97-0.99 virclust and sqroot, 1-3 genes (shorter @ +0.96) 0.9387 Acc, 0.7776 ARI, NMI 0
                # That's ALL (i.e. 27?) that exists > 0.9 Acc
                'PairwiseDistanceCutoff': 0.99
            },
            'family': {
                'cluster identity': 0.3,
                # 0.99 shorter 1 gene, perfect (1.0, 1.0, 1.0)
                # 0.99 others 1 gene, 0.9309 Acc, 0.995 ARI, 0.926 NMI
                # That's ALL (only 4?! (shorter @ 2 gene also same as above))
                'PairwiseDistanceCutoff': 0.99  # 0.97-0.98 drops to 0.894 Acc (-0.4), 0.993 ARI, 0.885 NMI
            },
            'subfamily': {
                'cluster identity': 0.3,
                # 0.87 - 0.96 @ 0.3 virclust/sqroot, (shorter starts @ 0.84, ends 0.94), jaccard 0.93 - 0.97 @ 1-3 genes
                # all are perfect
                # 0.97-0.99 @ 0.4 sqroot, shorter, virclust @ 1 gene are perfect
                # 0.83 - 0.0.86 @ 0.3 1 gene 0.978 Acc, 0.984 ARI, 0.9558 NMI
                'PairwiseDistanceCutoff': 0.87
            },
            'genus': {
                # Another rare instance of shorter superior
                # 0.80 for 0.4 shorter 1-3 gene, 0.9567 Acc, 0.8981 ARI, 0.8738 NMI
                # 0.81 as above, 0.9538 Acc, 0.8896 ARI, 0.8580 NMI
                # 0.74 virclust/sqroot @ 1-3 genes | 0.85 jaccard @ 1-3 genes, 0.950 Acc, 0.9720 ARI, 0.934 NMI
                # 0.91 virclust/sqroot @ 0.5 for 1-3 genes, 0.9500 Acc, 0.8754 ARI, 0.8390 NMI
                # THEN "regularity"
                # 0.73 - 0.76 virclust/sqroot for 0.3 @ 1 gene | 0.67 - 0.72 shorter for 0.3 @ 1 gene |
                # 0.83 - 0.85 virclust/sqroot for 0.4 @ 1-2 genes | 0.74 - 0.76 for 0.3 @ 2-3 genes
                # ALL above are 0.9494 Acc, 0.8659 ARI, 0.826 NMI
                'cluster identity': 0.4,
                'PairwiseDistanceCutoff': 0.74
            },
        },
        'Adnaviria': {
            'realm': 0,
            'kingdom': 0,
            'phylum': 0,
            'class': 0,
            'order': {
                'cluster identity': 0.3,
                'PairwiseDistanceCutoff': 0.99  # Acc 0.967, 0.989 ARI, 0.953 NMI @ 1-3 genes
                # 0.97 - 0.98 for all but Jaccard. Another instance where shorter superior at 0.97/0.98
                # Below 0.96 Acc 0.948, 0.975 ARI, 0.911 NMI
                # 0.94-0.95 for shorter @ 40%, otherwise below 0.97 below 0.94 Acc
            },
            'family': {
                # ONLY recorded instance of 40%, 0.99, 1-3 genes for 0.963 Acc, 0.988 ARI, 0.942 NMI
                'cluster identity': 0.3,
                'PairwiseDistanceCutoff': 0.98  # 0.962 Acc, 0.979 ARI, 0.921 NMI
                # 0.99 Also good, but overlaps with order
                # 0.95 - 0.97 with 2-3 genes, NOT 1
            },
            'subfamily': {
                'cluster identity': 0.3,
                # 0.94, 0.97 all 0.987 Acc, 0.997 ARI, 0.984 NMI @ 1-3 genes
                # 0.92 - 0.93 all 0.985 Acc, 0.983 ARI, 0.960 NMI @ 1 gene
                'PairwiseDistanceCutoff': 0.92  # Selected over 0.94 to decrease overlap
            },
            'genus': {
                # 0.98 - 0.99 SqRoot ONLY, 60%, 0.982 Acc, 0.960 ARI, 0.900 NMI @ 1 gene
                # 0.95 - 0.99 all, 50%, 0.981 Acc, 0.9577 ARI, 0.894 NMI @ 3 gene
                # 0.98 - 0.99 all, 60%, 0.981 Acc, 0.960 ARI, 0.898 NMI @ 1-2 genes
                # 0.88 - 0.91 Jaccard only, 40%, 0.977 Acc, 0.919 ARI, 0.851 NMI @ 1 gene
                # 0.94 - 0.95 virclust, 40%, 0.977 Acc, 0.919 ARI, 0.851 NMI @ 1 gene
                # 0.80 - 0.91 all, 30%, 0.977 Acc, 0.919 ARI, 0.851 NMI @ 1-3 genes
                'cluster identity': 0.3,
                'PairwiseDistanceCutoff': 0.83  # Selected for 30% and was 0.03 from Acc "drop" below 0.8
                # Weird double peak in 80s, and then in low 60s
            },
        },
        'Varidnaviria': {
            'realm': 0,
            'kingdom': 0,
            'phylum': 0,
            'class': 0,
            'order': {
                'cluster identity': 0.3,
                'PairwiseDistanceCutoff': 0.99  # Acc 0.967, 0.989 ARI, 0.953 NMI @ 1-3 genes
                # 0.97 - 0.98 for all but Jaccard. Another instance where shorter superior at 0.97/0.98
                # Below 0.96 Acc 0.948, 0.975 ARI, 0.911 NMI
                # 0.94-0.95 for shorter @ 40%, otherwise below 0.97 below 0.94 Acc
            },
            'family': {
                # ONLY recorded instance of 40%, 0.99, 1-3 genes for 0.963 Acc, 0.988 ARI, 0.942 NMI
                'cluster identity': 0.3,
                'PairwiseDistanceCutoff': 0.98  # 0.962 Acc, 0.979 ARI, 0.921 NMI
                # 0.99 Also good, but overlaps with order
                # 0.95 - 0.97 with 2-3 genes, NOT 1
            },
            'subfamily': {
                'cluster identity': 0.3,
                # 0.94, 0.97 all 0.987 Acc, 0.997 ARI, 0.984 NMI @ 1-3 genes
                # 0.92 - 0.93 all 0.985 Acc, 0.983 ARI, 0.960 NMI @ 1 gene
                'PairwiseDistanceCutoff': 0.92  # Selected over 0.94 to decrease overlap
            },
            'genus': {
                # 0.98 - 0.99 SqRoot ONLY, 60%, 0.982 Acc, 0.960 ARI, 0.900 NMI @ 1 gene
                # 0.95 - 0.99 all, 50%, 0.981 Acc, 0.9577 ARI, 0.894 NMI @ 3 gene
                # 0.98 - 0.99 all, 60%, 0.981 Acc, 0.960 ARI, 0.898 NMI @ 1-2 genes
                # 0.88 - 0.91 Jaccard only, 40%, 0.977 Acc, 0.919 ARI, 0.851 NMI @ 1 gene
                # 0.94 - 0.95 virclust, 40%, 0.977 Acc, 0.919 ARI, 0.851 NMI @ 1 gene
                # 0.80 - 0.91 all, 30%, 0.977 Acc, 0.919 ARI, 0.851 NMI @ 1-3 genes
                'cluster identity': 0.3,
                'PairwiseDistanceCutoff': 0.83  # Selected for 30% and was 0.03 from Acc "drop" below 0.8
                # Weird double peak in 80s, and then in low 60s
            },
        }
    }
}
