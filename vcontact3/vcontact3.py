import logging
import os
import shutil
import sys

from . import __version__ as version
from . import exports as exporter
from . import imports as imports
from . import performance as performance
from . import profiles as profiles
from . import utilities as utils
from . import resolver as resolvers
from . import gbga as gbgas
from . import databases as databases

import pickle
import gzip


def run_vcontact3(
        nucleotides=None,
        proteins=None,
        gene2genome=None,
        nucl_len=None,
        run_prodigal_gv=None,
        run_viral=None,
        output_dir=None,
        db_domain=None,
        db_version=None,
        db_path=None,
        default_realm=None,
        min_shared=None,
        dist_metric=None,
        mmseqs_fp=None,
        exports=None,
        nat_breaks=None,
        keep_temp=None,
        no_progress=None,
        threads=None,
        no_parallel=None,
        reduce_memory=None,
        no_graphs=None,
        min_members=None,
        target_ranks=None,
        verbose=None,
        force=False,
):
    # https://fangpenlin.com/posts/2012/08/26/good-logging-practice-in-python/
    # Logger config
    logger_levels = {
        0: logging.INFO,
        1: logging.WARNING,
        2: logging.ERROR,
        3: logging.DEBUG,
    }
    logging.basicConfig(
        filename=output_dir / f'vConTACT3_{version}.log',
        filemode='a',
        level=logger_levels[verbose],
        format="[%(levelname)s] %(asctime)s %(message)s",
        datefmt="%Y-%m-%d %H:%M:%S %p",
    )

    # Create logger
    logger = logging.getLogger(__name__)

    main_intro = f"This is vConTACT3 {version}"
    logger.info(f"\n{main_intro:=^80}\n")

    logger.info('Identifying TTY')
    isatty = sys.__stdin__.isatty()
    if isatty:  # If interactive
        if no_progress:  # Disable
            logger.info("Interactive mode detected, but progress is DISABLED by user")
            isatty = False
        else:
            logger.info("Interactive mode detected and progress ENABLED")
    else:
        logger.info("Non-interactive mode detected, progress DISABLED")

    logger.info("Identifying tool locations...")

    tools_bin = ["mmseqs"]
    tools_arg = [mmseqs_fp]

    tools = utils.find_tools(tools_bin, tools_arg)

    logger.info(
        "Identified the following locations for tools. If one is not found, it may not impact the analysis, "
        "unless said tool is required for the analysis (i.e. not finding MCL but needing it to cluster "
        "proteins)\n" + utils.fmt_dict(tools)
    )

    logger.info(f"Identified {threads} CPUs")

    logger.info("Sourcing databases...")

    db_info, db_fp = databases.open_database(db_path, db_version, db_domain)

    logger.info("Identifying and validating inputs...")

    # TODO Streamline block logic
    # TODO Unable to identify post-nucleotide files during re-run, but internally, files are being used
    nucleotides_fp = None
    proteins_fp = None
    gene2genome_fp = None
    if nucleotides:
        if not nucleotides.exists():
            logger.error(
                "Unable to locate nucleotides file. This could be a simple path issue. Please provide the "
                "full path to the nucleotide file (often ends in *.fna or *.fasta) if the relative path "
                "fails."
            )
        nucleotides_fp = nucleotides  # else...
        if proteins and gene2genome and nucl_len:
            logger.warning(
                "Nucleotide and protein sequences (WITH gene-to-genome file), and genome length files were provided."
                "Will bypass gene prediction. To avoid this notification in the future, provide just the "
                "nucleotide sequence file."
            )
            # Don't add nucleotide here to keep nucleotide variable empty

        elif any(not x for x in [proteins, gene2genome, nucl_len]):  # If missing any one of these
            names = ['protein sequence', 'gene-to-genome', 'nucleotide length']
            for user_arg, target in zip([proteins, gene2genome, nucl_len],
                                        names):
                if not user_arg:
                    logger.warning(f"Unable to identify {target} file.")

            if not nucleotides:
                logger.error(f"Unable to proceed further without ALL of the {''.join(names)} "
                             f"files OR genome nucleotide sequences")
                exit(1)

            else:  # Only providing nucleotides
                # Send to gene calling
                nucleotides_fp = nucleotides
        else:
            print('Unknown status')
            exit(1)

    input_fps = {
        "Nucleotides": nucleotides_fp,
        "Proteins": proteins_fp,
        "Gene-to-genome": gene2genome_fp
    }
    logger.info(
        'Identified the following input files. If you provided a file path and it is not found (i.e. "None") '
        "then check for any warning messages and correct.\n" + utils.fmt_dict(input_fps)
    )

    pre_flight_complete = "Pre-flight checks complete! Beginning analysis"
    logger.info(f"\n{pre_flight_complete:-^80}\n")

    output_dir = utils.check_dir_exists_and_create(output_dir)

    # What kind of inputs do we have?
    if nucleotides_fp:
        logger.info("Predicting genes and generating gene-to-genome mapping file.")
        proteins_fp, gene2genome_fp, nucl_len_fp = imports.call_genes(
            nucleotides_fp, output_dir, force=force, threads=threads,
            run_pyrodigal_gv=run_prodigal_gv, run_viral=run_viral,
            ref_db=db_info, ref_fp=db_fp, ref_domain=db_domain, verbose=isatty,
        )   # Will write g2g to parquet (to better handle huge scale datasets)
    else:  # Need to catch this and 're-direct' any existing protein/gene2genome input to output directory
        logger.info(
            "Identified gene, gene-to-genome mapping, and genome lengths files, copying to output directory..."
        )
        proteins_fp = output_dir / proteins.name
        gene2genome_fp = output_dir / gene2genome.name
        nucl_len_fp = output_dir / nucl_len.name
        shutil.copy2(proteins, proteins_fp)
        shutil.copy2(gene2genome, gene2genome_fp)
        shutil.copy2(nucl_len, nucl_len_fp)

    profile_fn = proteins_fp.stem
    profile_pkl = output_dir / f"{profile_fn}.{db_domain}.profile.pkl.gz"

    if not profile_pkl.exists():

        identities = [0.3, 0.4, 0.5, 0.6, 0.7]

        profile = profiles.Profiles(
            user_genes=proteins_fp,
            user_g2g=gene2genome_fp,
            user_len=nucl_len_fp,
            ref_db=db_info,
            ref_fp=db_fp,
            ref_domain=db_domain,
            identities=identities,
            output_dir=output_dir,
            threads=threads,
            force=False,
            keep_temp=keep_temp,
            mmseqs_fp=tools['mmseqs'],
            verbose=isatty,
        )

        with gzip.open(profile_pkl, "wb") as profile_gzip_fh:
            logger.info(
                f'Saving profiles to {profile_pkl}. If there is a problem during downstream processing, '
                'vConTACT3 will resume from AT LEAST this point. Enable --force-overwrite if you wish to '
                'start from scratch.'
            )
            pickle.dump(profile, profile_gzip_fh, pickle.HIGHEST_PROTOCOL)

    else:
        with gzip.open(profile_pkl, "rb") as profile_gzip_fh:
            profile = pickle.load(profile_gzip_fh)
            logger.info(
                f'Loaded profiles from {profile_pkl}. This may occur if the previous run abruptly failed or '
                f'there was some sort of issue. Continuing from this point, or further.'
            )

    resolver_fn = proteins_fp.stem
    resolver_pkl = output_dir / f"{resolver_fn}.{db_domain}.resolver.pkl.gz"

    if not resolver_pkl.exists():

        resolver = resolvers.Resolver(
            profile,
            ref_db=db_info,
            ref_fp=db_fp,
            ref_domain=db_domain,
            metric=dist_metric,
            min_shared_genes=min_shared,
            threads=threads,
            output_dir=output_dir,
            no_parallel=no_parallel,
            no_graphs=no_graphs,
            verbose=isatty,
        )

        with gzip.open(resolver_pkl, "wb") as resolver_gzip_fh:
            logger.info(
                f'Saving resolvers to {resolver_pkl}. If there is a problem during downstream processing, '
                f'vConTACT3 will resume from AT LEAST this point. Enable --force-overwrite if you wish to '
                f'start from scratch.'
            )
            pickle.dump(resolver, resolver_gzip_fh)

    else:
        with gzip.open(resolver_pkl, "rb") as resolver_gzip_fh:
            resolver = pickle.load(resolver_gzip_fh)
            logger.info(
                f'Loaded resolvers from {resolver_pkl}. This may occur if the previous run abruptly failed or '
                f'there was some sort of issue. Continuing from this point, or further.'
            )

    gbga_pkl = output_dir / f"{resolver_fn}.{db_domain}.gbga.pkl.gz"

    if not gbga_pkl.exists():

        gbga = gbgas.GBGA(
            resolver,
            ref_db=db_info,
            ref_fp=db_fp,
            ref_domain=db_domain,
            metric=dist_metric,
            predict_type='naive',
            default_realm=default_realm,
            min_shared_genes=min_shared,
            threads=threads,
            output_dir=output_dir,
            verbose=isatty,
            reduce_memory=reduce_memory,
        )

        with gzip.open(gbga_pkl, "wb") as gbga_gzip_fh:
            logger.info(
                f"Saving GBGA information to {gbga_pkl}. If there is a problem during downstream "
                f"processing, vConTACT3 will resume from AT LEAST this point. Enable --force-overwrite if you "
                f"wish to start from scratch."
            )
            pickle.dump(gbga, gbga_gzip_fh)

    else:

        with gzip.open(gbga_pkl, "rb") as gbga_gzip_fh:
            gbga = pickle.load(gbga_gzip_fh)
            logger.info(
                f'Loaded GBGA from {gbga_pkl}. This may occur if the previous run abruptly failed or '
                f'there was some sort of issue. Continuing from this point, or further.'
            )

    # Performance of the run
    performance_pkl = output_dir / f"{resolver_fn}.{db_domain}.performance.pkl.gz"

    if not performance_pkl.exists():

        performancer = performance.Performance(
            gbga,
            output_dir=output_dir
        )

        with gzip.open(performance_pkl, "wb") as performance_gzip_fh:
            logger.info(
                f'Saving performance to {performance_pkl}. If there is a problem during downstream processing, '
                f'vConTACT3 will resume from AT LEAST this point. Enable --force-overwrite if you wish to '
                f'start from scratch.'
            )
            pickle.dump(performancer, performance_gzip_fh)

    else:
        with gzip.open(performance_pkl, "rb") as performance_gzip_fh:
            performancer = pickle.load(performance_gzip_fh)
            logger.info(
                f"Loaded performances from {performance_pkl}. This may occur if the previous run abruptly "
                f"failed or there was some sort of issue. Continuing from this point, or further."
            )

    # TODO If someone wants to re-run and this is still present, it won't rerun. Either remove or enable re-run options
    export_pkl = output_dir / f"{resolver_fn}.{db_domain}.exports.pkl.gz"

    if not export_pkl.exists():

        export = exporter.Export(gbga,
                                 performancer,
                                 profile,
                                 exports,
                                 output_dir,
                                 nat_breaks=nat_breaks,
                                 min_members=min_members,
                                 target_ranks=target_ranks)

        with gzip.open(export_pkl, "wb") as export_gzip_fh:
            logger.info(
                f"Saving exports info to {export_pkl}. If there is a problem during downstream processing, "
                f"vConTACT3 will resume from AT LEAST this point. Enable --force-overwrite if you wish to "
                f"start from scratch."
            )
            pickle.dump(export, export_gzip_fh)

    else:
        with gzip.open(export_pkl, "rb") as export_gzip_fh:
            export = pickle.load(export_gzip_fh)
            logger.info(
                f"Loaded exports info from {export_pkl}. This may occur if the previous run abruptly failed or "
                "there was some sort of issue. Continuing from this point, or further."
            )

    finished_msg = f'vConTACT3 {version} finished successfully! \n If you find this software useful, please cite the ' \
                   f'following tools that helped run this analysis:\n\n' \
                   f'\n\n' \
                   f'Steinegger, M. & Söding, J. MMseqs2 enables sensitive protein sequence searching for the analysis of massive data sets. Nat Biotechnol 35, 1026–1028 (2017).\n' \
                   f'Angriman, E., van der Grinten, A., Hamann, M., Meyerhenke, H. & Penschuck, M. Algorithms for Large-Scale Network Analysis and the NetworKit Toolkit. in Algorithms for Big Data: DFG Priority Program 1736 (eds. Bast, H., Korzen, C., Meyer, U. & Penschuck, M.) 3–20 (Springer Nature Switzerland, Cham, 2022).\n' \
                   f'Virtanen, P. et al. SciPy 1.0: fundamental algorithms for scientific computing in Python. Nat Methods 17, 261–272 (2020).\n' \


    logger.info(finished_msg)
