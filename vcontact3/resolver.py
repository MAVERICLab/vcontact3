"""
Resolve cross-domain connections in the network and assign domains to user sequences
"""

import gzip
import logging
import math
import pickle
import sys
import time
import warnings
import multiprocessing as mp
from collections import Counter, defaultdict
from itertools import combinations
from pathlib import Path
from typing import Literal, get_args
from typing import Tuple
from pprint import pprint
import h5py

import igraph as ig
import networkit as nk
import networkx as nx
import numpy as np
import pandas as pd
import scipy.sparse as sparse
from tqdm import tqdm

try:
    from line_profiler_pycharm import profile as profile_this
except ImportError:
    pass

from .utilities import write_igraphz

from . import profiles

warnings.filterwarnings("ignore")

logger = logging.getLogger(__name__)

_TYPES = Literal['VirClust', 'SqRoot', 'Shorter', 'Jaccard']

tqdm.pandas()


def process_component(args: Tuple[int, pd.DataFrame, dict, dict, dict]) -> Tuple[dict, list, str]:
    """
    args
    :return: dict
    """
    component, component_df, ref_realm_pc_counts, user_realm_shared_pcs, ref_realm_d = args

    c_members = component_df.index.tolist()
    unique_gene_realms = Counter()

    # Identify reference and user sequences in the component
    c_refs = component_df[component_df['Reference']].index.tolist()
    c_user = component_df[~component_df['Reference']].index.tolist()

    # Count unique PCs shared
    for realm in ref_realm_pc_counts.keys():  # Don't bother if it's not even in refs

        for user_genome, user_shared_realms_d in user_realm_shared_pcs.items():
            if user_genome in c_user:
                if f'{realm}_shared_PCs' in user_shared_realms_d:
                    user_shared_pcs = user_shared_realms_d[f'{realm}_shared_PCs']
                    unique_gene_realms.update([realm] * len(user_shared_pcs))

    # Get realm counts for references
    ref_realms = [ref_realm_d[ref] for ref in c_refs if ref in ref_realm_d]
    realm_counts = Counter([r for r in ref_realms if r])
    realm_keys = list(realm_counts.keys())
    unique_gene_realms_keys = list(unique_gene_realms.keys())

    # Determine realm prediction based on realm counts and unique gene realms
    if len(realm_keys) == 1:  # Only 1 reference realm
        realm_prediction = realm_keys[0]

    elif len(realm_keys) > 1:  # More than 1 reference realm  # rare if correct component splits

        ranked_refs = [k for k, v in realm_counts.most_common()]
        if not unique_gene_realms:
            realm_prediction = '|'.join(ranked_refs)

        else:

            if len(unique_gene_realms_keys) == 1:

                if set(unique_gene_realms_keys).issubset(set(realm_keys)):
                    realm_prediction = '|'.join(ranked_refs)

                else:
                    ranked_refs += [unique_gene_realms_keys]
                    realm_prediction = '|'.join(ranked_refs)
            else:

                if set(unique_gene_realms_keys).issubset(set(realm_keys)):
                    realm_prediction = '|'.join(ranked_refs)
                else:

                    ranked_unique_genes = [k for k, v in unique_gene_realms.most_common()]
                    preds = set(ranked_refs) | set(ranked_unique_genes)
                    realm_prediction = '|'.join(preds)

    elif not realm_keys:  # No references with realms, falling back to PCs

        if not unique_gene_realms:  # Can't do much
            realm_prediction = 'No prediction'

        else:  # Has PCs it can use

            if len(unique_gene_realms_keys) == 1:  # Only 1, thank goodness
                realm_prediction = unique_gene_realms_keys[0]
            else:
                ranked_unique_genes = [k for k, v in unique_gene_realms.most_common()]
                realm_prediction = '|'.join(ranked_unique_genes)

    else:
        realm_prediction = 'No prediction'
        # logger.warning(f'Unable to determine realm for component {component} with genomes: {c_members}')

    component_summary = {
        'component': component,
        'Realms': '|'.join(realm_keys) if realm_keys else np.nan,
        'References': len(c_refs),
        'User': len(c_user),
        'Realm prediction': realm_prediction
    }
    for realm, unique in unique_gene_realms.items():
        component_summary[f'{realm} (unique genes)'] = unique

    return component_summary, c_members, realm_prediction


def process_genomes(profile_df: pd.DataFrame, user_genomes: list) -> dict:
    """
    param profile_df: (pd.DataFrame) containing the profile of reference genomes
    param user_genomes: (list) of user genomes

    :return: dict Dictionary containing user genomes as keys and their respective user PCs as values.
    """

    profile_coo = profile_df.sparse.to_coo()  # Fast
    pcs = profile_df.columns.tolist()
    genome_indices = profile_df.index.tolist()

    # Keep track of PCs prep
    user_pcs = defaultdict(lambda: {'user_PCs': []})

    for genome in user_genomes:
        if genome in genome_indices:
            idx = genome_indices.index(genome)

            # Ugh, so much faster
            positions = np.where(profile_coo.row == idx)[0]

            user_pcs[genome]['user_PCs'].extend([pcs[i] for i in profile_coo.col[positions]])

    return user_pcs


class Resolver(object):

    def __init__(self, profile,
                 ref_db: dict,
                 ref_fp: Path,
                 ref_domain: str,
                 metric: str,
                 min_shared_genes: int,
                 threads: int,
                 output_dir: Path,
                 no_parallel: bool = False,
                 no_graphs: bool = False,
                 verbose: bool = True,
                 ):

        verbose = not verbose

        ref_version = ref_fp.stem
        ref_dir = ref_fp.parent

        identities = [0.3, 0.4, 0.5, 0.6, 0.7]
        if isinstance(profile, profiles.Profiles):
            self.genomes_overview = profile.genomes_overview  # Everything

            if not all(str(identity) in profile.identities for identity in identities):
                logger.error(f'The following identities are not present in the overviews: {identities}')
                sys.exit(1)

        else:
            logger.error('This isn\'t a Resolvers object. '
                         'Please send this error message to the author.')
            sys.exit(1)

        self.metric = metric
        self.min_shared_genes = min_shared_genes
        self.threads = threads
        self.output_dir = output_dir
        self.name = 'HMMprofile'
        self.identities = {}
        self.ref_db = ref_db[ref_version]["Domains"][ref_domain]  # TODO Don't know if need entire dict
        # TODO just grab the identities here
        ref_genomes_report_df = pd.read_parquet(ref_dir / self.ref_db['Genomes Report'])

        # EACH identity will have its own profile and network
        # Aggregate composition of each identity's unique genes
        # TODO multiprocess this...?
        for identity in identities:

            identity = str(identity)
            logger.info(f'Identifying {identity}...')

            if identity not in self.identities:
                self.identities[identity] = {}

            profile_fp = profile.identities[identity]['crosstab_fp']  # Don't need updated_c2g as it --> crosstab

            with h5py.File(profile_fp, 'r') as f:
                data = f['crosstab_data'][:]
                row = f['crosstab_row'][:]
                col = f['crosstab_col'][:]
                shape = tuple(f['crosstab_shape'][:])

                row_labels = f['crosstab_row_labels'][:].astype(str)  # Convert back from byte strings to strings
                col_labels = f['crosstab_col_labels'][:].astype(str)

                # Reconstruct the sparse matrix
                # THIS IS THE ONLY COO_MATRIX in h5
                profile_df = pd.DataFrame.sparse.from_spmatrix(
                    sparse.coo_matrix((data, (row, col)), shape=shape),
                    index=row_labels, columns=col_labels)

            curr_identity_genomes = profile.identities[identity]['genomes_overview']

            unique_genes = self.ref_db['identities'][identity]['unique genes']
            matrices_fp = output_dir / f'HMMprofile.{identity}_{metric}_dist_and_shared_genes.h5'
            # TODO Need to expose network export at this point for user
            # ntw_fp = output_dir / f'{name}_{metric}_distance.ntw' if export_ntw else False
            ntw_fp = False
            if matrices_fp.exists():
                logger.info(f'Identified existing shared gene and distance matrices in output directory.')
                with h5py.File(matrices_fp, 'r') as f:
                    # Load distance matrix
                    distance_data = f['distance_data'][:]
                    distance_indices = f['distance_indices'][:]
                    distance_indptr = f['distance_indptr'][:]
                    distance_shape = tuple(f['distance_shape'][:])

                    distance_pos_labels = f['distance_index'][:].astype(str)

                    # Don't delete, this was original logic, but wanted to reduce
                    # distance_csr = sparse.csr_matrix((distance_data, distance_indices, distance_indptr),
                    #                                  shape=distance_shape)
                    distance_matrix_df = pd.DataFrame.sparse.from_spmatrix(
                        sparse.csr_matrix((distance_data, distance_indices, distance_indptr), shape=distance_shape),
                        index=distance_pos_labels, columns=distance_pos_labels)

                    # Load the second sparse matrix (related_matrix) # csr_matrix((data, indices, indptr), shape=(3, 3))
                    shared_data = f['shared_data'][:]
                    shared_indices = f['shared_indices'][:]
                    shared_indptr = f['shared_indptr'][:]
                    shared_shape = tuple(f['shared_shape'][:])

                    shared_labels = f['shared_index'][:].astype(str)

                    shared_df = pd.DataFrame.sparse.from_spmatrix(
                        sparse.csr_matrix((shared_data, shared_indices, shared_indptr), shape=shared_shape),
                        index=shared_labels, columns=shared_labels)

            else:
                # Build distance matrix w/ network + shared genes - exclude network, because not needed at this point
                (matrices_fp,
                 shared_df,  # Genes shared between genomes
                 ntw_fp,
                 distance_matrix_df
                 ) = build_dist_matrix(crosstab=profile_df, output_dir=output_dir,
                                       name=profile.identities[identity]['name'], metric=metric, identity=identity,
                                       export_ntw=False, verbose=verbose)

            # Keep these so they can be reused during GBGA
            self.identities[identity]['shared_and_dist_fp'] = matrices_fp  # "Raw" shared genes
            self.identities[identity]['network_fp'] = ntw_fp

            filtered_dist_network_fp = output_dir / f'HMMprofile.{identity}_{metric}_dist.nkbg'
            filtered_shared_network_fp = output_dir / f'HMMprofile.{identity}_{metric}_shared.nkbg'
            filtered_network_tracker_fp = output_dir / f'HMMprofile.{identity}_{metric}_ntw.pkl'
            if not filtered_dist_network_fp.exists() or not filtered_network_tracker_fp.exists():
                G, tracker = build_networkit_network(distance_matrix_df, shared_df, ref_genomes_report_df,
                                                     min_shared_genes, no_graphs,
                                                     output_dir / f'HMMprofile.{identity}_{metric}',
                                                     filtered_network_tracker_fp)
            else:
                G = nk.readGraph(str(filtered_dist_network_fp), nk.Format.NetworkitBinary)
                with open(filtered_network_tracker_fp, 'rb') as tracker_fh:
                    tracker = pickle.load(tracker_fh)

            node_index_map = {v['idx']: k for k, v in tracker.items()}  # idx: genome_id
            cc = nk.components.ConnectedComponents(G)
            cc.run()

            # Iterate over each component
            for num_component, component_nodes in enumerate(cc.getComponents()):
                orig_nodes = [node_index_map[c] for c in component_nodes]
                curr_identity_genomes.loc[orig_nodes, 'component'] = str(num_component)  # Is INT

            self.identities[identity]['filtered_dist_network_fp'] = filtered_dist_network_fp
            self.identities[identity]['filtered_shared_network_fp'] = filtered_shared_network_fp
            self.identities[identity]['filtered_network_tracker_fp'] = filtered_network_tracker_fp

            logger.info('Preparing for identification of genes shared between references and user sequences')

            # Okay. Round 3 of revising this entire workflow. Already have a high-performance graph, iterable through
            # each component. Since we already have distance matrix, just need to assign realm via refs or unique genes

            component_summary_df = pd.DataFrame(
                index=[str(i) for i, n in enumerate(curr_identity_genomes['component'].unique())],
                columns=['References', 'User', 'Realms'])  # force i as str

            # Set user and reference sequences
            user_genomes = curr_identity_genomes[~(curr_identity_genomes['Reference'])].index.tolist()
            ref_seqs = curr_identity_genomes[curr_identity_genomes['Reference']].index.tolist()

            # THIS ENTIRE BLOCK, UNTIL CONNECTED COMPONENTS, is simply assigning realm PCs to each member

            # Too long to .loc members and see if they share unique genes with references
            realm_pc_counts = {}
            ref_realm_pcs = {}  # {genome: {realm: [pc1, pc2,...]}
            for realm, unique_gene_fp in unique_genes.items():
                ref_profile_df = pd.read_parquet(ref_dir / unique_gene_fp)

                # Profiled here, increased by 50X
                logger.info(f'Identifying genes shared within references of realm: {realm}')

                if int(ref_version) > 220:

                    #
                    row_dict = {k: n for n, k in enumerate(ref_profile_df.row.unique())}
                    col_dict = {k: n for n, k in enumerate(ref_profile_df.col.unique())}

                    # Apply to df
                    ref_profile_df['col'] = ref_profile_df['col'].map(col_dict)
                    ref_profile_df['row'] = ref_profile_df['row'].map(row_dict)

                    ref_coo_df = sparse.coo_matrix((ref_profile_df['data'], (ref_profile_df['row'], ref_profile_df['col'])))

                    inverse_row_dict = {v: k for k, v in row_dict.items()}
                    inverse_col_dict = {v: k for k, v in col_dict.items()}

                    ref_genome_pcs = {genome: [] for genome in row_dict.keys()}
                    for i, j in zip(ref_coo_df.row, ref_coo_df.col):
                        genome = inverse_row_dict[i]  # Find genome name by index
                        pc = inverse_col_dict[j]  # Find pc name by index
                        ref_genome_pcs[genome].append(pc)

                    for genome, genome_pcs in tqdm(ref_genome_pcs.items(),
                                                   total=len(ref_genome_pcs),
                                                   unit=' genome PCs', disable=verbose):
                        if genome not in ref_realm_pcs:
                            ref_realm_pcs[genome] = {}

                        ref_realm_pcs[genome][f'{realm}_PCs'] = genome_pcs

                        if realm not in realm_pc_counts:
                            realm_pc_counts[realm] = Counter(genome_pcs)
                        else:
                            realm_pc_counts[realm].update(genome_pcs)

                else:

                    for genome, pcs in tqdm(ref_profile_df.iterrows(),
                                            total=len(ref_profile_df),
                                            unit=' genome PCs', disable=verbose):
                        pcs = pcs[pcs >= 1]

                        if genome not in ref_realm_pcs:
                            ref_realm_pcs[genome] = {}

                        if len(pcs) > 0:

                            ref_realm_pcs[genome][f'{realm}_PCs'] = pcs.index.tolist()

                            if realm not in realm_pc_counts:
                                realm_pc_counts[realm] = Counter(pcs.index.tolist())
                            else:
                                realm_pc_counts[realm].update(pcs.index.tolist())

            ref_unique_gene_counts = {}  # {realm1: {pc: count}, realm2: {pc: count},...}

            logger.info(f'Ensuring no PCs (in reference genomes) are shared between realms...')
            shared_keys = set(realm_pc_counts.keys()) & set(unique_genes.keys())
            for realm in shared_keys:
                curr_realm = realm_pc_counts[realm].copy()

                for comp_realm in shared_keys:
                    if realm != comp_realm:  # Self
                        comp_realm_counts = realm_pc_counts[comp_realm]

                        shared_keys_to_remove = curr_realm.keys() & comp_realm_counts.keys()

                        for shared_key in shared_keys_to_remove:
                            del curr_realm[shared_key]

                ref_unique_gene_counts[realm] = curr_realm

            logger.info(f'Enforcing minimum PC count (3) for each unique gene in each realm...')
            for realm in shared_keys:
                ref_unique_gene_counts[realm] = {k: v for k, v in ref_unique_gene_counts[realm].items() if v >= 3}

            # Can't iterrows if too many user sequences, as pd will copy and make int64 and consume 100s of GBs (works
            # superfast if not many user sequences). And np.vectorize, pd.apply, pd.swifter.apply are all very slow

            # Profile start
            logger.info(f'Identifying genes shared within user sequences (references still included)...')

            user_pcs = process_genomes(profile_df, user_genomes)

            logger.info(f'Identifying shared PCs between user and references')

            # Verified to be identical to previous method
            user_realm_shared_pcs = defaultdict(dict)  # user_id: {realm_shared_PCs: [pc1, pc2,...], ...}
            for user_genome, pcs_data in user_pcs.items():
                pcs = pcs_data['user_PCs']

                for realm, realm_counts in ref_unique_gene_counts.items():
                    shared_genes = set(pcs) & set(realm_counts.keys())

                    if shared_genes:
                        user_realm_shared_pcs[user_genome][f'{realm}_shared_PCs'] = shared_genes

            logger.info(f'Iterating through network components and identifying realms for identity: {identity}')

            # Go through component to assign
            # This block breaks down refs/users and realm "support"
            # Precompute realm keys avoid a million (literally!) df.loc[]
            ref_report_realms_d = ref_genomes_report_df.set_index('RefSeqID')['realm'].to_dict()

            # Group once outside the main loop to avoid repeated groupby calls
            grouped = curr_identity_genomes.groupby('component')

            args = [(component, component_df, ref_unique_gene_counts, user_realm_shared_pcs, ref_report_realms_d)
                    for component, component_df in grouped]

            if no_parallel:
                # Single-threaded processing
                logger.info(f'Disabled parallel processing of network components in order to reduce memory usage.'
                            f'Using sequential processing instead.')
                results = [process_component(arg) for arg in args]
            else:
                logger.info(f'Parallel processing of network components enabled...')
                with mp.Pool(self.threads) as pool:
                    results = pool.map(process_component, args)

            for component_summary, c_members, realm_prediction in results:
                component = str(component_summary['component'])  # MUST be INT to match --> forcing to str
                component_summary_df.at[component, 'Realms'] = component_summary['Realms']
                component_summary_df.at[component, 'References'] = component_summary['References']
                component_summary_df.at[component, 'User'] = component_summary['User']

                curr_identity_genomes.loc[c_members, 'realm prediction'] = realm_prediction
                curr_identity_genomes.loc[c_members, 'component'] = component

            self.identities[identity]['genomes_overview'] = curr_identity_genomes
            self.identities[identity]['components'] = component_summary_df

            # Maintain to keep track of isolated and edgeless
            isolates = [k for k, v in tracker.items() if v['isolated']]
            self.identities[identity]['isolated'] = isolates
            edgeless = [k for k, v in tracker.items() if v['edgeless']]
            self.identities[identity]['edgeless'] = edgeless

            logger.info(f'Completed identity {identity}')

            # Resolver does not need to hierarchical clustering anything, it just needs to assign

    def __repr__(self):
        mes = (
            "Clustering object {name}, consisting of the following:"
        )
        return mes.format(
            name=self.name
        )


def vectorized_sqroot_dist(shared_HMMs: sparse.csr_matrix, per_genome_HMM: np.array):
    # Obtain indices to calculate distances
    genomeA, genomeB = shared_HMMs.nonzero()

    shared_HMM = shared_HMMs.data  # Use non-zero directly

    # Get genome counts
    HMMs_A = per_genome_HMM[genomeA]
    HMMs_B = per_genome_HMM[genomeB]

    # Compute distances for all pairs
    distances = 1 - (shared_HMM / np.sqrt(HMMs_A * HMMs_B))

    return distances, genomeA, genomeB


def vectorized_jaccard_dist(shared_HMMs: sparse.csr_matrix, per_genome_HMM: np.array):

    genomeA, genomeB = shared_HMMs.nonzero()
    shared_HMM = shared_HMMs.data  # Use non-zero directly

    HMMs_A = per_genome_HMM[genomeA]
    HMMs_B = per_genome_HMM[genomeB]

    distances = 1 - float(shared_HMM / ((HMMs_A + HMMs_B) - shared_HMM))

    return distances, genomeA, genomeB


def vectorized_vircluster_dist(shared_HMMs: sparse.csr_matrix, per_genome_HMM: np.array):

    genomeA, genomeB = shared_HMMs.nonzero()
    shared_HMM = shared_HMMs.data  # Use non-zero directly

    HMMs_A = per_genome_HMM[genomeA]
    HMMs_B = per_genome_HMM[genomeB]

    distances = 1 - ((2 * shared_HMM) / (HMMs_A + HMMs_B))

    return distances, genomeA, genomeB


def vectorized_shorter_dist(shared_HMMs: sparse.csr_matrix, per_genome_HMM: np.array):

    genomeA, genomeB = shared_HMMs.nonzero()
    shared_HMM = shared_HMMs.data  # Use non-zero directly

    HMMs_A = per_genome_HMM[genomeA]
    HMMs_B = per_genome_HMM[genomeB]

    try:
        # TODO Double-check np calc
        distances = 1 - (shared_HMM / np.amin([HMMs_A, HMMs_B]))
    except np.AxisError as e:
        print(f'Numpy error: {e}\nShared HMMs: {shared_HMM}\nHMMs_A: {HMMs_A}\nHMMs_B: {HMMs_B}')
        exit(0)

    return distances, genomeA, genomeB


def build_dist_matrix(crosstab: pd.DataFrame, output_dir: Path, name: str, identity: float, metric: _TYPES = 'SqRoot',
                      export_ntw: bool = False, verbose: bool = True):

    """

    Builds a distance matrix using the HMM/PC Profile X Genome matrix

    :param crosstab:
    :param output_dir: Output directory
    :param identity:
    :param metric: Distance metric to apply to the profile
    :param export_ntw: Whether to export the edgelist network
    :param name:
    """

    # https://stackoverflow.com/questions/9522877/pythonic-way-to-have-a-choice-of-2-3-options-as-an-argument-to-a-function
    options = get_args(_TYPES)
    assert metric in options, f"'{metric}' is not in {options}"

    # Need this information to re-map numpy arrays back to human-readable
    genome_pos = crosstab.index.values  # .columns for module_pos

    logger.info(f'Building distance matrix from PC profile')
    # matrix = sparse.csr_matrix(crosstab.to_numpy(dtype=limit))

    matrix = crosstab.sparse.to_coo().tocsr()
    logger.debug(f'{matrix.data.nbytes / (1024 * 1024)} megabytes consumed')

    shared_hmms = matrix.dot(matrix.T)  # Can't cast to int8 as some genomes have >127 potential shared HMMs
    logger.info(f'Finished matrix preparation, converting to distances')

    if np.any(shared_hmms.data < 0):  # Hit if dtype is too large
        logger.error(f'Negative values in common PCs, cannot continue')
        exit(1)

    genomes, unique_hmms = matrix.shape  # Number of genomes, number of unique HMMs

    per_genome_HMMs = matrix.sum(1)
    per_genome_HMMs = per_genome_HMMs.A1  # HMMs for each genome, flattened (position based!)

    # total_common_hmms = float(shared_hmms.getnnz())

    # Moved saving HMMs towards end in order to integrate into h5, besides, calc/save takes seconds for huge scale

    start = time.time()
    logger.info(f"Building genome network with {genomes} contigs and {unique_hmms} HMMs")

    # data, rows, cols = vectorized_sqroot_dist(shared_hmms, per_genome_HMMs)  # 52.198s-->0.181s @ 5500 x 112000

    try:
        distance_func = getattr(sys.modules[__name__], 'vectorized_' + metric.lower() + '_dist')

        logger.info(f'Calculating distances using {metric}')

        data, rows, cols = distance_func(shared_hmms, per_genome_HMMs)

    except AttributeError as e:

        logger.error(f'Vectorized distance metric {metric} not found')
        exit(1)

    if len(data) == 0:
        logger.error(
            "No edges are found in genome_network. This is not supposed to happen since "
            "Reference genomes will have edges between themselves."
        )
        sys.exit(1)

    S = sparse.coo_matrix((data, (rows, cols)), shape=(genomes, genomes), dtype='float32').tocsr()

    logger.info(f"Genome-similarity/distance network : {genomes} genomes, {S.getnnz()} edges")

    end = time.time()
    t = end - start
    logger.info(f"Genome network building time: {t:.3f}s")

    if export_ntw:

        ntw_fp = output_dir / f'{name}_{metric}_distance.ntw'

        if not ntw_fp.exists():
            logger.info(f"Writing genome edges to {ntw_fp}. This file can be used by numerous 3rd party programs when "
                        f"imported as an edgelist.")

            with open(ntw_fp, "wt") as out_fh:
                pairs = zip(*S.nonzero())
                # dok is efficient at accessing elements
                dok_matrix = sparse.dok_matrix(S)
                for r, c in tqdm(pairs, unit=" genome edges written", disable=verbose):
                    # Need to keep track of which nodes go into network for outlier analysis

                    out_fh.write(
                        " ".join(
                            [str(x) for x in (genome_pos[r], genome_pos[c], dok_matrix[r, c],)]
                        )
                    )
                    out_fh.write("\n")

        else:
            logger.warning(f"Identified an existing network file at {ntw_fp}, reusing...")

    else:
        logger.info(f"Not exporting genome network...")
        ntw_fp = None

    sp_matrices_fp = output_dir / f'HMMprofile.{identity}_{metric}_dist_and_shared_genes.h5'
    logger.info(f'Writing shared HMMs and distance matrix to {sp_matrices_fp}')

    with h5py.File(sp_matrices_fp, 'w') as f:
        # Save shared PCs, CSR matrix
        f.create_dataset('shared_data', data=shared_hmms.data, compression='gzip')
        f.create_dataset('shared_indices', data=shared_hmms.indices, compression='gzip')
        f.create_dataset('shared_indptr', data=shared_hmms.indptr, compression='gzip')
        f.create_dataset('shared_shape', data=shared_hmms.shape, compression='gzip')

        # Save row and column labels
        shared_bytes_index = np.array(crosstab.index.astype(str).tolist(), dtype='S')
        f.create_dataset('shared_index', data=shared_bytes_index, compression='gzip')

        # Save distances, COO matrix
        f.create_dataset('distance_data', data=S.data, compression='gzip')
        f.create_dataset('distance_indices', data=S.indices, compression='gzip')
        f.create_dataset('distance_indptr', data=S.indptr, compression='gzip')
        f.create_dataset('distance_shape', data=S.shape, compression='gzip')

        distance_bytes_index = np.array(genome_pos.astype(str).tolist(), dtype='S')

        f.create_dataset('distance_index', data=distance_bytes_index, compression='gzip')

    # Create
    shared_HMMs_df = pd.DataFrame.sparse.from_spmatrix(data=shared_hmms,
                                                       index=crosstab.index.tolist(),
                                                       columns=crosstab.index.tolist())

    distance_df = pd.DataFrame.sparse.from_spmatrix(data=S, columns=genome_pos,
                                                    index=genome_pos)  # 23206400 bytes (23 MB)

    return sp_matrices_fp, shared_HMMs_df, ntw_fp, distance_df


def virclust_dist(genomeA, genomeB, shared_HMMs: np.array, per_genome_HMM: np.array):

    HMMs_A, HMMs_B = per_genome_HMM[genomeA], per_genome_HMM[genomeB]
    shared_HMM = shared_HMMs[genomeA, genomeB]

    dist = 1 - ((2 * shared_HMM) / (HMMs_A + HMMs_B))

    return dist


def sqroot_dist(genomeA, genomeB, shared_HMMs: np.array, per_genome_HMM: np.array):

    HMMs_A, HMMs_B = per_genome_HMM[genomeA], per_genome_HMM[genomeB]
    shared_HMM = shared_HMMs[genomeA, genomeB]

    dist = 1 - (shared_HMM / math.sqrt(HMMs_A * HMMs_B))

    return dist


def shorter_dist(genomeA, genomeB, shared_HMMs: np.array, per_genome_HMM: np.array):

    HMMs_A, HMMs_B = per_genome_HMM[genomeA], per_genome_HMM[genomeB]
    shared_HMM = shared_HMMs[genomeA, genomeB]

    try:
        dist = 1 - (shared_HMM / np.amin([HMMs_A, HMMs_B]))
    except np.AxisError as e:
        print(f'Numpy error: {e}\nShared HMMs: {shared_HMM}\nHMMs_A: {HMMs_A}\nHMMs_B: {HMMs_B}')
        exit(0)

    return dist


def jaccard_dist(genomeA, genomeB, shared_HMMs: np.array, per_genome_HMM: np.array):

    HMMs_A, HMMs_B = per_genome_HMM[genomeA], per_genome_HMM[genomeB]
    shared_HMM = shared_HMMs[genomeA, genomeB]

    dist = 1 - float(shared_HMM / ((HMMs_A + HMMs_B) - shared_HMM))

    return dist


def build_networkit_network(distance_matrix: pd.DataFrame, shared_matrix: pd.DataFrame,
                            reference_df: pd.DataFrame, min_shared: int = 3, no_graphs: bool = False,
                            g_out_fp: Path = '', t_out_fp: Path = '') -> Tuple[nk.Graph, dict]:

    """
    :param distance_matrix: A pandas DataFrame containing the distance matrix between genomes.
    :param shared_matrix: A pandas DataFrame containing the shared gene matrix between genomes.
    :param reference_df: A pandas DataFrame containing reference data for the reference genomes.
    :param min_shared: The minimum number of shared genes required to connect two genomes.
    :param g_out_fp: The output file base path where the NetworKit graph(s) will be saved.
    :param t_out_fp: The output file path where the map tracker will be saved.
    :return: A NetworKit graph (G) and a map tracker (map_tracker) for the genomes.

    Finally, the function writes the NetworKit graph and the map tracker to the specified output file paths.
    """

    # These two used internally
    index_map = {node: idx for idx, node in enumerate(shared_matrix.index)}  # 5373
    rev_index_map = {idx: node for idx, node in enumerate(shared_matrix.index)}

    # Keep track of each node's
    map_tracker = {node: {'idx': idx, 'edgeless': False, 'filtered': False, 'isolated': False} for idx, node in enumerate(shared_matrix.index)}

    # Set up dataframe for faster access
    annnotation_cols = ['GenomeName', 'RefSeqID', 'realm']

    fast_ref = reference_df[annnotation_cols].copy()
    fast_ref = fast_ref.set_index('RefSeqID', drop=False)  # For quicker conversion...
    fast_ref = fast_ref.loc[[k for k in index_map.keys() if k in fast_ref.index.tolist()]]  # Only those in dataset...
    # Difference between index_map.keys and refs are user sequences + ref domains not in contention
    fast_ref['csgraph'] = fast_ref.index.map(index_map)
    fast_ref = fast_ref.set_index('csgraph', drop=True)

    ref_dict = fast_ref['realm'].to_dict()

    def dict_lookup(dict_, vals):

        values = [dict_[k] for k in vals]

        return values

    def find_inner_indices(data):
        left_index, right_index = 0, len(data) - 1
        left_most, right_most = None, None
        last_left_value, last_right_value = None, None

        # Scan from the left to the right
        while left_index < len(data):
            if data[left_index] is not None:
                if last_left_value is None:
                    last_left_value = data[left_index]
                if data[left_index] == last_left_value:
                    left_most = left_index
            left_index += 1

        # Scan from the right to the left
        while right_index >= 0:
            if data[right_index] is not None:
                if last_right_value is None:
                    last_right_value = data[right_index]
                if data[right_index] == last_right_value:
                    right_most = right_index
            right_index -= 1

        return left_most, right_most

    # Extract the sparse scipy matrix
    coo_dist = distance_matrix.sparse.to_coo()
    coo_shared = shared_matrix.sparse.to_coo()

    # https://networkit.github.io/dev-docs/python_api/graph.html#networkit.graph.GraphFromCoo
    sharedG = nk.Graph(n=coo_shared.shape[0], weighted=True, directed=False)
    coo_shared.eliminate_zeros()  # Remove any existing zeros to avoid adding them as edges
    for i, j, v in zip(coo_shared.row, coo_shared.col, coo_shared.data):
        if i <= j:  # Avoid adding two edges for undirected graphs
            sharedG.addEdge(i, j, w=v, addMissing=True)

    # Remove self-loops
    sharedG.removeSelfLoops()
    logger.debug(f'There are {sharedG.numberOfEdges()} edges and {sharedG.numberOfNodes()} nodes in the shared graph')

    # Repeat for distance
    distG = nk.Graph(n=coo_dist.shape[0], weighted=True, directed=False)
    # Don't eliminate 0s, 0 dist is effectively identical
    for i, j, v in zip(coo_dist.row, coo_dist.col, coo_dist.data):
        if i <= j:
            distG.addEdge(i, j, w=v, addMissing=True)
    distG.removeSelfLoops()
    logger.debug(f'There are {distG.numberOfEdges()} edges and {distG.numberOfNodes()} nodes in the distance graph')

    # Identical between the two...
    edgeless = [rev_index_map[u] for u in sharedG.iterNodes() if sharedG.degree(u) == 0]  # Get node names
    for u in edgeless:
        map_tracker[u]['edgeless'] = True

    # Remove edges below threshold
    logger.info(f'Filtering edges at {min_shared} to reduce cross-realm connections')
    filtered_edges = []
    for (u, v, w) in sharedG.iterEdgesWeights():
        if w < min_shared:
            filtered_edges.append((u, v))

    for (u, v) in filtered_edges:
        sharedG.removeEdge(u, v)
        distG.removeEdge(u, v)  # Identical

    # Clean up
    sharedG.compactEdges()
    distG.compactEdges()

    filtered = [rev_index_map[u] for u in distG.iterNodes() if distG.degree(u) == 0]  # Get node names
    for u in filtered:

        if not map_tracker[u]['edgeless']:  # If it didn't have an edge
            map_tracker[u]['filtered'] = True

    if filtered_edges == 0:
        logger.warning('No edges were filtered')

    logger.info(f'Removed {len(filtered_edges)} edges. There are now {distG.numberOfEdges()} edges')

    iteration = 0
    while True:
        modified = False

        cc = nk.components.ConnectedComponents(distG)  # Due to loop, will eval entire graph every time
        cc.run()

        # Iterate over each component
        for component, component_nodes in enumerate(cc.getComponents()):

            ref_nodes = [v for v in component_nodes if v in ref_dict.keys()]

            # dict-based lookup 50X+ faster than df.loc[], and increases as more genomes are added
            dict_realms = dict_lookup(ref_dict, ref_nodes)
            realm_counts = Counter(r for r in dict_realms if r)

            if len(realm_counts) >= 2:

                # Two ways of dealing with cross-realm components: 1) raise edges until drop out 2) sever boundaries

                modified = True
                logger.info(f'Separating multiple-realm component {component} between {", ".join(realm_counts.keys())}')

                # Get indices of each realm
                realm_groups = {}
                for ref_node in ref_nodes:
                    realm = ref_dict[ref_node]
                    if realm:
                        if realm not in realm_groups:
                            realm_groups[realm] = []
                        realm_groups[realm].append(ref_node)

                # Check consistency of realm counts between "main" graph and subgraph
                for k, v in realm_groups.items():
                    if realm_counts[k] != len(v):
                        logger.warning(
                            f'Inconsistent realm counts detected! Original: {realm_counts[k]}, subgraph: {len(v)}')

                # Don't want Dijkstra for the entire graph
                subG = nk.graphtools.subgraphFromNodes(distG, component_nodes, compact=False)

                # Identify realm "groups"
                realms = list(realm_groups.keys())

                for (realm1, realm2) in combinations(realms, 2):
                    logger.debug(f'Separating {realm1} and {realm2}...')
                    nodes_group1 = realm_groups[realm1]
                    nodes_group2 = realm_groups[realm2]

                    cross_realm_edges = []

                    for u in nodes_group1:
                        for v in nodes_group2:
                            biDij = nk.distance.BidirectionalDijkstra(subG, u, v, True)
                            biDij.run()

                            path = biDij.getPath()

                            if path:
                                full_path = [u] + path + [v]
                                realm_path = [ref_dict[k] if k in ref_dict else None for k in full_path]

                                distWeights = [subG.weight(u1, v1) for u1, v1 in zip(full_path, full_path[1:])]
                                sharedWeights = [sharedG.weight(u1, v1) for u1, v1 in zip(full_path, full_path[1:])]

                                # Get end points, barring None
                                left_realm, right_realm = find_inner_indices(realm_path)

                                subpath_distances = distWeights[left_realm:right_realm + 1]

                                max_dist_index = left_realm + subpath_distances.index(max(subpath_distances))

                                left_edge = full_path[max_dist_index]
                                right_edge = full_path[max_dist_index + 1]

                                cross_realm_edges.append((left_edge, right_edge))

                    # Truly remove edges
                    logger.debug(f'There were {len(cross_realm_edges)} edges between {realm1} and {realm2}')

                    true_clip = 0
                    for (c1, c2) in cross_realm_edges:
                        if subG.hasEdge(c1, c2):
                            subG.removeEdge(c1, c2)
                        if distG.hasEdge(c1, c2):
                            distG.removeEdge(c1, c2)
                        true_clip += 1

                    logger.info(f'Removed {true_clip} cross-realm edges')

        logger.debug(f'Running iteration {iteration}')
        if not modified:
            break

        iteration += 1

    # Validate
    validate_cc = nk.components.ConnectedComponents(distG)
    validate_cc.run()

    # Double-check for homogeneity
    for new_component, new_component_nodes in enumerate(validate_cc.getComponents()):
        new_ref_nodes = [v for v in new_component_nodes if v in ref_dict.keys()]

        new_dict_realms = dict_lookup(ref_dict, new_ref_nodes)
        new_realm_counts = Counter(r for r in new_dict_realms if r)

        if len(new_realm_counts) >= 2:
            logger.warning(f'Multiple realms still detected in graph...')

    if not no_graphs:

        g_dist_fp = Path(str(g_out_fp) + '_dist.nkbg')
        logger.info(f'Writing NetworKit distance graph to {g_dist_fp}')
        nk.writeGraph(distG, str(g_dist_fp), fileformat=nk.Format.NetworkitBinary)

        g_shared_fp = Path(str(g_out_fp) + '_shared.nkbg')
        logger.info(f'Writing NetworKit shared genes graph to {g_shared_fp}')
        nk.writeGraph(sharedG, str(g_shared_fp), fileformat=nk.Format.NetworkitBinary)

    isolated = [rev_index_map[u] for u in distG.iterNodes() if distG.degree(u) == 0]  # Get node names

    # if not already edgeless
    for u in isolated:
        if not map_tracker[u]['edgeless']:  # If it didn't have an edge

            # And it wasn't already filtered
            if not map_tracker[u]['filtered']:
                map_tracker[u]['isolated'] = True

            # else it was isolated during filtering

        # else it was always going to be "isolated"

    with open(t_out_fp, 'wb') as f:
        pickle.dump(map_tracker, f)

    return distG, map_tracker  # Only "need" these, despite sharedG also being available


def build_igraph_network(distance_matrix: pd.DataFrame, shared_matrix: pd.DataFrame, out_fp: Path) -> ig.Graph:
    """
    Builds an igraph network from the provided distance and shared gene matrices.

    :param distance_matrix: A pandas DataFrame containing the distance matrix between genomes.
    :param shared_matrix: A pandas DataFrame containing the shared gene matrix between genomes.
    :param out_fp: The output file path where the igraph network will be saved.

    :return: An igraph network object.

    This function first converts the distance and shared gene matrices into networkx graphs, then merges them into a
    single graph. It then loads the resulting graph into an igraph object and writes it to the specified output file.

    The function also keeps track of isolates (genomes with no connections) and marks them as such in the igraph object.

    Note that this function does not remove isolates from the final igraph network. This is because removing isolates
    can significantly increase processing time, and it is often useful to keep track of isolates separately.

    The function uses the `write_igraphz` function to save the igraph network to a file in the igraph zipped format.
    """

    # Distance matrices maintain dtypes until "conversion" to networkx
    distance_S = distance_matrix.sparse.to_coo()
    shared_S = shared_matrix.sparse.to_coo()

    logger.info(f'Building distance and shared gene networks')
    distG = nx.from_scipy_sparse_array(distance_S, create_using=nx.Graph, parallel_edges=False, edge_attribute='distance')
    sharedG = nx.from_scipy_sparse_array(shared_S, create_using=nx.Graph, parallel_edges=False, edge_attribute='shared')
    logger.info(f'Finished building...')

    # Due to the nature of sparse matrices, and the fact they can self-match, all genomes should be in the network!
    logger.info(f'Building mapping between genomes and ids')
    mapping = dict(zip(range(len(distance_matrix.index.tolist())), distance_matrix.index.tolist()))  # genomes x genomes
    distG = nx.relabel_nodes(distG, mapping)  # Or else it's all ints
    sharedG = nx.relabel_nodes(sharedG, mapping)  # Or else it's all ints

    # Remove self-loops from both
    logger.info(f'Removing self-loops from both distance and shared gene networks')
    distG.remove_edges_from(list(nx.selfloop_edges(distG)))  # And this would be dist = 0.0
    sharedG.remove_edges_from(list(nx.selfloop_edges(sharedG)))  # Here there will be max shared genes

    # Remove the newfound isolates
    logger.info(f'Removing isolated nodes from distance and shared gene networks')
    isolates = list(nx.isolates(distG))
    # NOT removing isolates avoids separating out singletons to iterate through, and introduces the overhead of
    # needing to keep track of the isolates in a separate dataframe or list, BUT dramatically increases processing time

    logger.info(f'Merging shared gene and distance networkx')
    G = nx.compose(distG, sharedG)

    # At this point, a lot of memory is allocated
    del distG
    del sharedG

    logger.info(f'Loading into iGraph')
    g = ig.Graph.from_networkx(G)

    g.vs['name'] = g.vs['_nx_name']
    edgeless_vertices = g.vs.select(name_in=isolates)
    edgeless_indices = [v.index for v in edgeless_vertices]
    g.vs[edgeless_indices]['edgeless'] = True

    del G

    write_igraphz(g, out_fp)

    return g


def annotate_igraph_network(network: ig.Graph, ref_genomes_df: pd.DataFrame, out_fp: Path, verbose: bool = True):

    # TODO GenomeName does not exist for user sequences

    network.vs['name'] = network.vs['_nx_name']

    logger.info(f'Decorating network with reference information wherever possible...')
    annnotation_cols = ['GenomeName', 'RefSeqID', 'realm', 'superkingdom', 'kingdom', 'phylum', 'class',
                        'order', 'family', 'subfamily', 'genus']
    reference_df = ref_genomes_df[annnotation_cols]

    for ref, node_values in tqdm(reference_df.iterrows(), total=len(reference_df), unit=' references', disable=verbose):

        refid = node_values['RefSeqID']
        node_values = node_values.to_dict()

        try:
            node = network.vs.find(name=refid)

            for k, v in node_values.items():
                if not pd.isnull(v):
                    node[k] = v

        except ValueError:  # Not all references are in the network
            continue

    write_igraphz(network, out_fp)

    return network


def filter_distances(ag: ig.Graph, modifying_df, out_fp: Path, enhanced_sensitivity: bool, threshold: int = 3,
                     verbose: bool = True):

    # We want to keep distances, as they're useful to organize the upper ranks, but they also
    """

    :param ag: Annotated graph in igraph format
    :param modifying_df:
    :param out_fp:
    :param threshold: Minimum number of shared genes to be considered
    """

    logger.info(f'Filtering edges at {threshold} to reduce cross-realm connections')
    filtered_edges = [(e, e.source, e.target, e['distance'], e['shared']) for e in ag.es if e['shared'] < threshold]
    # This is the FIRST time that we filter distances. Benchmarks used ">= shared_min" so keeping consistency
    ag.delete_edges([e[0] for e in filtered_edges])
    if len(filtered_edges) == 0:
        logger.warning('No edges were filtered')
    logger.info(f'Removed {len(filtered_edges)} edges')

    logger.info('Identifying further cross-realm components potentially introduced by density-dependent...')
    connected_components = ag.connected_components(mode='weak')
    ag.vs['rawComponent'] = connected_components.membership  # Assign everyone a membership

    ugraphs = []

    filtered_pairs = []
    multi_realm_cc = 0
    for n, cc_members in enumerate(connected_components):

        realm_counts = Counter(r for r in ag.vs[cc_members]['realm'] if r)

        subgraph = ag.subgraph(cc_members)

        if not realm_counts:  # Entirely unknown?
            ugraphs.append(subgraph)

        elif len(realm_counts) == 1:  # Only one realm
            ugraphs.append(subgraph)

        elif len(realm_counts) >= 2:  # Will 3 create situations where some parts more affected than others

            multi_realm_cc += 1

            if subgraph.ecount() == 0:
                logger.warning(f'rawComponent {n} has 0 edges. Graph is inconsistent!')

            i_thres = 3

            while True:

                # These edges are only valid WITHIN this component. When they get unioned, their index will change
                filtered_pair = [
                    (e, subgraph.vs[e.source]['name'], subgraph.vs[e.target]['name'], e['distance'], e['shared']) for e in
                    subgraph.es if e['shared'] <= i_thres]
                filtered_pairs.extend(filtered_pair)

                # Delete edges...
                subgraph.delete_edges([e[0] for e in filtered_pair])

                multi_realms = 0
                for pruned_mem in subgraph.connected_components(mode='weak'):
                    pruned_counts = Counter(r for r in subgraph.vs[pruned_mem]['realm'] if r)

                    if len(pruned_counts) > 1:  # Can't be less than
                        multi_realms += 1

                if multi_realms == 0:
                    break

                # else, there ARE multiple realms and need to remove even more
                i_thres += 1

            ugraphs.append(subgraph)

    ugraph = ig.disjoint_union(ugraphs)

    updated_connected_components = ugraph.connected_components(mode='weak')
    ugraph.vs['preFixComponent'] = updated_connected_components.membership  # Assign everyone a membership

    def validator(graph: ig.Graph):

        for i, i_members in enumerate(graph.connected_components(mode='weak')):

            i_realms = Counter(r for r in graph.vs[i_members]['realm'] if r)
            if len(i_realms) >= 2:
                logger.warning(f'preFixComponent {i} has {len(i_realms)} realms')

    validator(ugraph)

    # Repair edges that are direct references
    logger.info('Repairing edges removed during multi-realm component analysis')
    edges_to_add = []
    edges_to_resolve = []
    for iv_, u, v, w, s in tqdm(filtered_pairs, total=len(filtered_pairs), unit=' filtered edges', disable=verbose):

        u_r, v_r = ugraph.vs.find(name=u)['realm'], ugraph.vs.find(name=v)['realm']

        if pd.isnull(u_r) or pd.isnull(v_r):  # If either is unknown, we're not checking for this yet
            edges_to_resolve.append((u, v, w, s))

        elif u_r == v_r:  # Don't matter what strength, they're references to each other
            edges_to_add.append((u, v, w, s))

        else:
            edges_to_resolve.append((u, v, w, s))  # Could just {}.subset{}, but would require adding another to list

    logger.info(f'Recovering {len(edges_to_add)} realm-realm edges lost during filtering...')
    es_end = ugraph.ecount()
    ugraph.add_edges([(e[0], e[1]) for e in edges_to_add])
    ugraph.es[es_end:]['distance'] = [e[2] for e in edges_to_add]
    ugraph.es[es_end:]['shared'] = [e[3] for e in edges_to_add]

    def iterative_repair(graph: ig.Graph, to_filter_edges: list, verbose: bool = True):

        """
        Reset each time, unless vertices are deleted, should be maintained

        """

        component_to_realm = {}  # Set up dict connecting component to realm
        member_to_component = {}  # Set up dict connecting member to component
        for i_component, component in enumerate(graph.connected_components(mode='weak')):
            revised_counts = Counter([r for r in graph.vs[component]['realm'] if r])

            if len(revised_counts) > 1:
                logger.error('Warning: Identified multiple realms within subcomponent: ', revised_counts)
                exit(1)

            if revised_counts:
                component_to_realm[i_component] = revised_counts.most_common(1)[0][0]
            else:
                component_to_realm[i_component] = np.nan

            for member in component:
                member_name = graph.vs[member]['name']
                member_to_component[member_name] = i_component

        to_add_edges = []
        for u, v, w, s in tqdm(to_filter_edges[::-1], total=len(to_filter_edges), unit=' iter edges', disable=verbose):

            # Get components
            u_c = member_to_component[u]
            v_c = member_to_component[v]
            # Get component realms
            u_c_realm = component_to_realm[u_c]
            v_c_realm = component_to_realm[v_c]

            if u_c_realm == v_c_realm:  # Two components, now merged, have "added in" references
                to_add_edges.append((u, v, w, s))

            elif pd.notnull(u_c_realm) and pd.notnull(v_c_realm):
                if u_c_realm != v_c_realm:  # Don't want to merge components if each are of different realms
                    continue
                else:
                    logger.warning(f'Unable to identify realms. Both exist, but are neither the same nor different')
                    logger.warning(u, u_c, u_c_realm, v, v_c, v_c_realm)

            elif pd.notnull(u_c_realm) or pd.notnull(v_c_realm):  # Not both
                # It's possible for edges DURING ITERATION to connect ref-unknown and that same unknown-diff ref
                # Let the specialized function handle these cases
                continue

            elif pd.isnull(u_c_realm) and pd.isnull(v_c_realm):
                continue  # Don't want to merge two subcomponents that have no realm

            else:
                logger.warning(f'Unable to identify {u} and {v}, with {u_c} and {v_c}, and {u_c_realm} and {v_c_realm}')
                continue

        if not to_add_edges:  # Continue until no more components can be merged
            return to_filter_edges

        else:
            logger.info(f'Repairing {len(to_add_edges)} edges from filtering...')
            ee_end = graph.ecount()
            graph.add_edges([(e[0], e[1]) for e in to_add_edges])
            graph.es[ee_end:]['distance'] = [e[2] for e in to_add_edges]
            graph.es[ee_end:]['shared'] = [e[3] for e in to_add_edges]

            return list(set(to_filter_edges) - set(to_add_edges))

    # Round 1 (after reference-reference)
    iteration = 0
    while True:

        logger.info(f'Processing repair iteration {iteration}, starting with {len(edges_to_resolve)} edges')
        remaining_edges = iterative_repair(ugraph, edges_to_resolve, verbose)  # How many remains

        if len(remaining_edges) == len(edges_to_resolve):  # Hasn't decreased
            break

        iteration += 1  # Update remaining to include how many the current iteration has remaining
        edges_to_resolve = remaining_edges

    validator(ugraph)

    # The question is, how to repair components that could connect to other components that are connected to diff realms?
    # Problem is that DURING ITERATION, could connect two different realm components through an unknown intermediate
    # So, basically, repeat iterative_repair, but add edges one-by-one

    logger.debug(f'Writing temporary output graph')
    write_igraphz(ugraph, out_fp.parent / f'{out_fp.name.replace(".pkl.gz", "_temp.pkl.gz")}')

    edges_to_resolve.sort(key=lambda x: x[2])

    def iterative_repair_2nd(graph: ig.Graph, to_filter_edges: list):

        """
        A variant of iterative_repair, but instead of adding edges in batch (i.e. the faster and "how to do it" way),
        this repair approach adds edges sequentially, and then updates the mapping dictionaries. This is to control for
        the fact that adding an edge between two components can/will "guilt" a component into assuming the realm of its
        larger merging component partner. But if that guilted component is also connected to another component with a
        different realm... then that component would suddenly now connect two differing realms. While this does -
        biologically - occur, it complicates assignments.

        :param graph: iGraph graph that must have the "name" attribute and at least one "realm" attribute
        :param to_filter_edges: List of edges (of u, v, s, w) to be added to the graph
        """

        # Set up dict connecting member to component
        # Set up dict connecting component to realm
        component_to_realm = {}  # component_num: realm
        member_to_component = {}  # name: component_num
        component_counts = Counter()  # component_num: count
        for component_i, component in enumerate(graph.connected_components(mode='weak')):
            revised_counts = Counter([r for r in graph.vs[component]['realm'] if r])

            if len(revised_counts) > 1:
                logger.warning('Identified multiple realms of subcomponent: ', revised_counts, ' graph is inconsistent')
                exit(1)

            if revised_counts:
                component_to_realm[component_i] = revised_counts.most_common(1)[0][0]
            else:
                component_to_realm[component_i] = np.nan

            for mem in component:
                mem_name = graph.vs[mem]['name']
                member_to_component[mem_name] = component_i

            component_counts[component_i] = len(component)

        to_add_edge = None
        for u, v, w, s in tqdm(to_filter_edges[::-1], total=len(to_filter_edges), unit=' final edges', disable=verbose):

            u_c = member_to_component[u]
            v_c = member_to_component[v]

            u_c_realm = component_to_realm[u_c]
            v_c_realm = component_to_realm[v_c]

            if u_c_realm == v_c_realm:
                to_add_edge = (u, v, w, s)

            elif pd.notnull(u_c_realm) and pd.notnull(v_c_realm):  # AND
                if u_c_realm != v_c_realm:
                    continue  # DON'T connect
                else:  # TODO In neither case would we be adding edges, should this whole block be skipped?
                    continue

            elif pd.notnull(u_c_realm) or pd.notnull(v_c_realm):  # Not both
                to_add_edge = (u, v, w, s)

            elif pd.isnull(u_c_realm) and pd.isnull(v_c_realm):
                continue  # Don't want to merge two subcomponents that have no realm

            else:
                print('Unknown groups', u, u_c, u_c_realm, v, v_c, v_c_realm)

            if to_add_edge:
                u_e, v_e, w_e, s_e = to_add_edge
                graph.add_edge(u_e, v_e, shared=s_e, weight=w_e)

                # Rebuild dicts
                if u_c != v_c:  # If the components are not the same
                    # Everyone in one of the components now becomes members of the other
                    greater_component = u_c if component_counts[u_c] >= component_counts[v_c] else v_c
                    lesser_component = v_c if component_counts[v_c] < component_counts[u_c] else u_c

                    # == between component_counts does occur, though it is generally a rare occurrence

                    # Update counts
                    component_counts[greater_component] += component_counts[lesser_component]
                    del component_counts[lesser_component]

                    # Remove component that no longer exists
                    # del component_to_realm[lesser_component]  # FUTURE vertices don't know component no longer exists

                    # Update assignments - ONLY need two, only one edge is added
                    member_to_component[u] = greater_component
                    member_to_component[v] = greater_component

                else:  # One of the previous vertex connected two components, and now they're in the same component
                    continue
                    # No need to change memberships

        return

    if enhanced_sensitivity:
        logger.info(f'Enabling enhanced sensitivity')
        iterative_repair_2nd(ugraph, edges_to_resolve)

    # And finally, modify dataframe
    logger.info(f'Modifying annotation data and writing final graph')

    isolates = ugraph.vs.select(_degree_eq=0)
    isolates_indices = [v.index for v in isolates]
    ugraph.vs[isolates_indices]['isolated'] = True

    if not modifying_df.empty:  # TODO Use graph directly, instead of another connected component enumeration
        for i, members in enumerate(ugraph.connected_components(mode='weak')):
            ugraph.vs[members]['component'] = str(i)
            members = ugraph.vs[members]['name']
            modifying_df.loc[members, 'component'] = str(i)

    # ugraph.vs['component'] = ugraph.connected_components(mode='weak').membership

    write_igraphz(ugraph, out_fp)

    return ugraph
