#!/usr/bin/env python
import sys
import os
import logging
import re
import subprocess
import tempfile
import multiprocessing as mp
import shutil
from collections import defaultdict
from typing import Tuple, Dict

import screed
import pandas as pd
import numpy as np
import scipy

from vcontact3.utilities import (
        tqdm_log, check_dependencies, canonical_gid, Taxonomy
)
from vcontact3.config import (
        FASTANI_GENOMES, FASTANI_GENOMES_EXT, TAXONOMY_FILE, 
        AF_THRESHOLD, SPECIES_MSH_DIRNAME, ANI_SPECIES_THRESHOLD, 
        QUERY_EACH_GENOME_AS_FILE, ANI_SUMMARY_COLS, 
        ANI_CLOSEST_COLS, REF_GENOME_SKANI
)

REF_SKANI_SKETCH_DB = '/fs/ess/PAS1117/jiarong/dev/vcontact3/tests/ref_data/genomes.skani'
SKANI_M=200
SKANI_C=125
SKANI_S=80

MAX_FILES_PER_DIR = 1000
NO_MASH=True
MAX_D =  0.2 
MASH_K = 21
MASH_V = 1.0
MASH_S = 1000
REF_GENOME_MSH = None

# get logger for the whole module, ie. species
logging.basicConfig(force=True)
logging.getLogger().setLevel(logging.DEBUG)
logger = logging.getLogger(__name__)

def species(seqfiles, out_dir, skip_votu=False, each_genome_as_file=False, extension=None, cpus=1): 
    out_dir= os.path.realpath(os.path.expanduser(out_dir))
    os.makedirs(out_dir, exist_ok=True)

    if not each_genome_as_file:
        if not skip_votu:
            # votu clustering
            qry_dist_mat = f'{out_dir}/query.dist_square_mat.tsv'
            args = ['skani', 'triangle', '--distance', '--full-matrix', '-i', '-s', f'{SKANI_S}', '-m', f'{SKANI_M}', '-c', f'{SKANI_C}', '--min-af', f'{AF_THRESHOLD*100}']
            args.extend(seqfiles)
            args.extend(['>', qry_dist_mat])

            logger.debug(' '.join(args))
            #print(' '.join(args))

            qry_af_mat = f'{out_dir}/query.af_square_mat.tsv'
            with open(qry_dist_mat, 'w') as fw:
                proc = subprocess.Popen(args, stdout=fw, stderr=subprocess.PIPE, encoding='utf-8')
                stdout, stderr = proc.communicate()

                if os.path.exists('skani_matrix.af'):
                    shutil.move('skani_matrix.af', qry_af_mat)
                if proc.returncode != 0:
                    logger.error('species module STDERR:\n' + stderr)
                    raise Exception('species module: skani triangle returned a non-zero exit code.')

            # skani sketch use the input file path as genome name in --ql mode
            #   eg. ../ref_data/genomes/GCF_000916495.1_ViralProj240050_genomic.fna.gz 
            pw_dist_df = pd.read_csv(qry_dist_mat, sep='\t', skiprows=1, header=None, index_col=0)
            #print(pw_dist_df)
            # skani take the whole header as name in -i mode
            names = [header.split(None, 1)[0] for header in pw_dist_df.index]
            arr = np.array(pw_dist_df)
            linkage = scipy.cluster.hierarchy.linkage(arr, method='average')
            # Form clusters 
            fclust = scipy.cluster.hierarchy.fcluster(linkage, t=ANI_SPECIES_THRESHOLD, criterion='distance')
            # Make Cdb from fclust
            c_db = pd.DataFrame({'genome': names, 'votu_cluster': fclust})

            # each genome as seq
            d_size = {}
            for seqfile in seqfiles:
                with screed.open(seqfile) as sh:
                    for rec in sh:
                        header = rec.name
                        name = header.split(None, 1)[0]
                        d_size[name] = len(rec.sequence)

            c_db['genome_size'] = c_db['genome'].map(d_size)

            # pick longest genome for each cluster
            _df = c_db.groupby('votu_cluster').apply(lambda x: x.sort_values('genome_size', ascending=False).head(n=1))
            _d = dict(zip(_df['votu_cluster'],  _df['genome']))
            c_db['votu_repr']  = c_db['votu_cluster'].map(_d)

            # write fna for votu reps
            votu_reps = set(c_db['votu_repr'])
            votu_rep_f = f'{out_dir}/votu.repr.fna'
            with open(votu_rep_f, 'w') as fw:
                for seqfile in seqfiles:
                    with screed.open(seqfile) as sh:
                        for rec in sh:
                            header = rec.name
                            name = header.split(None, 1)[0]
                            if not name in votu_reps:
                                continue
                            seq = rec.sequence
                            mes = f'>{header}\n{seq}\n'
                            fw.write(mes)

            votu_seqfiles = [votu_rep_f,]

        else:
            # skip_votu == True
            votu_seqfiles = seqfiles

            # each genome as seq
            names = []
            sizes = []
            for seqfile in seqfiles:
                with screed.open(seqfile) as sh:
                    for rec in sh:
                        header = rec.name
                        name = header.split(None, 1)[0]
                        names.append(name)
                        sizes.append(len(rec.sequence))

            # mimic Cdb from votu clustering
            c_db = pd.DataFrame({'genome': names, 'votu_cluster': ['NA']* len(names), 'genome_size': sizes})
            c_db['votu_repr']  = names

        # find the closest ref for each otu rep
        votu2ref_ani_f = f'{out_dir}/votu2ref.tsv'
        # -c -m is set in ref sketch
        ref_sketch_db = REF_SKANI_SKETCH_DB

        args = ['skani', 'search', '--qi', '-s', f'{SKANI_S}']
        args.extend(['-t', f'{cpus}'])
        args.extend(['-d', ref_sketch_db])
        args.extend(['-o', votu2ref_ani_f])
        args.extend(votu_seqfiles)

        logger.debug(' '.join(args))
        #print(' '.join(args))
        proc = subprocess.Popen(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE, encoding='utf-8')
        stdout, stderr = proc.communicate()

        if proc.returncode != 0:
            logger.error('species module STDOUT:\n' + stdout)
            logger.error('species module STDERR:\n' + stderr)
            raise Exception('species module: skani search returned a non-zero exit code.')

        # decide to use the df instead of dict
        #  load the df; sort by query, ANI*AF and take the top hit; assign taxon; filter by ANI (95) and AF (80); 
        df_votu2ref = pd.read_csv(votu2ref_ani_f, sep='\t', header=0)
        # cols: Ref_file | Query_file | ANI | Align_fraction_ref | Align_fraction_query | Ref_name | Query_name
        cols = df_votu2ref.columns.tolist()
        qry_idx = 6
        ref_idx = 0
        ani_idx = 2
        af_ref_idx = 3
        af_qry_idx = 4
        qry_colname = cols[qry_idx]
        ref_colname = cols[ref_idx]
        ani_colname = cols[ani_idx]
        af_ref_colname = cols[af_ref_idx]
        af_qry_colname = cols[af_qry_idx]

        # skani use the input file path as genome name in genome as file mode
        #   need to convert to basename to be consistent w/ TAXONOMY_FILE
        df_votu2ref[ref_colname] = [os.path.basename(f) for f in df_votu2ref[ref_colname]]
        # skani use the fasta header as genome in genome as seq mode
        #   need to convert to name
        df_votu2ref[qry_colname] = [header.split(None, 1)[0] for header in df_votu2ref[qry_colname]]
        # take the larger of frac_ref and frac_qry as af, ie. the aligned fraction of shorter seq
        df_votu2ref['af_max'] = [max(row.loc[af_ref_colname], row.loc[af_qry_colname]) for idx, row in df_votu2ref.iterrows()]
        df_votu2ref['ani_x_af_max'] = df_votu2ref[ani_colname] * df_votu2ref['af_max']
        lst = [] 
        for qry, sub_df in df_votu2ref.groupby(qry_colname):
            sub_df_sort = sub_df.sort_values(ascending=False, by=['ani_x_af_max', 'af_max', ani_colname])
            toprow = sub_df_sort.head(n=1)
            lst.append(toprow)

        df_closest_ref = pd.concat(lst)

        # assign taxonomy
        # cols: id | lineage
        with open(TAXONOMY_FILE) as fh:
            d_taxonomy = dict(line.rstrip().split('\t', 1) for line in fh)

        #print(d_taxonomy)
        df_closest_ref['lineage'] = [d_taxonomy[i] for i in df_closest_ref[ref_colname]]

        sel1 = df_closest_ref['af_max'] >= AF_THRESHOLD
        sel2 = df_closest_ref[ani_colname] >= ANI_SPECIES_THRESHOLD

        sel = sel1 & sel2
        df_closest_ref['species_assigned'] = sel

        votu_rep_w_species = set(df_closest_ref.loc[sel, :][qry_colname])
        votu_rep_w_uncl_species_f = f'{out_dir}/votu.repr.unclassified_species.fna'
        with open(votu_rep_w_uncl_species_f, 'w') as fw:
            for f in votu_seqfiles:
                with screed.open(f) as sh:
                    for rec in sh:
                        header = rec.name
                        name = header.split(None, 1)[0]
                        if name in votu_rep_w_species:
                            continue
                        seq = rec.sequence
                        mes = f'>{header}\n{seq}\n'
                        fw.write(mes)

        df_species = c_db.merge(df_closest_ref, how='left', left_on='votu_repr', right_on=qry_colname)

        species_outfile = f'{out_dir}/species.tsv'
        df_species.to_csv(species_outfile, sep='\t', index=False, na_rep='NA')
        return df_species

    # QUERY_EACH_GENOME_AS_FILE == True
    else:
        names = []
        sizes = []
        for seqfile in seqfiles:
            name = os.path.basename(seqfile)
            if extension:
                name = name.rsplit(extension, 1)[0]
            names.append(name)
            with screed.open(seqfile) as sh:
                size = sum([len(rec.sequence) for rec in sh])
                sizes.append(size)

        df = pd.DataFrame({'genome': names, 'genome_size': sizes, 'filepath': seqfiles})

        if not skip_votu:
            # votu clustering
            qry_dist_mat = f'{out_dir}/query.dist_square_mat.tsv'
            args = ['skani', 'triangle', '--distance', '--full-matrix', '-s', f'{SKANI_S}', '-m', f'{SKANI_M}', '-c', f'{SKANI_C}', '--min-af', f'{AF_THRESHOLD*100}']
            args.extend(seqfiles)
            args.extend(['>', qry_dist_mat])

            logger.debug(' '.join(args))
            #print(' '.join(args))

            qry_af_mat = f'{out_dir}/query.af_square_mat.tsv'
            with open(qry_dist_mat, 'w') as fw:
                proc = subprocess.Popen(args, stdout=fw, stderr=subprocess.PIPE, encoding='utf-8')
                stdout, stderr = proc.communicate()

                if os.path.exists('skani_matrix.af'):
                    shutil.move('skani_matrix.af', qry_af_mat)

                if proc.returncode != 0:
                    logger.error('species module STDERR:\n' + stderr)
                    raise Exception('species module: skani triangle returned a non-zero exit code.')

            pw_dist_df = pd.read_csv(qry_dist_mat, sep='\t', skiprows=1, header=None, index_col=0)
            #print(pw_dist_df)
            # skani take the file path as name in the mode w/o -i
            names = [df.loc[df['filepath'] == i, 'genome'].iloc[0] for i in pw_dist_df.index]
            arr = np.array(pw_dist_df)
            linkage = scipy.cluster.hierarchy.linkage(arr, method='average')
            # Form clusters 
            fclust = scipy.cluster.hierarchy.fcluster(linkage, t=ANI_SPECIES_THRESHOLD, criterion='distance')
            # Make Cdb from fclust
            c_db = pd.DataFrame({'genome': names, 'votu_cluster': fclust})

            # each genome as file
            c_db = c_db.merge(df, how='left', on='genome')

            # pick longest genome for each cluster
            _df = c_db.groupby('votu_cluster').apply(lambda x: x.sort_values('genome_size', ascending=False).head(n=1))
            _d = dict(zip(_df['votu_cluster'],  _df['genome']))
            c_db['votu_repr']  = c_db['votu_cluster'].map(_d)

            votu_reps = set(c_db['votu_repr'])
            votu_seqfiles = _df['filepath'].tolist()

        else:
            # skip_votu == True
            votu_seqfiles = seqfiles

            #df = pd.DataFrame({'genome': names, 'genome_size': sizes, 'filepath': seqfiles})
            c_db = df.loc[:, ['genome', 'genome_size']]
            c_db['votu_cluster'] = 'NA'
            c_db['votu_repr'] = c_db['genome']
            c_db = c_db.loc[:, ['genome', 'votu_cluster', 'genome_size', 'votu_repr']]
            votu_reps = set(c_db['votu_repr'])

        # find the closest ref for each otu rep
        votu2ref_ani_f = f'{out_dir}/votu2ref.tsv'
        # -c -m is set in ref sketch
        ref_sketch_db = REF_SKANI_SKETCH_DB

        args = ['skani', 'search', '-s', f'{SKANI_S}']
        args.extend(['-t', f'{cpus}'])
        args.extend(['-d', ref_sketch_db])
        args.extend(['-o', votu2ref_ani_f])
        args.extend(votu_seqfiles)

        logger.debug(' '.join(args))
        #print(' '.join(args))
        proc = subprocess.Popen(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE, encoding='utf-8')
        stdout, stderr = proc.communicate()

        if proc.returncode != 0:
            logger.error('species module STDOUT:\n' + stdout)
            logger.error('species module STDERR:\n' + stderr)
            raise Exception('species module: skani search returned a non-zero exit code.')

        # decide to use the df instead of dict
        #  load the df; sort by query, ANI*AF and take the top hit; assign taxon; filter by ANI (95) and AF (80); 
        df_votu2ref = pd.read_csv(votu2ref_ani_f, sep='\t', header=0)
        # cols: Ref_file | Query_file | ANI | Align_fraction_ref | Align_fraction_query | Ref_name | Query_name
        cols = df_votu2ref.columns.tolist()
        qry_idx = 1
        ref_idx = 0
        ani_idx = 2
        af_ref_idx = 3
        af_qry_idx = 4
        qry_colname = cols[qry_idx]
        ref_colname = cols[ref_idx]
        ani_colname = cols[ani_idx]
        af_ref_colname = cols[af_ref_idx]
        af_qry_colname = cols[af_qry_idx]

        # skani use the input file path as genome name in genome as file mode
        #   need to convert to basename to be consistent w/ TAXONOMY_FILE
        df_votu2ref[ref_colname] = [os.path.basename(f) for f in df_votu2ref[ref_colname]]
        # skani use the file path as genome in "genome as file" mode
        #   need to convert to name
        df_votu2ref[qry_colname] = [df.loc[df['filepath'] == i, 'genome'].iloc[0] for i in df_votu2ref[qry_colname]]
        # take the larger of frac_ref and frac_qry as af, ie. the aligned fraction of shorter seq
        df_votu2ref['af_max'] = [max(row.loc[af_ref_colname], row.loc[af_qry_colname]) for idx, row in df_votu2ref.iterrows()]
        df_votu2ref['ani_x_af_max'] = df_votu2ref[ani_colname] * df_votu2ref['af_max']
        lst = [] 
        for qry, sub_df in df_votu2ref.groupby(qry_colname):
            sub_df_sort = sub_df.sort_values(ascending=False, by=['ani_x_af_max', 'af_max', ani_colname])
            toprow = sub_df_sort.head(n=1)
            lst.append(toprow)

        df_closest_ref = pd.concat(lst)

        # assign taxonomy
        # cols: id | lineage
        with open(TAXONOMY_FILE) as fh:
            d_taxonomy = dict(line.rstrip().split('\t', 1) for line in fh)

        #print(d_taxonomy)
        df_closest_ref['lineage'] = [d_taxonomy[i] for i in df_closest_ref[ref_colname]]

        sel1 = df_closest_ref['af_max'] >= AF_THRESHOLD
        sel2 = df_closest_ref[ani_colname] >= ANI_SPECIES_THRESHOLD

        sel = sel1 & sel2
        df_closest_ref['species_assigned'] = sel
        cl_st = set(df_closest_ref.loc[sel, qry_colname])
        uncl_st = set(votu_reps) - cl_st
        #print(uncl_st)
        uncl_tab = df.loc[df['genome'].isin(uncl_st), ['genome', 'filepath']]
        votu_rep_w_uncl_species_f = f'{out_dir}/votu.repr.unclassified_species.tsv'
        uncl_tab.to_csv(votu_rep_w_uncl_species_f, sep='\t', index=False, na_rep='NA')

        df_species = c_db.merge(df_closest_ref, how='left', left_on='votu_repr', right_on=qry_colname)

        species_outfile = f'{out_dir}/species.tsv'
        df_species.to_csv(species_outfile, sep='\t', index=False, na_rep='NA')
        return df_species

    ### combine the votu cluster and species cluster, and the size of final cluster can be use for network visualization to expand the rep seq
