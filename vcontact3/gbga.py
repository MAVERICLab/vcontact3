"""
Guilt-by-Genome-Association. Taxonomic classification
"""

import gzip
import itertools
import logging
import pickle
import warnings
from collections import Counter
from pathlib import Path
import h5py
from sklearn.metrics.pairwise import pairwise_distances

import numpy as np
import pandas as pd
from scipy.cluster.hierarchy import linkage, fcluster
from scipy.spatial.distance import squareform
from scipy import sparse

from vcontact3.config import ranks, all_ranks, not_implemented, realms
from vcontact3.distances import distances
from . import resolver as resolvers

from tqdm import tqdm

warnings.filterwarnings("ignore")

logger = logging.getLogger(__name__)

# TODO All "default" realms should still be considered default, but placed in separate category
# TODO ensure that all No Realm --> No prediction


class GBGA(object):

    def __init__(self,
                 resolver,
                 ref_db: dict,
                 ref_fp: Path,
                 ref_domain: str,
                 metric: str,
                 predict_type: str,
                 default_realm: str,  # Must be one of implemented realms
                 min_shared_genes: int,
                 threads: int,
                 output_dir: Path,
                 reduce_memory: bool = False,
                 verbose: bool = True,
                 ):

        verbose = not verbose

        ref_version = ref_fp.stem
        ref_dir = ref_fp.parent

        if isinstance(resolver, resolvers.Resolver):
            # From Resolver, we need to get the predicted realms for each of the user sequences
            self.genomes_overview = resolver.genomes_overview
            self.genomes_overview = self.genomes_overview.drop(columns='genome_id')  # Duplicate with index
            # Will be populated as processing each genome

            # Resolver only has two real outputs, the overviews and components at each identity
            self.identities = resolver.identities  # Dict with identities: {'genomes_overview':..., 'Components':...}

        else:
            logger.error("This is not a valid resolver object. Please send this error to the developers.")

        if reduce_memory:
            logger.info(f'Enabling low-memory mode for agglomerative clustering...')

        self.metric = metric
        self.min_shared_genes = min_shared_genes
        self.threads = threads
        self.output_dir = output_dir

        # Taxonomy for pseudo-results
        self.ref_db_fp = ref_dir / ref_db[ref_version]["Domains"][ref_domain]['Genomes Report']
        ref_db_df = pd.read_parquet(self.ref_db_fp)

        if ref_domain == 'prokaryotes':
            ref_domain = 'bacteria'  # They are treated identically

        if ref_domain == 'eukaryotes':
            ref_domain = 'eukaryota'

        # Iterate through all identities, 3-4 (of the 5) need to be done so anyway
        # Go through identities 1st, so that the matrices get loaded once. THEN go through components
        # THEN check ranks. Any other combination of loops will take longer

        identities = [0.3, 0.4, 0.5, 0.6, 0.7]

        # There are 3 (or more?) means of assigning taxonomy
        # TOP-DOWN - where assignments begin at the realm, then get divided by the components within each rank working
        # downward. This effectively removes the chance that a member of a lower rank could be assigned to a different
        # rank group of a sister rank
        # NAIVE -

        # Okay, identified rank (component) dropping bug. If realm can't be predicted for an identity, there wouldn't be
        # an assignment, as v3 won't have a realm to pull distance thresholds from

        # Instead - and obviously - every sequence can only be from one realm, so why not get all the predictions and
        # assign the overall best realm

        # Resolver assigns realms to viruses based on their component, so each component should be a mono realm WITHIN
        # an identity

        # AGGREGATE realms and realm predictions from every identity
        for identity in identities:
            identity = str(identity)

            genomes_overview_df = self.identities[identity]['genomes_overview']  # + Realms (x3) + realm prediction
            genomes_overview_df = genomes_overview_df.drop(columns='genome_id')  # Duplicate with index

            components_df = self.identities[identity]['components']  # Component composition; References, User, Realms,

            # For every identity, for every component, determine the realm prediction to use
            for component, component_df in genomes_overview_df.groupby(by='component'):

                ref_realms = components_df.loc[component, 'Realms']  # Prediction based on references

                # Prediction based on shared genes and compared across realms --> TODO Is this always a single term?
                realm_predictions = [c for c in component_df['realm prediction'].unique().tolist() if pd.notnull(c)][0]

                component = str(component)
                component_members = component_df.index.tolist()

                # Update and track
                self.genomes_overview.loc[component_members, f'component ({identity})'] = component
                self.genomes_overview.loc[component_members, f'component size ({identity})'] = len(component_df)
                self.genomes_overview.loc[component_members, f'Realm Reference ({identity})'] = ref_realms
                # Predictions prioritizes references, then falls back to shared genes
                self.genomes_overview.loc[component_members, f'Realm Predictions ({identity})'] = realm_predictions

            # Add in singletons?
            true_singletons = self.identities[identity]['edgeless']
            post_clean_singletons = self.identities[identity]['isolated']

            self.genomes_overview.loc[true_singletons, f'network ({identity})'] = 'edgeless'
            self.genomes_overview.loc[post_clean_singletons, f'network ({identity})'] = 'isolated'

        # Now that everything from all identities have been aggregated, can "predict" realm for each sequence
        realm_ref_cols = itertools.product(['Realm Reference'], identities)
        realm_ref_cols = [f'{a} ({b})' for a, b in realm_ref_cols]

        realm_pred_cols = itertools.product(['Realm Predictions'], identities)
        realm_pred_cols = [f'{a} ({b})' for a, b in realm_pred_cols]

        def aggregate_predictions(ref_array: np.ndarray, gene_array: np.ndarray):
            # Ensure present
            refs = [v for v in ref_array if pd.notna(v)]
            if any('|' in r for r in refs):
                refs = [r.split('|') if '|' in r else [r] for r in refs]
                refs = list(itertools.chain.from_iterable(refs))

            refs = Counter(refs)
            refs_keys = list(refs.keys())

            if len(refs_keys) == 1:
                return refs_keys[0], 'reference'  # TODO Add in count support

            if len(refs_keys) >= 1:

                if set(refs_keys).issubset({'Adnaviria', 'Varidnaviria'}):
                    return 'Adnaviria|Varidnaviria', 'references'
                    # TODO Causes Distances to require 'Adnaviria|Varidnaviria'

                # No longer "forcing" Adnaviria and Varidnaviria to be treated as 'Adnaviria|Varidnaviria'
                # If, in the future, there is a better way to differentiate, then this won't have to be rewritten

                else:  # In any other case where there's multiple, will need to inspect manually, but majority rules...
                    maj_ref, maj_ref_count = refs.most_common(1)[0]
                    ref_groups = [f'{k}({v})' for k, v in refs.most_common()]

                    # It is possible for this situation to create conflicts within a component
                    # 'Duplodnaviria' and 'Monodnaviria' are possible at large scale, but majority is correct in 100%

                    return maj_ref, f'reference conflicts ({"|".join(ref_groups)})'

            if not refs:  # i.e. 0

                genes = [v for v in gene_array if pd.notna(v)]

                if genes:  # If realm unique genes

                    # Is "No prediction" a valid prediction?
                    if any('|' in r for r in genes):
                        genes = [r.split('|') if '|' in r else [r] for r in genes]
                        genes = list(itertools.chain.from_iterable(genes))

                    genes = Counter(genes)
                    genes_keys = set(genes.keys())

                    maj_gene, maj_count = genes.most_common(1)[0]
                    gene_groups = [f'{k}({v})' for k, v in genes.most_common()]

                    # OKAY, had to simplify the 100 lines of logic
                    if len(genes_keys) == 1:
                        if maj_gene == 'No prediction':  # and it's no help...
                            return 'No Realm', 'None'
                        else:
                            return maj_gene, maj_count

                    elif len(genes_keys) >= 1:  # Can't be 0 or else would be None

                        if len(genes_keys) == 2:

                            # Of course, the one exception
                            if genes_keys.issubset({'Adnaviria', 'Varidnaviria'}):
                                return 'Adnaviria|Varidnaviria', sum(genes.values())

                            # Handle all the 'No prediction' cases
                            # else?
                            for realm in realms:
                                if genes_keys.issubset({realm, 'No prediction'}):
                                    return realm, genes[realm]  # Only count the realm

                            # Else, conflict
                            return f'{maj_gene}', '|'.join(gene_groups)

                        elif len(genes_keys) == 3:
                            if genes_keys.issubset({'No prediction', 'Adnaviria', 'Varidnaviria'}):
                                return 'Adnaviria|Varidnaviria', sum([genes['Adnaviria'], genes['Varidnaviria']])

                            for realm_pairs in itertools.combinations(realms, 2):
                                if genes_keys.issubset({realm_pairs[0], realm_pairs[1], 'No prediction'}):
                                    return f'{maj_gene}', '|'.join(gene_groups)

                            # At this point, it's THREE realms, and that's not a "No prediction"
                            return 'No Realm', 'None'

                            # End it here
                    else:  # Greater than 1 and NOT 1-3... that's not enough confidence
                        return 'No Realm', 'None'

                else:
                    return 'No Realm', 'None'  # If no refs and then no genes

            return 'No Realm', 'None'  # Shouldn't hit here, should hit above

        # Realm references and predictions are done component-wise, but each assessment is done genome-wise
        # This is because each identity can have a different set of references and/or supporting unique genes
        logger.info('Aggregating realm predictions and support from each of the identities for each genome...')
        for genome, genome_s in tqdm(self.genomes_overview.iterrows(),
                                     unit=' genomes', disable=verbose, total=len(self.genomes_overview)):

            genome_prediction, genome_support = aggregate_predictions(genome_s[realm_ref_cols].values,
                                                                      genome_s[realm_pred_cols].values)
            self.genomes_overview.loc[genome, f'realm (prediction)'] = genome_prediction
            self.genomes_overview.loc[genome, f'realm (support)'] = genome_support

        # With reference realms defined for any/all genomes, can predict their rank...
        # Logically, getting each genome, finding its realm, then loading up the appropriate identity + threshold, but
        # it's encumbered by grabbing all members of that rank (its associated with), then RE-loading the same identity
        # every single time a realm that uses that identity needs to be used

        def assign_prediction(component_preds: list, default_name: str, poz, idd):
            # Validated to be 100.0% identical to previous method
            # Every single genome within a component should have a prediction - even if it's "No prediction"
            predictions = Counter(component_preds)
            predictions_set = set(predictions.keys())

            # TODO Handling 'Adnaviria|Varidnaviria' <--- results from aggregate_predictions()

            if len(predictions_set) == 1:  # If only one prediction

                prediction = predictions.most_common(1)[0][0]

                if prediction in not_implemented:
                    return 'Not implemented'

                elif prediction in {'No prediction', 'No Realm'}:
                    return default_name

                return prediction

            predicted_realms = predictions_set - {'No prediction', 'No Realm'}

            if len(predicted_realms) == 1:
                prediction = next(iter(predicted_realms))

                if prediction in not_implemented:
                    return 'Not implemented'

                return prediction

            if len(predicted_realms) > 1:
                logger.warning(f'Multiple conflicting realms for component {poz} at identity ({idd})')
                return default_name

            if not predicted_realms:
                return default_name

            logger.warning(f'Unhandled prediction case for component {poz} at identity ({idd})')
            return default_name

        # AT THIS POINT, there are two ways to do this. This begins the top-down method, where each component has
        # all ranks iterated and assigned a cluster, or
        logger.info('Assigning predictions under each of the identities (if applicable) for each genome...')
        # TODO Line profile
        if predict_type == 'naive':

            for identity in identities:  # BUILD COMPONENTS
                identity = str(identity)

                # Load for each identity, rather than each component
                shared_and_dist_matrix_fp = self.identities[identity]['shared_and_dist_fp']
                with h5py.File(shared_and_dist_matrix_fp, 'r') as f:
                    # Selectively load only distance, as other isn't required
                    distance_data = f['distance_data'][:]
                    distance_indices = f['distance_indices'][:]
                    distance_indptr = f['distance_indptr'][:]
                    distance_shape = tuple(f['distance_shape'][:])

                    distance_pos_labels = f['distance_index'][:].astype(str)

                    distance_csr = sparse.csr_matrix((distance_data, distance_indices, distance_indptr),
                                                     shape=distance_shape)
                    distance_matrix_df = pd.DataFrame.sparse.from_spmatrix(distance_csr, index=distance_pos_labels,
                                                                           columns=distance_pos_labels)

                for component, component_df in self.genomes_overview.groupby(by=f'component ({identity})'):
                    component = str(component)

                    # This is the "final" place where we can triple-check the realm prediction for a component
                    # TODO Is it even possible for this to occur?
                    #  Each component is assigned, and then each genome pulls from that component.
                    #  Every component already assigned realm. I suppose, that each genome would be under different
                    #  possible realms for each identity... but each identity in this stage only used for assigning rank

                    realm_prediction = assign_prediction(component_df['realm (prediction)'].tolist(),
                                                         default_name=default_realm, poz=component, idd=identity)

                    # TODO No prediction exists for tiny % user sequences, usually 3-4 No prediction + 1 Duplodnaviria

                    # Filter distance matrix to only include component members
                    component_members = component_df.index.tolist()  # Get members
                    component_dist_df = distance_matrix_df.loc[component_members, component_members].copy()

                    # By definition, everything should be "connected" within a component
                    # Convert to condensed, then linkage
                    # distArray = distance.squareform(component_dist_df.to_numpy())  # To condensed

                    # Still need to keep track of what happens at each identity, even if there's no "movement" on ranks
                    for rank in ranks:

                        if realm_prediction == 'Not implemented':
                            self.genomes_overview.loc[component_members, f'{rank} (component)'] = 'Not implemented'
                            continue

                        # TECHNICALLY, every single genome within the component should be the same
                        dist_val = distances[ref_domain][realm_prediction][rank]['PairwiseDistanceCutoff']
                        perc_id = distances[ref_domain][realm_prediction][rank]['cluster identity']

                        if pd.isnull(dist_val):  # No pairwise distance cutoff for this realm (Adna subfam)
                            continue

                        # Sadly, there's no way of knowing the percent identity of the cluster without 1st getting the
                        # realm prediction, AND having the taxonomic rank
                        if float(perc_id) != float(identity):
                            continue

                        # Assign genomes at rank to clusters
                        if len(component_dist_df) > 1:

                            # For 0.001, float64 sparse = 47.4 MB, float32 sparse = 31.6 MB ... CSR cannot do float16
                            # For 0.001, float64 dense = 806.889, float32 dense = 403 MB, float16 dense = 201 MB
                            # sp_matrix = sp_matrix.astype(np.float32)  # Verified identical as float64 (via ARI)
                            # sp_matrix = sp_matrix.astype(np.float16)  # 0.99985 ARI at order and family

                            if reduce_memory:
                                sp_matrix = distance_matrix_df.loc[component_members, component_members]
                                sp_matrix = sp_matrix.sparse.to_coo().tocsr()
                                sp_data = sp_matrix.data
                                sp_data = sp_data.astype(np.float16)
                                dense_matrix = np.zeros(sp_matrix.shape, dtype=np.float16)
                                dense_matrix[sp_matrix.nonzero()] = sp_data

                            else:
                                csr_matrix = sparse.csr_matrix(
                                    distance_matrix_df.loc[component_members, component_members],
                                    dtype=np.float32)

                                # TODO Evaluate the lower (0.96, 0.94) ARI at genus and subfamily...
                                # csr_matrix = csr_matrix <= dist_val
                                dense_matrix = csr_matrix.toarray()

                            dense_matrix[dense_matrix == 0] = 1
                            np.fill_diagonal(dense_matrix, 0)

                            linkage_matrix = linkage(squareform(dense_matrix), method='average')
                            clusters = fcluster(linkage_matrix, t=dist_val, criterion='distance')

                        else:
                            clusters = 0

                        self.genomes_overview.loc[component_members, f'{rank} (component)'] = clusters
                        # TODO - if the realm is not implemented, NONE of the genomes will have a "rank (component)"
                        # TODO ValueError: Must have equal len keys and value when setting with an iterable

        self.genomes_overview.to_csv(output_dir / 'genome_support.csv')

        # The "final" phase of GBGA - rectifying the taxonomy

        # Add in taxonomy for the references
        # TODO Fix user genomes having RefSeqID name in index, and then absent from GenomeName

        columns = ['RefSeqID', 'GenomeName'] + all_ranks
        self.final_overview = self.genomes_overview.merge(ref_db_df[columns], left_index=True, right_on='RefSeqID',
                                                          how='left')
        self.final_overview = self.final_overview.set_index('RefSeqID', drop=False)

        # Must. Sort. This. Dataframe.

        # 'RefSeqID' is no longer a column as it's now the index
        sorted_columns = ['GenomeName', 'Annotated', 'Proteins', 'Reference', 'Size (Kb)',
                          'realm', 'phylum', 'class',
                          'order', 'family', 'subfamily', 'genus',  # Pulled from ref_db_df
                          'order (component)', 'family (component)', 'subfamily (component)', 'genus (component)']
        component_columns = itertools.product(
            ['component', 'Realm Reference', 'Realm Predictions', 'network'],
            identities
        )

        network_columns = itertools.product(['network'], identities)
        network_columns = [f'{a} ({b})' for a, b in network_columns]

        # Add to identify non-edgeless singletons
        component_count_col = itertools.product(['component size'], identities)
        component_count_col = [f'{a} ({b})' for a, b in component_count_col]

        component_columns = [f'{a} ({b})' for a, b in component_columns]
        self.final_overview = self.final_overview[
            sorted_columns + component_columns + ['realm (prediction)', 'realm (support)'] + component_count_col]
        self.final_overview = self.final_overview.rename(
            columns={
                'realm': 'realm (Reference)',
                'phylum': 'phylum (Reference)',
                'class': 'class (Reference)',  # These 3 are never predicted
                'order': 'order (Reference)',
                'family': 'family (Reference)',
                'subfamily': 'subfamily (Reference)',
                'genus': 'genus (Reference)'})  # These all pulled from ref_db, and need to be annotated as such

        # Set up for when vConTACT3 makes predictions that other ranks can rely on
        self.final_overview[[f'{r} (prediction)' for r in all_ranks if r != 'realm']] = np.nan  # Handled earlier

        # Need to exclude pure singletons, as other nodes have a chance of matching at some level
        pre_final_overview = self.final_overview.loc[~(self.final_overview[network_columns] == 'edgeless').all(axis=1)]

        # Need to exclude those that may have a network edge at some level, but no component
        never_component = pre_final_overview.loc[(pre_final_overview[component_count_col] == 1.0).all(axis=1)]
        pre_final_overview = pre_final_overview.loc[~(pre_final_overview[component_count_col] == 1.0).all(axis=1)]

        # Instead of groupby component 0.3, assigning upper ranks, then through rank predictions, doing it in one block
        new_upper_ranks = {}
        # for r_predict, r_predict_df in self.final_overview.groupby(by='realm (prediction)'):
        for r_predict, r_predict_df in pre_final_overview.groupby(by='realm (prediction)'):

            # if r_predict == 'No Realm':  # TODO, check
            #     continue

            if r_predict == 'No prediction':
                continue

            # DON'T join Adnaviria & Varidnaviria. ALL references with separate realm predicts also have different 0.3
            # components, AND they track with their taxonomy. So joining groups that are in different 0.3 would
            # introduce errors. And because they're refs, they'll inherit the realm prediction for everything below

            # DON'T exclude "No realm". Many have the same 0.3 component, and all known examples should be grouped
            # together. This means that they don't have any taxonomy AND no unique genes (well, <3). Since they in same
            # component, that means they share sufficient identity to be grouped together. These are shockingly accurate

            # Don't skip not_implemented, they can still be predicted at the highest ranks

            # And can't just assume that the highest ranks are mono. Since v3 can't go above order, phylum and class are
            # indistinguishable

            for rank in all_ranks:

                # Don't keep track of everything - don't need to
                novel_ranks = {}

                rank_pos = all_ranks.index(rank)  # Should never need to go higher than -1

                rank_members = r_predict_df.index.tolist()
                r_predict_df = self.final_overview.loc[rank_members]  # Keep current, keep HERE

                if rank == 'realm':  # All are r_predict
                    continue

                # For these two, can't really predict
                if rank == 'phylum':
                    ref_assignments = Counter([r for r in r_predict_df[f'{rank} (Reference)'].values
                                               if pd.notnull(r)])

                    if len(ref_assignments) == 1:
                        ref_assignment = ref_assignments.most_common(1)[0][0]

                    elif len(ref_assignments) > 1:
                        ref_assignment = '|'.join(ref_assignments.keys())

                    elif not ref_assignments:

                        if r_predict == 'Ribozyviria':  # Apparently it's possible to have an entire realm as isolates
                            continue  # TODO Should there be another indicator here? There is, technically, no assignment

                        elif r_predict == 'Adnaviria|Varidnaviria':  # At huge scales, enough connections to surpass
                            # cross-realm?
                            # For prokaryotes, these are effectively identical
                            ref_assignment = 'Adnaviria|Varidnaviria'

                        elif r_predict == 'No Realm':
                            ref_assignment = default_realm

                        else:
                            logger.warning(f'There is no reference assignment for {r_predict}. This should not occur.')
                            exit(1)

                    else:
                        logger.warning(f'Unable to identify phylum for {r_predict}')
                        exit(1)

                    self.final_overview.loc[rank_members, f'{rank} (prediction)'] = ref_assignment

                elif rank == 'class':  # Unlike Phylum, which sits below realm, class CAN be multiple and request help

                    ref_assignments = Counter([r for r in r_predict_df[f'{rank} (Reference)'].values
                                               if pd.notnull(r)])

                    if len(ref_assignments) == 1:
                        ref_assignment = ref_assignments.most_common(1)[0][0]

                    elif len(ref_assignments) > 1:
                        ref_assignment = '|'.join(ref_assignments.keys())

                    elif not ref_assignments:
                        upper_rank = all_ranks[rank_pos - 1]
                        upper_assignments = Counter([r for r in r_predict_df[f'{upper_rank} (prediction)'].values
                                                     if pd.notnull(r)])  # Must exist

                        if len(upper_assignments) == 1:
                            ref_assignment = upper_assignments.most_common(1)[0][0]

                        elif len(upper_assignments) > 1:
                            ref_assignment = '|'.join(upper_assignments.keys())

                        elif r_predict == 'Ribozyviria':
                            continue
                            # TODO Check if Adna|Vari will hit this

                        else:
                            logger.warning(f'Unable to identify {rank} for {r_predict} with no assignment')
                            exit(1)

                    else:
                        logger.warning(f'Unable to identify {rank} for {r_predict}')
                        exit(1)

                    self.final_overview.loc[rank_members, f'{rank} (prediction)'] = ref_assignment

                else:  # Below are actually predictable

                    if r_predict in not_implemented:  # And not always implemented
                        continue

                    try:
                        rank_identity = distances[ref_domain][r_predict][rank]['cluster identity']
                    except KeyError:  # No Realm
                        rank_identity = distances[ref_domain][default_realm][rank]['cluster identity']

                    if np.isnan(rank_identity):  # Can't do anything for ranks that have NO reference to even predict
                        # Most notably, Varidnaviria and Adnaviria subfamilies
                        # If don't have something, (and continue), then everything below will be empty
                        # (i.e. THIS will be fine, but the next rank will also not find anything, and be skipped too)

                        # Want to "maintain" the above rank prediction, but can't aggregate, as all the (e.g. families)
                        # will be propagated to a lower rank (e.g. subfamily)

                        # Worst case scenario - missing out on inconsistencies in the taxonomy
                        # Best case - perfect
                        # Middling case - maintain the status quo

                        #
                        safe_rank = all_ranks[rank_pos - 1]
                        # Only need to go upwards once
                        safe_predict = Counter(
                            [r for r in r_predict_df[f'{safe_rank} (prediction)'].values if pd.notnull(r)])

                        if sum(safe_predict.values()) == len(rank_members):
                            adj_names = [f'noRef_of_{r}' for r in r_predict_df[f'{safe_rank} (prediction)'].values]
                            self.final_overview.loc[r_predict_df.index, f'{rank} (prediction)'] = adj_names
                        else:
                            logger.warning(f'Unable to safely handle rank {rank} of {r_predict} as no ref exists. '
                                           f'Run may fail.')

                        continue

                    for comp_id, comp_id_df in r_predict_df.groupby(by=f'component ({rank_identity})'):

                        for rank_group, rank_predict_df in comp_id_df.groupby(by=f'{rank} (component)'):

                            curr_rank_members = rank_predict_df.index.tolist()
                            rank_predict_df = self.final_overview.loc[curr_rank_members]

                            ref_predict = Counter([r for r in rank_predict_df[f'{rank} (Reference)'].unique() if pd.notnull(r)])

                            if len(ref_predict) == 1:  # We're good

                                self.final_overview.loc[rank_predict_df.index, f'{rank} (prediction)'] = ref_predict.most_common(1)[0][
                                    0]

                            elif len(ref_predict) > 1:  # Inconsistency between distance and reference taxonomy
                                self.final_overview.loc[rank_predict_df.index, f'{rank} (prediction)'] = '|'.join(ref_predict.keys())

                            else:  # References offer no guidance, so need to go up a rank and create a new group

                                # There should be no cases where upper rank needs to go above -1

                                # Go up a rank
                                upper_rank = all_ranks[rank_pos - 1]

                                # Only need to go upwards once
                                upper_predict = Counter(
                                    [r for r in rank_predict_df[f'{upper_rank} (prediction)'].values if pd.notnull(r)])

                                if len(upper_predict) > 1:
                                    upper_rank_predict = '||'.join(list(upper_predict.keys()))
                                    # TODO Keep track of this inconsistency
                                elif len(upper_predict) == 1:
                                    upper_rank_predict = upper_predict.most_common(1)[0][0]
                                else:  # TODO Should control for this, but it really shouldn't occur
                                    logger.warning(f'Unable to identify upper prediction for {r_predict} of {rank}')
                                    continue

                                if upper_rank_predict not in novel_ranks:
                                    novel_ranks[upper_rank_predict] = 0
                                else:
                                    novel_ranks[upper_rank_predict] += 1

                                new_c = novel_ranks[upper_rank_predict]

                                novel_prediction = f'novel_{rank}_{new_c}_of_{upper_rank_predict}'  # novel_subfamily_of_familyName

                                self.final_overview.loc[rank_predict_df.index, f'{rank} (prediction)'] = novel_prediction

        # Iterate through realms, decide the optimal
        # Doing it this way (vs everything before), means that decisions are made top-down. Previously, it wasn't quite
        # bottom-up, but there was filling in from "above", and when the above didn't have a prediction, it needed to go
        # to the next level, and then next. The problem is that, since this is done at different identities across
        # different ranks and realms, there were inconsistencies that AGGREGATED with each iteration


def identify_hallmarks(crosstab: pd.DataFrame):

    crosstab = crosstab.loc[:, (crosstab != 0).any(axis=0)]  # Select any columns that don't equal 0

    # Convert to numpy in order to do numpy magic
    # ctab = crosstab.to_numpy()
    # pure_shared = sum(np.all(ctab == ctab[0, :], axis=0))  # Sum all True across columns where value ==

    # https://stackoverflow.com/a/56198794 --> not the answer, but inspired by
    shared_hmms = crosstab.eq(crosstab.iloc[0, :], axis=1).all(0)  # Series of columns w/ Booleans for all ==
    hallmark_df = crosstab.loc[:, shared_hmms.values]

    return hallmark_df  # .columns.tolist()

