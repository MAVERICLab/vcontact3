"""
Handles all database-related functions
"""

import json
import logging
import os
import shutil
from io import StringIO
from pathlib import Path
from sys import exit

import pandas as pd
import requests

from . import utilities as utils

logging.basicConfig(
    level=logging.INFO,
    format="[%(levelname)s] %(asctime)s %(message)s",
    datefmt="%Y-%m-%d %H:%M:%S",
)

# Create logger
logger = logging.getLogger(__name__)

pd.set_option("display.max_rows", 100)
pd.set_option("display.max_columns", 10)
pd.set_option("display.width", 1000)


def get_available_dbs():

    git_url = "https://bitbucket.org/MAVERICLab/vcontact3/raw/master/db_versions.tsv"

    # r = requests.get(git_url).content
    # version_info = StringIO(r.decode("utf-8"))

    version_df = pd.read_csv(git_url, sep="\t")

    return version_df


def download_version(version, dest: Path, version_infos: pd.DataFrame):

    # Already verified that DB folder exists and that version is available
    src_path = version_infos.loc[version_infos['Version'] == int(version), 'Location'].iloc[0]

    src_name = Path(src_path).name

    if dest == Path("."):
        dest_path = dest.resolve()
        dest_path = dest_path / src_name
    else:  # Wasn't
        dest_path = dest / src_name

    if dest_path.exists():
        logger.error(f"Database {version} already exists at {dest_path}. If you wish to re-download the database, "
                     f"please remove the existing database file and try again.")
        exit(1)

    logger.info(f"Downloading database {version} from {src_path}. This may take some time depending on your "
                f"internet connection.")
    version_dl_res = utils.execute_stdout(['curl', '-s', '-o', str(dest_path), src_path])

    if version_dl_res.returncode != 0:
        logger.error(
            f"Unable to retrieve database {version} from {src_path}. This can happen due to connectivity issues or if "
            f"a permissions issue exists at the specified database download location (currently set to: {dest})."
        )
        exit(1)

    # Now, decompress
    logger.info(f"Decompressing {dest_path.name} to {dest_path.parent}. This may take a moment...")
    shutil.unpack_archive(dest_path, dest_path.parent)

    logger.info(f"Successfully decompressed database. When running vConTACT3, please use the following path: "
                f"'--db-path {dest_path.parent} --db-version {version}'.\n\n Alternatively, you can specify the full "
                f"path to the database version itself: '--db-path {dest_path.parent}/{version}.json")


def prepare_databases(list_versions: bool, get_version: str, download_location: Path):

    """

    :param list_versions:
    :param get_version:
    :param download_location:

    """

    available_dbs = get_available_dbs()

    if list_versions:
        available_versions = available_dbs["Version"].tolist()

        logger.info(f"The available versions are: {', '.join(map(str, available_versions))}")

    if get_version:

        if get_version == "latest":

            latest_version = sorted([int(v) for v in available_dbs["Version"].tolist()])[-1]
            get_version = latest_version

        else:  # If not specified, need to check to see if whatever's selected is available

            if int(get_version) in available_dbs["Version"].tolist():
                pass
            else:
                logger.error(f"Unable to find version {get_version} in available versions: {available_dbs['Version']}")
                exit(1)

        if download_location:

            if not download_location.exists():
                logger.info(f"Database directory does not exist: {download_location}. Will create it and place "
                            f"databases in it.")
                os.makedirs(download_location)

                download_path = download_location

            else:  # Already exists
                download_path = download_location

        else:
            download_path = Path(".")

        download_version(get_version, download_path, available_dbs)


def open_database(db_path: Path, db_version: int, db_domain: str):

    latest = None

    if db_path.is_dir():

        if db_version:

            db_info_fp = db_path / f"{db_version}.json"

            if not db_info_fp.exists():
                logger.error(f"Database not found at {db_path}. Please specify either the database's folder (which "
                             f"should be named the database's version number) or the json file itself.")
                exit(1)

            else:
                logger.info(f"Found database at {db_path}. Checking for consistency...")

        else:  # Haven't specified a version, so select latest version
            jsons = db_path.glob('**/???.json')
            sorted_jsons = sorted(jsons, key=lambda x: int(x.stem), reverse=True)

            logger.info(f"Found {len(sorted_jsons)} databases in {db_path}. Since no version was specified, will use "
                        f"latest version.")

            db_info_fp = sorted_jsons[0]
            latest = db_info_fp.stem

            logger.info(f"Using latest version: {latest}")

    elif db_path.is_file():
        # Will is_file return error if doesn't exist?

        db_info_fp = db_path

    else:
        logger.error(f"Unable to determine database from the path {db_path} indicated. Please specify either the folder"
                     f" the database file is located (i.e. /path/to/database_folder) or the json file itself "
                     f"(/path/to/database_folder/version.json).")
        exit(1)

    # Check to ensure database is valid
    with open(db_info_fp, "r") as f:
        db_info = json.load(f)

    if db_version:  # If user specifies

        db_version = str(db_version)

        if db_version not in db_info.keys():
            logger.error(f"Database version {db_version} not found in database at {db_path}.")
            exit(1)

    if latest:  # Else
        db_version = str(latest)

        if db_version not in db_info.keys():
            logger.error(f"Database version 'latest' ({db_version}) not found in database at {db_path}.")
            exit(1)

    if db_domain not in db_info[db_version]['Domains']:
        logger.error(f"Could not find {db_domain} in database version {db_version} located at {db_path}.")
        exit(1)

    return db_info, db_info_fp
