"""
Handles "raw" data and processes it until suitable for gene_clusters, this includes converting and parsing input
formats. Builds, collects and organizes all sources of "clusters" before being fed into "profiling." Renamed from
protein_clusters to better represent the gene-centric approach, verses protein, which fails to fully "work" with HMMs.

This essentially serves as a catch-all for pre-processing
"""

import io
import logging
import multiprocessing.pool
import shutil
from pathlib import Path

import pandas as pd
import pyrodigal
import pyrodigal_gv
from Bio import SeqIO
from tqdm import tqdm

logger = logging.getLogger(__name__)


def process_sequence(sequence: SeqIO.SeqRecord, use_gv=False, use_viral=False):

    sequence = sequence[0]

    if use_gv:
        orf_finder = pyrodigal_gv.ViralGeneFinder(meta=True, viral_only=use_viral)
    else:
        orf_finder = pyrodigal.GeneFinder(meta=True)

    seq_len = len(sequence.seq)
    seq_preds = orf_finder.find_genes(bytes(sequence.seq))

    return sequence.id, seq_len, seq_preds


def extend_sequence(process_seq: iter, gv_flag, viral_flag):
    """Extend the iterator to include a flag indicating whether or not the sequence is a viral sequence.

    :param process_seq:
    :param gv_flag:
    :param viral_flag:
    """

    for item in process_seq:
        yield item, gv_flag, viral_flag


def call_genes(nucl: Path, outdir: Path, force=False, threads=None,
               run_pyrodigal_gv: bool = False, run_viral: bool = False,
               ref_db: dict = {}, ref_fp: Path = None, ref_domain: str = None,
               verbose: bool = False):
    """Generate gene calls and create gene-to-genome file. Could open as dataframe, but keeping handles separate to be
    loaded elsewhere.

    :param nucl: Path to nucleotide sequence file
    :param outdir: Path to output directory
    :param force: Overwrite or not
    :param threads: Number of threads to use
    :param run_pyrodigal_gv: Whether or not to run pyrodigal_gv
    :param run_viral: Whether or not to use virus only models
    :param ref_db: Reference database
    :param ref_fp: Path to reference file
    :param ref_domain: Genome domain to use
    :return:
    """

    # Database stuff
    ref_version = ref_fp.stem
    ref_dir = ref_fp.parent
    ref_db = ref_db[ref_version]["Domains"][ref_domain]  # Just a ref to the DB before identities
    ref_g2g_fp = ref_dir / ref_db["gene2genome"]  # Path
    ref_g2g_df = pd.read_parquet(ref_g2g_fp)
    ref_genomes = ref_g2g_df.genome_id.unique().tolist()

    verbose = not verbose

    # Genome stuff
    proteins_fp = outdir / f'{nucl.stem}.faa'
    g2g_fp = outdir / f'{nucl.stem}.g2g.parquet'
    nucl_fp = outdir / f'{nucl.stem}.nucl.parquet'

    if (proteins_fp.exists() and g2g_fp.exists() and nucl_fp.exists()) and not force:
        logger.info('Identified existing proteins and gene-to-genome files in output directory. These outputs will not'
                    'be overwritten without --force enabled')

        return proteins_fp, g2g_fp, nucl_fp

    elif (not proteins_fp.exists() or not g2g_fp.exists()) or force:

        logger.info(f'Identifying ORFs for {nucl.stem}')

        records = SeqIO.parse(nucl, "fasta")
        fio = io.StringIO()

        # logger.info(f'Identifying ORFs for {len(records)} genome sequences with {threads} threads')
        # Wahoo, with everything going through pyrodigal, both translations and g2g can be handled simultaneously
        nucl_records = {}
        nucl_headers = set(ref_genomes)
        with multiprocessing.pool.ThreadPool(threads) as pool:

            records = extend_sequence(records, run_pyrodigal_gv, run_viral)

            for result in tqdm(pool.imap_unordered(process_sequence, records), unit=' records', disable=verbose):
                id_, len_, predict = result

                if id_ in ref_genomes:
                    logger.warning(f'Record with duplicate genome ID found ({id_}). This record will be IGNORED. To '
                                   f'include this record, please rename the sequence header.')
                else:
                    nucl_headers.update([id_])

                    predict.write_translations(fio, f'{id_}')  # Write them to this file object, one result at a time
                    nucl_records[len(nucl_records)] = {
                        'genome_id': id_,
                        'Size (Kb)': len_ / 1000
                    }

        # https://stackoverflow.com/a/3253819
        with open(proteins_fp, 'w') as proteins_fh:  # Copy file object to file handle
            fio.seek(0)
            shutil.copyfileobj(fio, proteins_fh, -1)

        fio.close()

        g2g_records = {}
        with open(proteins_fp, 'r') as proteins_fh:
            for record in SeqIO.parse(proteins_fh, 'fasta'):

                protein_id = record.id  #seq1||idx0_1
                contig_id = protein_id.rsplit('_', 1)[0]  #seq1||idx0
                genome_id = contig_id.rsplit('||', 1)[0]  #seq1
                g2g_records[len(g2g_records)] = {
                    'protein_id': protein_id,
                    'genome_id': genome_id,
                    'keywords': 'None'  # TODO Better ways to leverage this?
                }

        g2g_df = pd.DataFrame.from_dict(g2g_records, orient='index')
        g2g_df.to_parquet(g2g_fp)

        nucl_records_df = pd.DataFrame.from_dict(nucl_records, orient='index')
        nucl_records_df.to_parquet(nucl_fp)

        return proteins_fp, g2g_fp, nucl_fp
