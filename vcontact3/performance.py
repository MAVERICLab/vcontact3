"""
Handles calculating performance metrics of viral clusters
"""

import logging
import sys
from pathlib import Path
from pprint import pprint

import numpy as np
import pandas as pd
from jenkspy import jenks_breaks
from scipy.spatial import distance
from sklearn.cluster import KMeans
from sklearn.metrics import rand_score
from sklearn.metrics.cluster import normalized_mutual_info_score
from sklearn.model_selection import train_test_split

from vcontact3.config import all_ranks
from vcontact3.gbga import GBGA

logger = logging.getLogger(__name__)


class Performance(object):
    """
    Consumes ViralCluster to generating performance metrics, i.e. accuracy,
    sensitivity, separation, etc...
    """

    def __init__(self, gbga,
                 output_dir: Path
                 ):

        # Set up inputs

        if isinstance(gbga, GBGA):
            self.final_overview = gbga.final_overview

        else:
            logger.error('This isn\'t a GBGA object. Please '
                         'send this error message to the author.')
            logger.error(type(gbga))
            sys.exit(1)

        self.overview_performances = {}
        for realm, realm_df in self.final_overview.groupby(by='realm (Reference)'):
            # TODO The reason performance drops ~2-4% is because unclassified sequences are pushed into default category

            if realm not in self.overview_performances:
                self.overview_performances[realm] = {}

            for rank in all_ranks:

                cleaned_df = realm_df[[f'{rank} (Reference)', f'{rank} (prediction)']]  # Only select ref + prediction
                cleaned_df = cleaned_df[pd.notnull(cleaned_df[f'{rank} (Reference)'])]  # Remove NaN
                cleaned_df = cleaned_df[pd.notnull(cleaned_df[f'{rank} (prediction)'])]  # Remove NaN, yes, being lazy

                contig_truth, contig_preds = cleaned_df[f'{rank} (Reference)'], cleaned_df[f'{rank} (prediction)']

                if contig_truth.empty or contig_preds.empty:
                    continue

                # Calculate performance
                contingency_tbl = pd.crosstab(contig_truth, contig_preds)

                (clstr_wise_ppv, clstr_wise_sens, acc, clstr_sep, complx_sep,
                 sep) = calculate_performance(
                    contingency_tbl)
                compo_score = sum([acc, clstr_wise_ppv, clstr_wise_sens])

                ari = rand_score(contig_truth, contig_preds)
                nmi = normalized_mutual_info_score(contig_truth, contig_preds)

                if rank not in self.overview_performances[realm]:
                    self.overview_performances[realm][rank] = {}

                self.overview_performances[realm][rank] = {
                    'Clustering-wise PPV': clstr_wise_ppv,
                    'Clustering-wise Sensitivity': clstr_wise_sens,
                    'Clustering-wise Accuracy': acc,
                    'Clustering-wise Separation': clstr_sep,
                    'Complex-wise Separation': complx_sep,
                    'Separation': sep,
                    'Adjusted Rand Index (ARI)': ari,
                    'Normalized Mutual Info Score (NMI)': nmi,
                    'Compo Score': compo_score
                }

    def __repr__(self):
        return ('Performances object, consisting of the following:\n{num_contigs} Contigs\n{kinds} {kind}')


def calculate_performance(contingency):
    # Calculations will assume 0's need to be counted if they exist. They technically do, but shouldn't be included
    # in the calculation
    contingency_table = contingency.replace({0: np.nan})
    # contingency_complex_weights = contingency_table.sum(axis=1)
    # contingency_cluster_weights = contingency_table.sum(axis=0)

    sensitivity_tbl = calc_sensitivity(contingency_table)

    ppv_tbl = calc_ppv(contingency_table)
    ppv_tbl = ppv_tbl.loc[ppv_tbl.index != 'unassigned']

    # Maximum "complex-wise" PPV for each cluster
    ppv_tbl.loc['cluster-wise PPV'] = ppv_tbl.max(axis=0)

    # Weighted average of cluster-wise PPV
    # https://stackoverflow.com/questions/21113384/python-numpy-average-with-nans
    ppv_indices = ~np.isnan(ppv_tbl.loc['cluster-wise PPV'].values)
    clustering_wise_ppv = np.average(
        ppv_tbl.loc['cluster-wise PPV'].values[ppv_indices])  # , weights=contingency_cluster_weights)

    # Maximum "cluster-wise" sensitivity for each complex (new column)
    sensitivity_tbl['complex-wise sensitivity'] = sensitivity_tbl.max(axis=1)

    sensitivity_tbl = sensitivity_tbl[sensitivity_tbl.index != 'unassigned']

    # Weighted average of complex-wise sensitivity
    sens_indices = ~np.isnan(sensitivity_tbl['complex-wise sensitivity'].values)
    clustering_wise_sensitivity = np.average(sensitivity_tbl['complex-wise sensitivity'].values[sens_indices])

    sep_tbl = calc_separation(contingency_table)
    sep_tbl = sep_tbl[sep_tbl.index != 'unassigned']
    sep_tbl.loc['cluster-wise separation'] = sep_tbl.sum(axis=0)
    sep_tbl.loc['cluster-wise separation'].replace({0: np.nan}, inplace=True)
    sep_tbl.loc[:, 'complex-wise separation'] = sep_tbl.sum(axis=1)

    cluster_wise_separation = sep_tbl.loc['cluster-wise separation', [col for col in sep_tbl.columns.tolist() if
                                                                      col != 'complex-wise separation']].mean()
    complex_wise_separation = sep_tbl.loc[
        sep_tbl.index != 'cluster-wise separation', 'complex-wise separation'].mean()

    clustering_wise_separation = geo_mean([cluster_wise_separation, complex_wise_separation])

    accuracy = geo_mean([clustering_wise_ppv, clustering_wise_sensitivity])

    return clustering_wise_ppv, clustering_wise_sensitivity, accuracy, cluster_wise_separation, \
           complex_wise_separation, clustering_wise_separation


def calc_sensitivity(contingency_table):

    """
    Fraction of proteins in complex i found in cluster j
    Complex-wise sensitivity isn't calculated here, but is simply max value for each complex
    :param contingency_table: taxon level X VC membership matrix
    :return: sensitivity_tbl
    """

    # Get sum of rows (adds new column)
    contingency_counts = contingency_table.copy()
    contingency_counts['sum'] = contingency_counts.sum(axis=1)

    # Each cluster/complex divided by the total sum of each complex
    sensitivity = contingency_table.div(contingency_counts['sum'], axis=0)

    return sensitivity


def calc_ppv(contingency_table):
    """
    Proportion of members of cluster j which belong to complex i, relative to total number of members
    :param contingency_table: taxon level X VC membership matrix
    :return:
    """

    # Get sum of columns (new row)
    counts = contingency_table.copy()
    counts.loc['sum'] = counts.sum(axis=0)  # adds a new ROW with sum of column

    ppv = contingency_table.div(counts.loc['sum'], axis=1)

    return ppv


def calc_accuracy(sensitivity_table, ppv_tbl):
    """

    :param sensitivity_table:
    :param ppv_tbl:
    :return:
    """
    def geo_mean_overflow(a, b):
        res = np.log([a, b])
        return np.exp(res.sum() / len(res))

    vec_geo_mean = np.vectorize(geo_mean_overflow)

    accuracy = pd.DataFrame(vec_geo_mean(sensitivity_table, ppv_tbl))
    accuracy.columns = sensitivity_table.columns
    accuracy.index = sensitivity_table.index

    return accuracy


def calc_separation(contingency_table):
    contingency_counts = contingency_table.copy()

    row_sum = contingency_counts.sum(axis=1)
    row_freq = contingency_counts.div(row_sum, axis=0)

    col_sum = contingency_counts.sum(axis=0)  # adds a new ROW with sum of column
    col_freq = contingency_counts.div(col_sum, axis=1)

    separation_tbl = row_freq.multiply(col_freq)

    return separation_tbl


def geo_mean(iterable):
    a = np.array(iterable)
    return a.prod() ** (1.0 / len(a))


def build_elbow(eval_range, array: np.array):

    wss_list = []
    for k in eval_range:
        model = KMeans(n_clusters=k, n_init=10)
        model.fit(array.reshape(-1, 1))
        wss_list.append(model.inertia_)

    return wss_list


def get_closest_centroid(obs, centroids):
    '''
    Function for retrieving the closest centroid to the given observation
    in terms of the Euclidean distance.

    Parameters
    ----------
    obs : array
        An array containing the observation to be matched to the nearest centroid
    centroids : array
        An array containing the centroids

    Returns
    -------
    min_centroid : array
        The centroid closes to the obs
    '''
    min_distance = sys.float_info.max
    min_centroid = 0

    for c in centroids:
        dist = distance.euclidean(obs, c)
        if dist < min_distance:
            min_distance = dist
            min_centroid = c

    return min_centroid


def get_prediction_strength(k, train_centroids, x_test, test_labels):
    '''
    Function for calculating the prediction strength of clustering

    Parameters
    ----------
    k : int
        The number of clusters
    train_centroids : array
        Centroids from the clustering on the training set
    x_test : array
        Test set observations
    test_labels : array
        Labels predicted for the test set

    Returns
    -------
    prediction_strength : float
        Calculated prediction strength
    '''
    n_test = len(x_test)

    # populate the co-membership matrix
    D = np.zeros(shape=(n_test, n_test))
    for x1, l1, c1 in zip(x_test, test_labels, list(range(n_test))):
        for x2, l2, c2 in zip(x_test, test_labels, list(range(n_test))):
            if tuple(x1) != tuple(x2):
                if tuple(get_closest_centroid(x1, train_centroids)) == tuple(get_closest_centroid(x2, train_centroids)):
                    D[c1, c2] = 1.0

    # calculate the prediction strengths for each cluster
    ss = []
    for j in range(k):
        s = 0
        examples_j = x_test[test_labels == j, :].tolist()
        n_examples_j = len(examples_j)
        for x1, l1, c1 in zip(x_test, test_labels, list(range(n_test))):
            for x2, l2, c2 in zip(x_test, test_labels, list(range(n_test))):
                if tuple(x1) != tuple(x2) and l1 == l2 and l1 == j:
                    s += D[c1, c2]
        try:
            ss.append(s / (n_examples_j * (n_examples_j - 1)))
        except ZeroDivisionError:
            ss.append(0)

    prediction_strength = min(ss)

    return prediction_strength


def determine_prediction_strength(eval_range, array: np.array):

    X_train, X_test = train_test_split(array.reshape(-1, 1),
                                       test_size=0.2, shuffle=True, random_state=42)

    # Run clustering
    strengths = []
    for k in eval_range:
        if k <= X_test.shape[0]:  # clusters=4 >= n_samples=3
            model_train = KMeans(n_clusters=k, random_state=42, n_init=10).fit(X_train)
            model_test = KMeans(n_clusters=k, random_state=42, n_init=10).fit(X_test)

            pred_str = get_prediction_strength(k, model_train.cluster_centers_, X_test, model_test.labels_)
            strengths.append(pred_str)
        else:
            # print(f'WARNING: Fewer samples ({X_test.shape[0]}) than clusters ({k}). Cannot ensure...')
            continue

    prediction_str = np.array(strengths)

    indices = np.where(prediction_str == np.nanmax(prediction_str))
    str_pos = indices[0][0]  # [-1]
    str_val = eval_range[str_pos]  # Get highest index value

    return strengths, str_val


def classify(value, breaks):
    for i in range(1, len(breaks)):
        if value < breaks[i]:
            return i
    return len(breaks) - 1


def goodness_of_variance_fit(array, classer):
    # get the break points
    print(array.shape)
    classer = jenks_breaks(array, classer)

    # do the actual classification
    classified = np.array([classify(i, classer) for i in array])

    # max value of zones
    maxz = max(classified)

    # nested list of zone indices
    zone_indices = [[idx for idx, val in enumerate(classified) if zone + 1 == val] for zone in range(maxz)]

    # sum of squared deviations from array mean
    sdam = np.sum((array - array.mean()) ** 2)

    # sorted polygon stats
    array_sort = [np.array([array[index] for index in zone]) for zone in zone_indices]

    # sum of squared deviations of class means
    sdcm = sum([np.sum((classified - classified.mean()) ** 2) for classified in array_sort])

    # goodness of variance fit
    gvf = (sdam - sdcm) / sdam

    return gvf


