#!/usr/bin/env python
import sys
import os
import logging
import re
import subprocess
import tempfile
import multiprocessing as mp
import shutil
from collections import defaultdict
from typing import Tuple, Dict

import screed
import pandas as pd

from vcontact3.utilities import (
        tqdm_log, check_dependencies, canonical_gid, Taxonomy,
)
from vcontact3.config import (
        FASTANI_GENOMES, FASTANI_GENOMES_EXT, TAXONOMY_FILE, 
        AF_THRESHOLD, SPECIES_MSH_DIRNAME, ANI_SPECIES_THRESHOLD, 
        QUERY_EACH_GENOME_AS_FILE, ANI_SUMMARY_COLS, 
        ANI_CLOSEST_COLS, REF_GENOME_MSH,
)

MAX_FILES_PER_DIR = 1000


# make sure out_dir is realpath and expanduser
def species(seqfiles, no_mash, max_d, out_dir, mash_k, mash_v, mash_s, 
            min_af, mash_db=None, extension=None, cpus=1): 
    '''Runs species pipeline: vOTU clustering and closest ref matching
    Parameters
    ----------
    seqfiles : list of sequence file paths
    genomes : dict[str, str]
        Dict[genome_id] = fasta_path
    no_mash : bool
        True if Mash should be used for pre-filtering, False otherwise.
    max_d : float
        Maximum distance to keep [0-1]
    out_dir : str
        The directory to write the output files to.
    prefix : str
        The prefix to use when writing output files.
    mash_k : int
        k-mer size [1-32]
    mash_v : float
        maximum p-value to keep [0-1]
    mash_s : int
        maximum number of non-redundant hashes
    min_af : float
        alignment fraction to consider closest genome
    mash_db : Optional[str]
        The path to read/write the pre-computed Mash reference sketch 
        database.
    extension:
        file extension for query genomes; this is removed from file
        basename to get query genome ID; only used when each genome
        is passed in as file (QUEYR_EACH_GENOME_AS_FILE=True)
    cpus: int
        number of threads to use
    '''

    os.makedirs(out_dir, exist_ok=True)
    genomes = {}
    query_seqfile = None

    if QUERY_EACH_GENOME_AS_FILE:
        # prep genomes dict(id, path)
        for seqfile in seqfiles:
            gid = remove_extension(seqfile, extension=extension)
            genomes[gid] = seqfile
    else:
        ## prep genomes as dict(id, id)
        #query_seqfile = os.path.join(out_dir, 'query_seqfile.fa')
        #with open(query_seqfile, 'w') as fw:
        #    for fpath in seqfiles:
        #        with screed.open(fpath) as sp:
        #            for rec in sp:
        #                header = rec.name
        #                gid = header.split(None, 1)[0]
        #                genomes[gid] = gid
        #                fw.write(f'>{header}\n{rec.sequence}\n')

        # fastANI does not support multiple genomes in the same file like
        #   mash -i; so have to write each contig as a file

        splitdir = os.path.join(
                os.path.realpath(os.path.expanduser(out_dir)), 
                'query_splitdir'
        )
        os.makedirs(splitdir, exist_ok=True)
        cnt = 0
        for seqfile in seqfiles:
            with screed.open(seqfile) as sh:
                for rec in sh:
                    header = rec.name
                    name = header.split(None, 1)[0]
                    dir_idx = int(cnt/MAX_FILES_PER_DIR)
                    tmpdir = f'{splitdir}/tmp{dir_idx}'
                    os.makedirs(tmpdir, exist_ok=True)
                    _f = f'{tmpdir}/{name}.fa'
                    with open(_f, 'w') as fw:
                        seq = rec.sequence
                        fw.write(f'>{header}\n{seq}\n')
                        genomes[name] = _f
                    cnt += 1

    otu = OTURep(cpus=1)
    c_db = otu.run(
            genomes, no_mash, max_d, out_dir, 'vOTU', mash_k, 
            mash_v, mash_s, min_af, mash_db=None, 
    )
    reps = set(c_db['rep'])
    genomes_filt = dict((i, genomes[i]) for i in reps)

    refrep = RefRep(cpus=1)
    # output file: {out_dir}/refrep.ani_closest.tsv
    # mash_k set to 21 to match k in pre-computed ref genome sketches
    refrep.run(
            genomes_filt, no_mash, max_d, out_dir, 'refrep', 21, 
            mash_v, mash_s, min_af, mash_db=REF_GENOME_MSH, 
    )
    ani_closest_f = f'{out_dir}/refrep.ani_closest.tsv'
    refrep_df = pd.read_csv(ani_closest_f, sep='\t', header=0)
    sel = refrep_df.iloc[:,5] == True  #satisfies_circumscription_criteria
    refrep_df_filt = refrep_df.loc[sel, :]
    clf_gid_list = refrep_df_filt.iloc[:, 0]
    clf_tax_list = refrep_df_filt.iloc[:, 4]
    _d = dict(zip(clf_gid_list, clf_tax_list))
    tax_list = c_db['rep'].map(_d)
    c_db['lineage'] = tax_list

    # get unclassified vOTUs, and save their seqfile for network analysis
    unc_reps = refrep_df.loc[~sel,:].iloc[:, 0].tolist()
    outseqfile = os.path.join(out_dir, 'vOTU_uncl.fa')
    with open(outseqfile, 'w') as fw:
        for gid in unc_reps:
            f = genomes_filt[gid]
            with screed.open(f) as sh:
                for n, rec in enumerate(sh):
                    fw.write(f'>{gid}||idx{n}\n{rec.sequence}\n')

    shutil.rmtree(splitdir)
    return c_db

def remove_extension(f, extension=None):
    f = os.path.basename(filename)

    if extension and f.endswith(extension):
        f = f[0:f.rfind(extension)]
    else:
        f = os.path.splitext(f)[0]

    if f[-1] == '.':
        f = f[0:-1]

    return f

class OTURep:
    """Compute vOTU or dereplicate sequences"""
    cluster_prefix = 'vOTU'

    def __init__(self, cpus):
        """Instantiate the ANI rep class.
        Parameters
        ----------
        cpus : int
            The maximum number of CPUs available to this workflow.
        """
        self.logger = logging.getLogger('timestamp')
        self.cpus = cpus

    @staticmethod
    def check_dependencies(no_mash):
        """Exits the system if the required programs are not on the path.
        Parameters
        ----------
        no_mash : bool
            True if Mash will be used, False otherwise.
        """
        dependencies = ['fastANI']
        if not no_mash:
            dependencies.append('mash')
        check_dependencies(dependencies)

    def run(self, genomes, no_mash, max_d, out_dir, prefix, 
                mash_k, mash_v, mash_s, min_af, mash_db, query_seqfile=None):
        """Runs the pipeline.
        Parameters
        ----------
        genomes : dict[str, str]
            Dict[genome_id] = fasta_path
        no_mash : bool
            True if Mash should be used for pre-filtering, False otherwise.
        max_d : float
             maximum distance to keep [0-1]
        out_dir : str
            The directory to write the output files to.
        prefix : str
            The prefix to use when writing output files.
        mash_k : int
            k-mer size [1-32]
        mash_v : float
            maximum p-value to keep [0-1]
        mash_s : int
            maximum number of non-redundant hashes
        min_af : float
            alignment fraction to consider closest genome
        mash_db : Optional[str]
            The path to read/write the pre-computed Mash reference sketch 
            database.
        query_seqfile : path
            Sequence file with each sequence as genome (needed when
            QUERY_EACH_GENOME_AS_FILE is False)
        """
        self.check_dependencies(no_mash)

        self.logger.info('Loading reference genomes.')
        d_compare = defaultdict(set)
        d_paths = genomes

        # Pre-filter using Mash if specified.
        if not no_mash:
            dir_mash = os.path.join(out_dir, SPECIES_MSH_DIRNAME)

            mash = Mash(self.cpus, dir_mash, prefix)
            self.logger.info(f'Using Mash version {mash.version()}')
            mash_results = mash.run(
                    genomes, genomes, max_d, mash_k, 
                    mash_v, mash_s, mash_db,  
                    query_seqfile=query_seqfile, 
                    ref_seqfile=query_seqfile,
            )
            for qry_gid, ref_hits in mash_results.items():
                d_compare[qry_gid] = d_compare[qry_gid].union(
                        set(ref_hits.keys())
                )

        # Compare against all genomes except self.
        else:
            for qry_gid in genomes:
                st = set(genomes.keys())
                st.remove(qry_gid)
                d_compare[qry_gid] = st

        self.logger.info(
                f'Calculating ANI with FastANI v{FastANI._get_version()}.'
        )
        fastani = FastANI(self.cpus, force_single=True)
        fastani_results = fastani.run(d_compare, d_paths)

        # output: '{prefix}.ani_summary.tsv'
        ANISummaryFile(out_dir, prefix, fastani_results)

        # parse ani summary and cluster
        sum_f = os.path.join(out_dir, f'{prefix}.ani_summary.tsv')
        n_db = pd.read_csv(sum_f, sep='\t', header=0)

        # convert colnames for clustering functions from drep
        # ['user_genome', 'reference_genome', 'ani', 'af', 
        #  'reference_taxonomy']
        tax_col = ANI_SUMMARY_COLS[4]
        n_db = n_db.drop([tax_col], axis=1)
        colnames_new = ['query', 'reference', 'ani', 'alignment_coverage']
        n_db.columns = colnames_new

        c_db, _d = genome_hierarchical_clustering(
                n_db, S_ani=ANI_SPECIES_THRESHOLD, cov_thresh=AF_THRESHOLD, 
                cluster=self.cluster_prefix, clusterAlg='average', 
                comp_method='euclidean', 
        )
        # Cluster ANI database
        # Args:
        #     Ndb: result of secondary clustering
        #         (columns: ['query', 'reference', 'ani', 'alignment_coverage'])
        #         ('query' and 'reference' items are assumed to be paths)
        # Keyword arguments:
        #     clusterAlg: how to cluster the database (default = single)
        #         ('single', 'complete', 'average', 'weighted', 'centroid',
        #           'median', 'ward')
        #     S_ani: thershold to cluster at (default = .99)
        #     cov_thresh: minumum coverage to be included in clustering 
        #         (default = .5)
        #     cluster: name of the cluster
        #     comp_method: comparison algorithm used
        # Returns:
        #     list: [Cdb, {cluster:[linkage, linkage_db, arguments]}]
        #         (Cdb columns: ['cluster', 'genome']
        #             (Cdb columns: ['genome', 'secondary_cluster', 
        #           'threshold', 'cluster_method', 'comparison_algorithm'])

        if not no_mash:
            # add singletons fitered by mash clustering
            max_cluster_idx = max(
                    int(i.rsplit('_', 1)[1]) \
                            for i in c_db['secondary_cluster']
            )
            singleton_lst = list(set(genomes.keys()) - set(c_db['genome']))
            cluster_idx = max_cluster_idx + 1
            _lst = []
            for i in singleton_lst:
                _lst.append(f'{self.cluster_prefix}_{cluster_idx}')
                cluster_idx += 1

            _d = {'genome': singleton_lst, 'secondary_cluster': _lst}
            _df = pd.DataFrame.from_dict(_d, orient='columns')
            _df['threshold'] = c_db['threshold'].iloc[0]
            _df['cluster_method'] = c_db['cluster_method'].iloc[0]
            _df['comparison_algorithm'] = \
                    c_db['comparison_algorithm'].iloc[0]

        c_db = pd.concat([c_db, _df])

        #Pick representative for each vOTU
        '''
        parameters
        ----------
        outfile: str
            output file path for OTU representive sequences
        seqfile : path
            Sequence file with each sequence as genome (needed when
            QUERY_EACH_GENOME_AS_FILE is False)

        returns
        -------
        pd.DataFrame
            columns: ['genome', 'secondary_cluster', 'threshold', 
                'cluster_method', 'comparison_algorithm', 
                'genome_size', 'rep']

        '''
        sizes = [get_genome_size(genomes=genomes, name=i) 
                for i in c_db['genome']]
        c_db['genome_size'] = sizes
        # pick longest genome for each cluster
        _df = c_db.groupby('secondary_cluster').apply(
                lambda x: x.sort_values('genome_size', 
                    ascending=False).head(n=1)
        )
        _d = dict(zip(_df['secondary_cluster'],  _df['genome']))
        c_db['rep']  = c_db['secondary_cluster'].map(_d)

        return c_db


def get_genome_size(name, genomes):
    '''
    parameters
    ----------
    genomes: dict[str, str]
        dictionary of genome id to genome file path
    name: str
        genome id

    returns
    -------
    int
        length of genome
    '''
    f = genomes[name]
    size = 0
    with screed.open(f) as sh:
        for rec in sh:
            size += len(rec.sequence)

    return  size

class RefRep:
    """Computes a list of genomes to a list of representatives."""

    def __init__(self, cpus):
        """Instantiate the ANI rep class.
        Parameters
        ----------
        cpus : int
            The maximum number of CPUs available to this workflow.
        """
        self.logger = logging.getLogger('timestamp')
        self.cpus = cpus

    @staticmethod
    def check_dependencies(no_mash):
        """Exits the system if the required programs are not on the path.
        Parameters
        ----------
        no_mash : bool
            True if Mash will be used, False otherwise.
        """
        dependencies = ['fastANI']
        if not no_mash:
            dependencies.append('mash')
        check_dependencies(dependencies)

    @staticmethod
    def _get_ref_genomes():
        """Returns a dictionary of genome accession to genome path.
        Returns
        -------
        dict[str, str]
            Dict[genome_id] = fasta_path
        """
        ref_genomes = dict()
        for f_name in os.listdir(FASTANI_GENOMES):
            if f_name.endswith(FASTANI_GENOMES_EXT):
                accession = f_name.split(FASTANI_GENOMES_EXT)[0]
                ref_genomes[accession] = os.path.join(FASTANI_GENOMES, f_name)
        return ref_genomes

    def run(self, genomes, no_mash, max_d, out_dir, prefix, mash_k, 
            mash_v, mash_s, min_af, mash_db=None, query_seqfile=None):
        """Runs the pipeline.
        Parameters
        ----------
        genomes : dict[str, str]
            Dict[genome_id] = fasta_path
        no_mash : bool
            True if Mash should be used for pre-filtering, False otherwise.
        max_d : float
             maximum distance to keep [0-1]
        out_dir : str
            The directory to write the output files to.
        prefix : str
            The prefix to use when writing output files.
        mash_k : int
            k-mer size [1-32]
        mash_v : float
            maximum p-value to keep [0-1]
        mash_s : int
            maximum number of non-redundant hashes
        min_af : float
            alignment fraction to consider closest genome
        mash_db : Optional[str]
            The path to read/write the pre-computed Mash reference sketch database.
        query_seqfile : path
            Sequence file with each sequence as genome (needed when
            QUERY_EACH_GENOME_AS_FILE is False)
        """
        self.check_dependencies(no_mash)

        self.logger.info('Loading reference genomes.')
        ref_genomes = self._get_ref_genomes()
        d_compare = defaultdict(set)
        d_paths = {**genomes, **ref_genomes}

        # Pre-filter using Mash if specified.
        if not no_mash:
            dir_mash = os.path.join(out_dir, SPECIES_MSH_DIRNAME)

            mash = Mash(self.cpus, dir_mash, prefix)
            self.logger.info(f'Using Mash version {mash.version()}')
            mash_results = mash.run(
                    genomes, ref_genomes, max_d, mash_k, mash_v, 
                    mash_s, mash_db, query_seqfile
            )
            for qry_gid, ref_hits in mash_results.items():
                d_compare[qry_gid] = d_compare[qry_gid].union(
                        set(ref_hits.keys()))

        # Compare against all reference genomes.
        else:
            for qry_gid in genomes:
                d_compare[qry_gid] = set(ref_genomes.keys())

        mes = f'Calculating ANI with FastANI v{FastANI._get_version()}.'
        self.logger.info(mes)
        fastani = FastANI(self.cpus, force_single=True)
        fastani_results = fastani.run(d_compare, d_paths)

        taxonomy = Taxonomy().read(TAXONOMY_FILE, canonical_ids=True)
        ANISummaryFile(out_dir, prefix, fastani_results, taxonomy)
        ANIClosestFile(out_dir, prefix, fastani_results, genomes,
                min_af, taxonomy)


class ANISummaryFile(object):
    name = 'ani_summary.tsv'

    def __init__(self, root, prefix, results, taxonomy=None):
        """Writes the ANI summary file generated by this pipeline.
        Parameters
        ----------
        root : str
            The directory to write the summary file to.
        prefix : str
            The output file prefix.
        results: dict[str, dict[str, dict[str, float]]]
            FastANI results.
        taxonomy : dict[str, tuple[str, str, str, str, str, str, str]]
            d[unique_id] -> [d__<taxon>, ..., s__<taxon>]
        """
        self.path = os.path.join(root, f'{prefix}.{self.name}')
        self.results = results
        self.taxonomy = taxonomy
        self.logger = logging.getLogger('timestamp')
        self._write()

    def _write(self):
        with open(self.path, 'w') as fh:
            mes = '\t'.join(ANI_SUMMARY_COLS)
            fh.write(f'{mes}\n')
            for qry_gid, ref_hits in sorted(self.results.items()):
                for ref_gid, ref_hit in sorted(ref_hits.items(), 
                        key=lambda x: (-x[1]['af'], -x[1]['ani'], x[0])):
                    if self.taxonomy != None:
                        canonical_rid = canonical_gid(ref_gid)
                        taxonomy_str = ';'.join(self.taxonomy[canonical_rid])
                    else:
                        taxonomy_str = 'NA'
                    fh.write(f'{qry_gid}\t{ref_gid}')
                    fh.write(f'\t{ref_hit["ani"]}\t{ref_hit["af"]}')
                    fh.write(f'\t{taxonomy_str}\n')
        self.logger.info(f'Summary of results saved to: {self.path}')


class ANIClosestFile(object):
    name = 'ani_closest.tsv'

    def __init__(self, root, prefix, results, genomes, min_af, taxonomy):
        """Writes the ANI closest file generated by this pipeline.
        Parameters
        ----------
        root : str
            The directory to write the summary file to.
        prefix : str
            The output file prefix.
        results: dict[str, dict[str, dict[str, float]]]
            FastANI results.
        genomes : dict[str, str]
            Dict[genome_id] = fasta_path
        min_af : float
            alignment fraction to consider closest genome
        taxonomy: dict[str, tuple[str, str, str, str, str, str, str]]
            d[unique_id] -> [d__<taxon>, ..., s__<taxon>]
        """
        self.logger = logging.getLogger('timestamp')
        self.path = os.path.join(root, f'{prefix}.{self.name}')
        self.results = results
        self.genomes = genomes
        self.min_af = min_af
        self.taxonomy = taxonomy
        #self.gtdb_radii = GTDBRadiiFile()
        #gtdb_ani_radius = self.gtdb_radii.get_rep_ani(canonical_rid)
        self.ani_radius = ANI_SPECIES_THRESHOLD
        self._write()

    def _write(self):
        with open(self.path, 'w') as fh:
            mes = '\t'.join(ANI_CLOSEST_COLS)
            fh.write(f'{mes}\n')

            for gid in sorted(self.genomes):
                if gid in self.results:
                    thresh_results = [
                        (ref_gid, hit) for (ref_gid, hit) \
                                in self.results[gid].items() \
                                    if (hit['af'] >= self.min_af and \
                                    hit['ani'] >= self.ani_radius)
                    ]
                    closest = sorted(thresh_results, 
                            key=lambda x: (-x[1]['ani'], -x[1]['af']))
                    if len(closest) > 0:
                        ref_gid = closest[0][0]
                        canonical_rid = canonical_gid(ref_gid)
                        taxonomy_str = ';'.join(self.taxonomy[canonical_rid])

                        closest_ani = closest[0][1]['ani']
                        closest_af = closest[0][1]['af']

                        meet_criteria = (closest_ani >= self.ani_radius 
                                and closest_af >= self.min_af)
                        fh.write(f'{gid}\t{ref_gid}\t{closest_ani}\t'
                                    f'{closest_af}\t{taxonomy_str}\t'
                                    f'{meet_criteria}\n')
                    else:
                        _s = '\t'.join(['NA']*5)
                        fh.write(f'{gid}\t{_s}\n')
                else:
                    _s = '\t'.join(['NA']*5)
                    fh.write(f'{gid}\t{_s}\n')
        self.logger.info(f'Closest representative hits saved to: {self.path}')

class Mash:
    """Runs Mash against genomes."""

    def __init__(self, cpus, out_dir, prefix):
        """Instantiate the Mash class.

        Parameters
        ----------
        cpus : int
            The maximum number of CPUs available to Mash.
        out_dir : str
            The directory to write output files to.
        prefix : str
            The prefix for all output files.
        """
        self.logger = logging.getLogger('timestamp')
        self.cpus = max(cpus, 1)
        self.out_dir = out_dir
        self.prefix = prefix

    @staticmethod
    def version():
        """Returns the version of mash, or 'unknown' if not known."""
        try:
            proc = subprocess.Popen(['mash', '--version'], stdout=subprocess.PIPE,
                                    stderr=subprocess.PIPE, encoding='utf-8')
            stdout, stderr = proc.communicate()
            if len(stdout) > 0:
                return stdout.strip()
            else:
                return 'unknown'
        except Exception:
            return 'unknown'

    def run(self, qry, ref, mash_d, mash_k, mash_v, mash_s, 
            mash_db, query_seqfile=None, ref_seqfile=None) \
                    -> Dict[str, Dict[str, Tuple[float, float, int, int]]]:
        """Run Mash on a set of reference and query genomes.

        Parameters
        ----------
        qry : dict[str, str]
            The set of query genomes and their path.
        ref : dict[str, str]
            The set of reference genomes and their path.
        mash_d : float
            The maximum distance to report.
        mash_k : int
            The number of k-mers to store for each sequence.
        mash_v : float
            Maximum p-value to report.
        mash_s: int
            Maximum number of non-redundant hashes.
        mash_db : Optional[str]
            The path to read/write the pre-computed Mash reference 
            sketch database.
        query_seqfile : path
            Sequence file with each sequence as genome (needed when
            QUERY_EACH_GENOME_AS_FILE is FALSE)
        ref_seqfile : path
            Sequence file with each sequence as genome

        Returns
        -------
        dict[query_id][ref_id] = (dist, p_val, shared_numerator, 
            shared_denominator)
        """
        qry_sketch = QrySketchFile(
                qry, self.out_dir, self.prefix, self.cpus, mash_k, 
                mash_s,  seqfile=query_seqfile
        )
        ref_sketch = RefSketchFile(
                ref, self.out_dir, self.prefix, self.cpus, mash_k, 
                mash_s, mash_db, seqfile=ref_seqfile
        )

        # Generate an output file comparing the distances between these genomes.
        mash_dists = DistanceFile(qry_sketch, ref_sketch, self.out_dir,
                self.prefix, self.cpus, max_d=mash_d, mash_v=mash_v)
        results = mash_dists.read()

        # Convert the results back to the accession
        path_to_qry = {os.path.basename(v): k for (k, v) in qry.items()}
        path_to_ref = {os.path.basename(v): k for (k, v) in ref.items()}
        out = defaultdict(dict)
        for qry_path, ref_hits in results.items():
            qry_bname = os.path.basename(qry_path)
            for ref_path, hit in ref_hits.items():
                ref_bname = os.path.basename(ref_path)
                out[path_to_qry[qry_bname]][path_to_ref[ref_bname]] = hit

        return out


class DistanceFile(object):
    """The resulting distance file from the mash dist command."""
    name = 'msh_distances.tsv'

    def __init__(self, qry_sketch, ref_sketch, root, prefix, cpus, max_d, mash_v):
        """Create a new Mash distance file using these arguments.

        Parameters
        ----------
        qry_sketch : QrySketchFile
            The query sketch file generated by Mash.
        ref_sketch : RefSketchFile
            The reference sketch file generated by Mash.
        root : str
            The directory where the distance file will be contained.
        prefix : str
            The prefix to use in for distance file.
        cpus : int
            The maximum number of CPUs available to Mash.
        max_d : float
            The maximum distance to consider.
        mash_v : float
            The maximum value to consider.
        """
        self.logger = logging.getLogger('timestamp')
        self.qry_sketch = qry_sketch
        self.ref_sketch = ref_sketch
        self.path = os.path.join(root, f'{prefix}.{self.name}')
        self.cpus = cpus
        self.max_d = max_d
        self.mash_v = mash_v

        self._calculate()

    def _calculate(self):
        self.logger.info('Calculating Mash distances.')
        args = ['mash', 'dist', '-p', self.cpus, '-d', self.max_d, '-v',
                self.mash_v, self.ref_sketch.path, self.qry_sketch.path]
        args = list(map(str, args))
        with open(self.path, 'w') as f_out:
            proc = subprocess.Popen(args, stdout=f_out, encoding='utf-8')
            proc.communicate()
        if proc.returncode != 0:
            raise Exception(f'Error running Mash dist')

    def read(self) -> Dict[str, Dict[str, Tuple[float, float, int, int]]]:
        """Reads the results of the distance file.

        Returns
        -------
        dict[query_id][ref_id] = (dist, p_val, shared_numerator, shared_denominator)
        """
        out = defaultdict(dict)
        with open(self.path, 'r') as fh:
            hits = re.findall(r'(.+)\t(.+)\t(.+)\t(.+)\t(\d+)\/(\d+)\n', fh.read())
        for ref_id, qry_id, dist, p_val, shared_n, shared_d in hits:
            dist, p_val = float(dist), float(p_val)
            shared_num, shared_den = int(shared_n), int(shared_d)
            out[qry_id][ref_id] = (dist, p_val, shared_num, shared_den)

        return out


class SketchFile:
    """Output files which are generated by mash sketch."""

    def __init__(self, genomes, path, cpus, k, s, seqfile=None):
        """Create a sketch file for a given set of genomes.

        Parameters
        ----------
        genomes : dict[str, str]
            The genomes to create a sketch file from (genome_id, fasta_path).
        path : str
            The path to write the sketch file to.
        cpus : int
            The maximum number of CPUs available for Mash.
        k : int
            The k-mer size.
        s : int
            Maximum number of non-redundant hashes.
        seqfile : path
            Sequence file with each sequence as genome (needed when
            QUERY_EACH_GENOME_AS_FILE is False)
        """
        self.logger = logging.getLogger('timestamp')
        self.genomes = genomes
        self.path = path
        self.data = dict()
        self.args = dict()
        self.cpus = cpus
        self.k = k
        self.s = s
        self.seqfile = seqfile

        os.makedirs(os.path.dirname(self.path), exist_ok=True)

        # Use the pre-existing sketch file, otherwise generate it.
        if os.path.isfile(self.path):
            self.logger.info(
                f'Loading data from existing Mash sketch file: {self.path}'
            )
            self._load_metadata()
            if not self._is_consistent():
                raise Exception(
                    f'The sketch file is not consistent with the '
                    f'input genomes. Remove the existing sketch '
                    f'file or specify a new output directory.'
                )
        else:
            self.logger.info(f'Creating Mash sketch file: {self.path}')
            self._generate()

    def _load_metadata(self):
        """Loads the metadata from an existing Mash sketch file."""
        args = ['mash', 'info', '-t', self.path]
        proc = subprocess.Popen(args, stdout=subprocess.PIPE,
                                stderr=subprocess.PIPE, encoding='utf-8')
        stdout, stderr = proc.communicate()

        if proc.returncode != 0:
            raise Exception(
                f'Error reading Mash sketch file {self.path}:\n{stderr}'
            )

        for hashes, length, path in re.findall(
                r'(\d+)\t(\d+)\t(.+)\t.*\n', stdout):
            self.data[path] = (int(hashes), int(length))

    def _is_consistent(self):
        """Returns True if the sketch was generated from the genomes."""
        s1 = set(os.path.basename(f) for f in self.data.keys())
        s2 = set(os.path.basename(f) for f in self.genomes.values())
        return s1 == s2

    def _generate(self):
        """Generate a new sketch file."""
        with tempfile.TemporaryDirectory(prefix='mash_tmp_') as dir_tmp:
            # QUERY_EACH_GENOME_AS_FILE is True
            if self.seqfile == None:
                path_genomes = os.path.join(dir_tmp, 'genomes.txt')
                with open(path_genomes, 'w') as fh:
                    for path in self.genomes.values():
                        fh.write(f'{path}\n')

                args = ['mash', 'sketch', '-l', '-p', self.cpus, 
                        '-o', self.path, '-k', self.k, '-s', self.s, 
                        path_genomes]
                args = list(map(str, args))
                self.logger.debug(args)
                proc = subprocess.Popen(
                        args, stdout=subprocess.PIPE, 
                        stderr=subprocess.PIPE, encoding='utf-8',
                )
                with tqdm_log(total=len(self.genomes), unit='genome') as p_bar:
                    for line in iter(proc.stderr.readline, ''):
                        if line.startswith('Sketching'):
                            p_bar.update()

                proc.wait()

            else:
                # QUERY_EACH_GENOME_AS_FILE is False
                path_genomes = self.seqfile
                args = ['mash', 'sketch', '-i', '-p', self.cpus, 
                        '-o', self.path, '-k', self.k, '-s', self.s, 
                        path_genomes]
                args = list(map(str, args))
                self.logger.debug(args)
                proc = subprocess.Popen(
                        args, stdout=subprocess.PIPE, 
                        stderr=subprocess.PIPE, encoding='utf-8',
                )
                proc.wait()
                
            if proc.returncode != 0 or not os.path.isfile(self.path):
                raise Exception(
                        f'Error generating Mash sketch: {proc.stderr.read()}'
                )


class QrySketchFile(SketchFile):
    name = 'query_sketch.msh'

    def __init__(self, genomes, root, prefix, cpus, k, s, seqfile=None):
        """Create a query file for a given set of genomes.

        Parameters
        ----------
        genomes : dict[str, str]
            The genomes to create a sketch file from (genome_id, fasta_path).
        root : str
            The directory where the sketch file will be saved.
        prefix : str
            The prefix to use for this file.
        cpus : int
            The maximum number of CPUs available for Mash.
        k : int
            The k-mer size.
        s : int
            Maximum number of non-redundant hashes.
        seqfile : path
            Sequence file with each sequence as genome (needed when
            QUERY_EACH_GENOME_AS_FILE is False)
        """
        path = os.path.join(root, f'{prefix}.{self.name}')

        super().__init__(genomes, path, cpus, k, s, seqfile)


class RefSketchFile(SketchFile):
    name = 'ref_sketch.msh'

    def __init__(self, genomes, root, prefix, cpus, k, s, mash_db=None,
            seqfile=None):
        """Create a query file for a given set of genomes.

        Parameters
        ----------
        genomes : dict[str, str]
            The genomes to create a sketch file from (genome_id, fasta_path).
        root : str
            The directory where the sketch file will be saved.
        prefix : str
            The prefix to use for this file.
        cpus : int
            The maximum number of CPUs available for Mash.
        k : int
            The k-mer size.
        s : int
            Maximum number of non-redundant hashes.
        mash_db : Optional[str]
            The path to read/write the pre-computed Mash reference sketch database.
        seqfile : path
            Sequence file with each sequence as genome (needed when
            QUERY_EACH_GENOME_AS_FILE is False)
        """
        if mash_db != None:
            export_msh = mash_db.rstrip('\\')
            if not export_msh.endswith(".msh"):
                export_msh = export_msh + ".msh"
            if os.path.isdir(export_msh):
                raise Exception(f"{export_msh} is a directory")
            os.makedirs(os.path.dirname(export_msh), exist_ok=True)
            path = export_msh
        else:
            path = os.path.join(root, f'{prefix}.{self.name}')

        super().__init__(genomes, path, cpus, k, s, seqfile=seqfile)


class FastANI(object):
    """Python wrapper for FastANI (https://github.com/ParBLiSS/FastANI)"""

    def __init__(self, cpus, force_single):
        """Instantiate the class.

        Parameters
        ----------
        cpus : int
            The number of CPUs to use.
        force_single : bool
            True if the ql and rl calls should be done individually.
        """
        self.cpus = max(cpus, 1)
        self.force_single = force_single
        self.logger = logging.getLogger('timestamp')
        self.version = self._get_version()
        self.minFrac = self._isMinFrac_present()
        self._suppress_v1_warning = False

    @staticmethod
    def _get_version():
        """Returns the version of FastANI on the system path.

        Returns
        -------
        str
            The string containing the fastANI version.
        """
        try:
            proc = subprocess.Popen(['fastANI', '-v'], stdout=subprocess.PIPE,
                                    stderr=subprocess.PIPE, encoding='utf-8')
            stdout, stderr = proc.communicate()
            if stderr.startswith('Unknown option:'):
                return 'unknown (<1.3)'
            version = re.search(r'version (.+)', stderr)
            return version.group(1)
        except Exception:
            return 'unknown'
     
    @staticmethod   
    def _isMinFrac_present():
        """Returns true if --minFraction is an option of FastANI on the system path.

        Returns
        -------
        bool
            True/False.
        """
        try:
            proc = subprocess.Popen(['fastANI', '-h'], stdout=subprocess.PIPE,
                                    stderr=subprocess.PIPE, encoding='utf-8')
            stdout, stderr = proc.communicate()
            if '--minFraction' in stderr:
                return True
            return False
        except Exception:
            return False

    def run(self, dict_compare, dict_paths, query_seqfile=None):
        """Runs FastANI in batch mode.

        Parameters
        ----------
        dict_compare : dict[str, set[str]]
            All query to reference comparisons to be made.
        dict_paths : dict[str, str]
            The path for each genome id being compared.

        Returns
        -------
        dict[str, dict[str, dict[str, float]]]
            A dictionary containing the ANI and AF for each comparison."""

        # Create the multiprocessing items.
        manager = mp.Manager()
        q_worker = manager.Queue()
        q_writer = manager.Queue()
        q_results = manager.Queue()

        # Populate the queue of comparisons in forwards and reverse direction.
        n_total = 0
        for qry_gid, ref_set in dict_compare.items():
            if self.force_single:
                qry_path = dict_paths[qry_gid]

                for ref_gid in ref_set:
                    ref_path = dict_paths[ref_gid]

                    fwd_dict = {'q': dict(), 'r': dict(), 'qry': qry_gid}
                    rev_dict = {'q': dict(), 'r': dict(), 'qry': qry_gid}

                    fwd_dict['q'][qry_gid] = qry_path
                    fwd_dict['r'][ref_gid] = ref_path

                    rev_dict['q'][ref_gid] = ref_path
                    rev_dict['r'][qry_gid] = qry_path

                    q_worker.put(fwd_dict)
                    q_worker.put(rev_dict)
                    n_total += 2

            else:
                fwd_dict = {'ql': dict(), 'rl': dict(), 'qry': qry_gid}
                rev_dict = {'ql': dict(), 'rl': dict(), 'qry': qry_gid}

                qry_path = dict_paths[qry_gid]
                fwd_dict['ql'][qry_gid] = qry_path
                rev_dict['rl'][qry_gid] = qry_path

                for ref_gid in ref_set:
                    ref_path = dict_paths[ref_gid]
                    fwd_dict['rl'][ref_gid] = ref_path
                    rev_dict['ql'][ref_gid] = ref_path

                q_worker.put(fwd_dict)
                q_worker.put(rev_dict)
                n_total += 2

        # Set the terminate condition for each worker thread.
        [q_worker.put(None) for _ in range(self.cpus)]

        # Create each of the processes
        p_workers = [mp.Process(target=self._worker,
                                args=(q_worker, q_writer, q_results))
                     for _ in range(self.cpus)]

        p_writer = mp.Process(target=self._writer, args=(q_writer, n_total))

        # Start each of the threads.
        try:
            # Start the writer and each processing thread.
            p_writer.start()
            for p_worker in p_workers:
                p_worker.start()

            # Wait until each worker has finished.
            for p_worker in p_workers:
                p_worker.join()

                # Gracefully terminate the program.
                if p_worker.exitcode != 0:
                    raise Exception('FastANI returned a non-zero exit code.')

            # Stop the writer thread.
            q_writer.put(None)
            p_writer.join()

        except Exception:
            for p in p_workers:
                p.terminate()
            p_writer.terminate()
            raise

        # Process and return each of the results obtained
        path_to_gid = {v: k for k, v in dict_paths.items()}
        q_results.put(None)
        return self._parse_result_queue(q_results, path_to_gid)

    def _worker(self, q_worker, q_writer, q_results):
        """Operates FastANI in list mode.

        Parameters
        ----------
        q_worker : mp.Queue
            A multiprocessing queue containing the available jobs.
        q_writer : mp.Queue
            A multiprocessing queue to track progress.
        q_results : mp.Queue
            A multiprocessing queue containing raw results.
        """
        while True:
            # Retrieve the next item, stop if the sentinel is found.
            job = q_worker.get(block=True)
            if job is None:
                break

            # Extract the values
            q = list(job.get('q').values())[0] \
                    if job.get('q') is not None else None
            r = list(job.get('r').values())[0] \
                    if job.get('r') is not None else None
            ql = job.get('ql')
            rl = job.get('rl')

            # Create a temporary directory to write the lists to.
            with tempfile.TemporaryDirectory(prefix='vOTU_fastani_tmp') \
                    as dir_tmp:
                path_qry = os.path.join(dir_tmp, 'ql.txt') \
                        if ql is not None else None
                path_ref = os.path.join(dir_tmp, 'rl.txt') \
                        if rl is not None else None
                path_out = os.path.join(dir_tmp, 'output.txt')

                # Write to the query and reference lists
                self._maybe_write_list(ql, path_qry)
                self._maybe_write_list(rl, path_ref)

                # Run FastANI
                result = self.run_proc(q, r, path_qry, path_ref, path_out)
                q_results.put((job, result))
                q_writer.put(True)

        return True

    def _writer(self, q_writer, n_total):
        """The writer function, which reports the progress of the workers.

        Parameters
        ----------
        q_writer : mp.Queue
            A queue of genome ids which have been processed.
        n_total : int
            The total number of items to be processed.
        """
        with tqdm_log(unit='comparison', total=n_total) as p_bar:
            for _ in iter(q_writer.get, None):
                p_bar.update()

    def run_proc(self, q, r, ql, rl, output):
        """Runs the FastANI process.

        Parameters
        ----------
        q : str
            The path to the query genome.
        r : str
            The path to the reference genome.
        ql : str
            The path to the query list file.
        rl : str
            The path to the reference list file.
        output : str
            The path to the output file.

        Returns
        -------
        dict[str, dict[str, float]]
            The ANI/AF of the query genomes to the reference genomes.
        """
        args = ['fastANI']
        if self.minFrac:
            args.extend(['--minFraction', '0'])
        if q is not None:
            args.extend(['-q', q])
        if r is not None:
            args.extend(['-r', r])
        if ql is not None:
            args.extend(['--ql', ql])
        if rl is not None:
            args.extend(['--rl', rl])
        args.extend(['-o', output, '--fragLen', '500'])
        self.logger.debug(' '.join(args))
        proc = subprocess.Popen(args, stdout=subprocess.PIPE,
                                stderr=subprocess.PIPE, encoding='utf-8')
        stdout, stderr = proc.communicate()

        if proc.returncode != 0:
            self.logger.error('STDOUT:\n' + stdout)
            self.logger.error('STDERR:\n' + stderr)
            raise Exception('FastANI returned a non-zero exit code.')

        # Parse the output
        return self.parse_output_file(output)

    def parse_output_file(self, path_out):
        """Parses the resulting output file from FastANI.

        Parameters
        ----------
        path_out : str
            The path where the output file resides.

        Returns
        -------
        dict[str, dict[str, tuple[float, float]]]
            The ANI/AF of the query genomes to the reference genomes.
        """
        out = dict()
        if os.path.isfile(path_out):
            with open(path_out, 'r') as fh:
                for line in fh.readlines():
                    """FastANI version >=1.1 uses tabs instead of spaces 
                    to separate columns.  Preferentially try split with 
                    tabs first instead of split() in-case of spaces in 
                    the file path."""
                    try:
                        try:
                            path_qry, path_ref, ani, frac1, frac2 = \
                                    line.strip().split('\t')
                        except ValueError:
                            path_qry, path_ref, ani, frac1, frac2 = \
                                    line.strip().split(' ')
                            if not self._suppress_v1_warning:
                                mes = ('You are using FastANI v1.0, '
                                        'it is recommended that you '
                                        'update to a more recent version.')
                                self.logger.warning(mes)

                                self._suppress_v1_warning = True
                        af = round(float(frac1) / float(frac2), 2)
                        ani = float(ani)/100.0
                        if path_qry not in out:
                            out[path_qry] = {path_ref: (ani, af)}
                        elif path_ref not in out[path_qry]:
                            out[path_qry][path_ref] = (ani, af)
                    except Exception as e:
                        mes = f'Exception reading FastANI output: {repr(e)}'
                        self.logger.error(mes)
                        raise Exception(f'Unable to read line "{line}"')
        return out

    def _maybe_write_list(self, d_genomes, path):
        """Writes a query/reference list to disk.

        Parameters
        ----------
        d_genomes : dict[str, str]
            A dictionary containing the key as the accession and value as path.
        path : str
            The path to write the file to.
        """
        if d_genomes is None or path is None:
            return
        with open(path, 'w') as fh:
            for gid, gid_path in d_genomes.items():
                fh.write(f'{gid_path}\n')

    def _parse_result_queue(self, q_results, path_to_gid):
        """Creates the output dictionary given the results from FastANI

        Parameters
        ----------
        q_results : mp.Queue
            A multiprocessing queue containing raw results.
        path_to_gid : dict[str ,str]
            A dictionary containing the file path to genome id.

        Returns
        -------
        dict[str, dict[str, dict[str, float]]]
            The ANI/AF of the query genome to all reference genomes.
        """
        out = dict()
        while True:
            q_item = q_results.get(block=True)
            if q_item is None:
                break

            job, result = q_item
            qry_gid = job['qry']

            for path_a, dict_b in result.items():
                for path_b, (ani, af) in dict_b.items():
                    gid_a, gid_b = path_to_gid[path_a], path_to_gid[path_b]

                    # This was done in the forward direction.
                    if gid_a == qry_gid:
                        ref_gid = gid_b
                    # This was done in the reverse direction.
                    elif gid_b == qry_gid:
                        ref_gid = gid_a
                    else:
                        raise Exception('FastANI results are malformed.')

                    # Take the largest ANI / AF from either pass.
                    if qry_gid not in out:
                        out[qry_gid] = {ref_gid: {'ani': ani, 'af': af}}
                    elif ref_gid not in out[qry_gid]:
                        out[qry_gid][ref_gid] = {'ani': ani, 'af': af}
                    else:
                        out[qry_gid][ref_gid]['ani'] = max(
                            out[qry_gid][ref_gid]['ani'], ani)
                        out[qry_gid][ref_gid]['af'] = max(
                            out[qry_gid][ref_gid]['af'], af)

        return out


#---------------------------------------
# functions for hierarchical clustering
#---------------------------------------

# might move them to utilities if needed in other places

import logging
import os
import sys

import numpy as np
import pandas as pd
import scipy.cluster
from scipy.spatial import distance as ssd


def genome_hierarchical_clustering(Ndb, **kwargs):
    '''
    Cluster ANI database
    Args:
        Ndb: result of secondary clustering
            (columns: ['query', 'reference', 'ani', 'alignment_coverage'])
            ('query' and 'reference' items are assumed to be paths)
    Keyword arguments:
        clusterAlg: how to cluster the database (default = single)
        S_ani: thershold to cluster at (default = .99)
        cov_thresh: minumum coverage to be included in clustering (default = .5)
        cluster: name of the cluster
        comp_method: comparison algorithm used
    Returns:
        list: [Cdb, {cluster:[linkage, linkage_db, arguments]}]
            (Cdb columns: 
                ['cluster', 'genome', 'threshold', 
                    'cluster_method', 'comparison_algorithm'])
    '''
    logging.debug('Clustering ANIn database')

    S_Lmethod = kwargs.get('clusterAlg', 'single')
    S_Lcutoff = 1 - kwargs.get('S_ani', .99)
    cov_thresh = float(kwargs.get('cov_thresh',0.5))
    cluster = kwargs.get('cluster','vOTU')
    comp_method = kwargs.get('comp_method', 'unk')

    Table = {'genome':[],'secondary_cluster':[]}

    # Handle the case where there's only one genome
    if len(Ndb['reference'].unique()) == 1:
        Table['genome'].append(
                os.path.basename(
                    Ndb['reference'].unique().tolist()[0]
                )
        )
        s = f'{cluster}_0' if cluster else f'{clust}'
        Table['secondary_cluster'].append(s)
        cluster_ret = []

    else:
        # Make linkage Ndb
        Ldb = make_linkage_Ndb(Ndb, **kwargs)

        # 3) Cluster the linkagedb
        Gdb, linkage = cluster_hierarchical(Ldb, linkage_method= S_Lmethod,
                                    linkage_cutoff= S_Lcutoff)

        # 4) Extract secondary clusters
        for clust, d in Gdb.groupby('cluster'):
            for genome in d['genome'].tolist():
                Table['genome'].append(genome)
                s = f'{cluster}_{clust}' if cluster else f'{clust}'
                Table['secondary_cluster'].append(s)

        # 5) Save the linkage
        arguments = {'linkage_method':S_Lmethod,'linkage_cutoff':S_Lcutoff,\
                    'comparison_algorithm':comp_method,'minimum_coverage':cov_thresh}
        cluster_ret = [linkage, Ldb, arguments]

    # Return the database
    Gdb = pd.DataFrame(Table)
    Gdb['threshold'] = S_Lcutoff
    Gdb['cluster_method'] = S_Lmethod
    Gdb['comparison_algorithm'] = comp_method

    return Gdb, cluster_ret


def iteratre_clusters(Bdb, Cdb, id='primary_cluster'):
    '''
    An iterator: Given Bdb and Cdb, yeild smaller Bdb's in the same cluster
    Args:
        Bdb: [genome, location]
        Cdb: [genome, id]
        id: what to iterate on (default = 'primary_cluster')
    Returns:
        list: [d(subset of b), cluster(name of cluster)]
    '''
    Bdb = pd.merge(Bdb,Cdb)
    for cluster, d in Bdb.groupby(id):
        yield d, cluster


def cluster_hierarchical(db, linkage_method= 'single', linkage_cutoff= 0.10):
    '''
    Perform hierarchical clustering on a symmetrical distiance matrix
    Args:
        db: DataFrame, result of db.pivot usually 
            ('reference' items as row index, 'query' items as columns, 
            'dist' between 'reference' and 'query' as value in matrix)
        linkage_method: passed to scipy.cluster.hierarchy.fcluster
        linkage_cutoff: distance to draw the clustering line (default = .1)
    Returns:
        list: [Cdb, linkage]
    '''
    # Save names
    names = list(db.columns)

    # Generate linkage dataframe
    arr =  np.asarray(db)
    arr[np.isnan(arr)] = 1
    try:
        arr = ssd.squareform(arr)
    except:
        logging.error("The database passed in is not symmetrical!")
        logging.error(arr)
        logging.error(names)
        sys.exit()
    linkage = scipy.cluster.hierarchy.linkage(arr, method= linkage_method)

    # Form clusters
    fclust = scipy.cluster.hierarchy.fcluster(linkage,linkage_cutoff, \
                    criterion='distance')
    # Make Cdb
    Cdb = _gen_cdb_from_fclust(fclust,names)

    return Cdb, linkage

def make_linkage_Ndb(Ndb, **kwargs):
    '''
    Filter the Ndb in accordance with the kwargs. Average reciprical ANI values
    Args:
        Ndb: result of secondary clutsering 
            (columns: ['query', 'reference', 'ani', 'alignment_coverage'])
    Keyword arguments:
        cov_thresh: minimum coverage threshold (default = 0.5)
    Return:
        DataFrame: pivoted DataFrame ready for clustering
            ('reference' items as row index, 'query' items as columns,
            'dist' between 'reference' and 'query' as value in maxtrix)
    '''
    d = Ndb.copy()
    cov_thresh = float(kwargs.get('cov_thresh',0.5))

    # Remove values without enough coverage
    d.loc[d['alignment_coverage'] <= cov_thresh, 'ani'] = 0

    # Make a linkagedb by averaging values and setting self-compare to 1
    add_avani(d)
    d['dist'] = 1 - d['av_ani']
    db = d.pivot('reference', 'query', 'dist')

    return db

def _gen_cdb_from_fclust(fclust,names):
    '''
    Make Cdb from the result of scipy.cluster.hierarchy.fcluster
    Args:
        fclust: result of scipy.cluster.hierarchy.fcluster
        names: list(db.columns) of the input dataframe
    Returns:
        DataFrame: Cdb (columns: ['cluster', 'genome'])
    '''
    Table={'cluster':[],'genome':[]}
    for i, c in enumerate(fclust):
        Table['cluster'].append(c)
        Table['genome'].append(names[i])

    return pd.DataFrame(Table)

def add_avani(db):
    '''
    add a column titled 'av_ani' to the passed in dataframe
    dataframe must have rows reference, querey, and ani
    Args:
        db: dataframe
    '''

    logging.debug('making dictionary for average_ani')
    combo2value = {}
    for i, row in db.iterrows():
        q, r = row['query'], row['reference']
        combo2value[f'{q}-vs-{r}'] = row['ani']

    logging.debug('list comprehension for average_ani')
    db['av_ani'] = [
        np.mean([combo2value[f'{q}-vs-{r}'], combo2value[f'{r}-vs-{q}']]) \
                if r != q else 1  for q, r in zip(db['query'].tolist(),
                    db['reference'].tolist())
    ]

    logging.debug('averageing done')
