"""
Handles exporting into various formats
"""

import itertools
import json
import logging
import sys
from heapq import heapreplace
from pathlib import Path
from random import shuffle
import pickle
import gzip
from collections import Counter

import networkit as nk
import igraph as ig
import jenkspy
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from ete3.clustering.clustvalidation import spearman_dist
from jinja2 import Environment, PackageLoader, select_autoescape
from scipy.cluster.hierarchy import linkage
from scipy.sparse import csr_matrix, csc_matrix
from scipy.spatial.distance import squareform
from skbio.tree import TreeNode as sktree
from scipy import stats
from tqdm import tqdm
from upsetplot import UpSet, from_memberships

from vcontact3.config import all_ranks, colors, extended_colors
from vcontact3.gbga import GBGA
from vcontact3.performance import Performance
from vcontact3.profiles import Profiles
from pprint import pprint

import seaborn as sns

plt.rcParams['svg.fonttype'] = 'none'

logger = logging.getLogger(__name__)

disable_ete = False
try:
    from ete3 import Tree, TreeStyle, AttrFace, add_face_to_node, TextFace, RectFace
except ImportError:
    disable_ete = True
    logger.warning('ETE3 TreeStyle might not be installed, rendering Trees may not be available. If you require '
                   'trees, please try to install via "python -m pip install PyQt5"')


# https://stackoverflow.com/a/61649667
def sublist_creator(x, n, sort=True):
    bins = [[0] for _ in range(n)]
    if sort:
        x = sorted(x)
    for i in x:
        least = bins[0]
        least[0] += i
        least.append(i)
        heapreplace(bins, least)
    return [x[1:] for x in bins]


class Export(object):
    """
        Export information

        Attributes:

    """

    def __init__(self, gbga: GBGA, performance: Performance, profile: Profiles,
                 to_export: list, output_dir: Path, nat_breaks: int = 1,
                 target_ranks: list = [], min_members: int = 5):
        """
        :param gbga: GBGA object
        :param performance: Performance object
        :param profile: Profiles object
        :param to_export: List of output types to export
        :param output_dir: Output directory
        :param nat_breaks: How many breaks to use applying natural breakpoints to networks
        """

        if isinstance(gbga, GBGA):
            self.genomes_overview = gbga.genomes_overview
            self.final_overview = gbga.final_overview
            self.identities = gbga.identities
        else:
            logger.error('This isn\'t a GBGA object. Please send this error message to the author.')
            sys.exit(1)

        if isinstance(profile, Profiles):
            # Don't need to pull everything from profile, just need the profile itself
            self.profile_profiles = {}
            for identity in ['0.3', '0.4', '0.5', '0.6', '0.7']:
                self.profile_profiles[identity] = profile.identities[identity]['crosstab_fp']

        else:
            logger.error('This isn\'t a Profiles object. Please send this error message to the author.')
            sys.exit(1)

        if isinstance(performance, Performance):
            self.overview_performances = performance.overview_performances
        else:
            logger.error('This isn\'t a Performance object. Please send this error message to the author.')
            sys.exit(1)

        tqdm.pandas()

        # Possible export types
        # Pairwise heatmap, Traditional PC profile

        exports_dir = output_dir / 'exports'
        if not exports_dir.exists():
            exports_dir.mkdir(parents=True, exist_ok=True)
        logger.info(f'Creating exports directory: {exports_dir}')

        logger.info(f'Exporting the following types: {", ".join(to_export)}')

        # Assignments table
        logger.info('Exporting the final assignments table')
        assignments_df = export_assignments(self.final_overview, self.genomes_overview, exports_dir)

        logger.info('Exporting performance metrics')
        export_performance(self.overview_performances, exports_dir)

        if 'hierarchy' in to_export:
            export_dsj3_tree(assignments_df, exports_dir)

        # Distance matrix serves as the foundation for all outputs
        for identity in ['0.3']:  # Identities - until merging networks - are only part of assignment picture...

            filtered_dist_ntw_fp = self.identities[identity]['filtered_dist_network_fp']
            filtered_shared_ntw_fp = self.identities[identity]['filtered_shared_network_fp']
            filtered_network_tracker_fp = self.identities[identity]['filtered_network_tracker_fp']

            with open(filtered_network_tracker_fp, 'rb') as f:  # Pairs with dist_ntw
                filtered_network_tracker = pickle.load(f)
                node_index_map = {v['idx']: k for k, v in filtered_network_tracker.items()}

            distG = nk.readGraph(str(filtered_dist_ntw_fp), nk.Format.NetworkitBinary)
            sharedG = nk.readGraph(str(filtered_shared_ntw_fp), nk.Format.NetworkitBinary)

            # Annotate network prep
            # We only want nodes in the network and columns that are useful

            if nat_breaks > 1:
                breaks_df = natural_breaks(distG, node_index_map, num_breaks=nat_breaks)
                # TODO Trust breaks_df index to be equivalent to G indices?
                # breaks_df built entirely by G, and node_index_map built alongside G, from same shared dist matrix
            elif nat_breaks == 1:
                breaks_df = pd.DataFrame()
            else:
                breaks_df = pd.DataFrame()
                logger.error(f'Unable to determine the number of breaks: {nat_breaks}')
                exit(1)

            if 'cytoscape' in to_export:
                cytoscape_dir = exports_dir / 'cytoscape'
                if not cytoscape_dir.exists():
                    cytoscape_dir.mkdir(parents=True, exist_ok=True)
                export_cytoscape(distG, breaks_df, node_index_map, assignments_df, cytoscape_dir)

            if 'pyupset' in to_export:

                ntw_cols = ['source', 'target', 'distance', 'shared']
                ntw_data = []
                for (u, v, w) in distG.iterEdgesWeights():
                    ntw_data.append((node_index_map[u], node_index_map[v], w, sharedG.weight(u, v)))  # distance

                filtered_ntw_df = pd.DataFrame(ntw_data, columns=ntw_cols)

                cytoscape_dir = exports_dir / 'cytoscape'
                if not cytoscape_dir.exists():
                    cytoscape_dir.mkdir(parents=True, exist_ok=True)

                export_upset(filtered_ntw_df, assignments_df.set_index('RefSeqID'), cytoscape_dir)

            if 'd3js' in to_export:

                d3js_dir = exports_dir / 'd3js'
                if not d3js_dir.exists():
                    d3js_dir.mkdir(parents=True, exist_ok=True)

                export_d3js_network(distG, breaks_df, node_index_map, assignments_df, d3js_dir)

            # Below are all realm-based (especially tree, which can't have disconnected parts)
            if any(e in to_export for e in ['profiles', 'tree', 'completeness']):  # No point doing below if not using

                genomes_overview_df = self.identities[identity]['genomes_overview']  # cmpnt + realm prediction (at id)
                genomes_overview_df = genomes_overview_df.drop(columns='genome_id')

                if 'profiles' in to_export:

                    profile_fp = self.profile_profiles[identity]
                    with gzip.open(profile_fp, 'rb') as profile_fh:
                        profile_df = pickle.load(profile_fh)

                    profiles_dir = exports_dir / 'pc_profiles'
                    if not profiles_dir.exists():
                        profiles_dir.mkdir(parents=True, exist_ok=True)

                    export_profile(profile_df, assignments_df, profiles_dir, target_ranks, min_members)

                if 'completeness' in to_export:

                    profile_fp = self.profile_profiles[identity]
                    with gzip.open(profile_fp, 'rb') as profile_fh:
                        profile_df = pickle.load(profile_fh)

                    completeness_dir = exports_dir / 'completeness'
                    if not completeness_dir.exists():
                        completeness_dir.mkdir(parents=True, exist_ok=True)

                    export_completeness(profile_df, assignments_df, completeness_dir, completeness_members=5)

                if 'tree' in to_export:  # One tree per realm

                    trees_dir = exports_dir / 'trees'
                    if not trees_dir.exists():
                        trees_dir.mkdir(parents=True, exist_ok=True)

                    # Don't load what you don't need to load until needed...
                    distance_fp = self.identities[identity]['distance_matrix_fp']
                    with gzip.open(distance_fp, 'rb') as distance_fh:
                        distance_df = pickle.load(distance_fh)

                    # Genomes overview is tied to identity
                    export_tree(distance_df, genomes_overview_df, assignments_df, trees_dir,
                                self.profile_profiles[identity],
                                5, calc_silhouette=False, disable_rendering=disable_ete)
                    # Silhouette is not enabled


def export_performance(performance_data: pd.DataFrame, output_dir: Path):
    # Reorganize dictionary
    reorganized_performances = {}
    for realm, ranks_d in performance_data.items():
        for rank, rank_perf in ranks_d.items():
            rank_perf['rank'] = rank
            rank_perf['realm'] = realm
            reorganized_performances[len(reorganized_performances)] = rank_perf

    performance_df = pd.DataFrame().from_dict(reorganized_performances, orient='index')
    reordered_cols = performance_df.columns.tolist()
    reordered_cols[0:0] = ['realm', 'rank']
    reordered_cols = reordered_cols[:-2]
    performance_df = performance_df[reordered_cols]

    performance_df.to_csv(output_dir / 'performance_metrics.csv')


def export_assignments(pre_assignments: pd.DataFrame, component_df: pd.DataFrame, output_dir: Path):

    """
    :param pre_assignments: RefSeqID index (+ user names) and GenomeName (no user names)
    :param component_df: genome_id index
    :param output_dir:
    :return:
    """

    def get_network_placement(component_info: pd.DataFrame):

        network_cols = [f'network ({c})' for c in [0.3, 0.4, 0.5, 0.6, 0.7]]
        vals = component_info[network_cols].fillna('None').values

        counts = Counter(vals)

        # Logic for this block assumes that ALL networks were the same
        # Are they really going to be the same everytime?

        if set(counts.keys()) == {'None'}:
            return 'Fully clustered'

        if set(counts.keys()) == {'edgeless'}:
            return 'No edges found'

        if set(counts.keys()) == {'filtered'}:
            return 'Edges removed by min PCs'

        if set(counts.keys()) == {'isolated'}:
            return 'Edges removed by filtering'

        # TODO Logic here is in the right direction by wrong
        # TODO Check if isolated *always* appears b4 edgeless: ['isolated' 'isolated' 'edgeless' 'edgeless' 'edgeless']
        if set(counts.keys()).issubset({'isolated', 'edgeless'}):
            if counts['isolated'] > counts['edgeless']:
                return 'Removed then edgeless'
            else:
                return 'No edges then removed'

        if set(counts.keys()).issubset({'None', 'isolated'}):
            return 'Lower clustered then removed'

        if set(counts.keys()).issubset({'None', 'edgeless'}):
            return 'Lower clustered then no edges'

        if set(counts.keys()).issubset({'None', 'isolated', 'edgeless'}):
            return 'Lower clustered then removed then edgeless'

    cleaned_cols = [[f'{ar} (Reference)', f'{ar} (prediction)'] for ar in all_ranks]
    cleaned_cols = list(itertools.chain.from_iterable(cleaned_cols))
    cleaned_cols.insert(0, 'GenomeName')

    final_cols = cleaned_cols.copy()
    final_cols[1:1] = ['RefSeqID', 'Proteins', 'Reference', 'Size (Kb)']

    component_df['network'] = component_df.apply(get_network_placement, axis=1)
    subcomponent_df = component_df['network']

    final_assignments = pre_assignments.reset_index()[final_cols]
    final_assignments = final_assignments.merge(subcomponent_df, how='left', left_on='RefSeqID', right_index=True)

    # TODO Temporary fix. Permanent fix needs to be done in GBGA
    final_assignments['GenomeName'] = final_assignments['GenomeName'].fillna(final_assignments['RefSeqID'])

    # Sort
    sort_cols = ['realm (prediction)', 'phylum (prediction)', 'class (prediction)', 'order (prediction)',
                 'family (prediction)', 'subfamily (prediction)', 'genus (prediction)']
    final_assignments = final_assignments.sort_values(by=sort_cols)

    final_assignments_fp = output_dir / 'final_assignments.csv'
    final_assignments.to_csv(final_assignments_fp, index_label='index')  # For clarity

    return final_assignments


def export_dsj3_tree(assignments_df: pd.DataFrame, output_dir: Path):
    pass


def natural_breaks(graph: nk.Graph, mapper: dict, num_breaks: int = 1):

    # Because these networks can become prohibitively large, will "split" them into connected components
    network_df = pd.DataFrame(data=list(mapper.values()),
                              index=range(graph.numberOfNodes()),
                              columns=['Node'])  # It appears that mapper.values() == graph.iterNodes() indices
    network_df['CC'] = np.nan
    network_df['CC Size'] = np.nan

    cc = nk.components.ConnectedComponents(graph)
    cc.run()

    # TODO This is like the 3rd time this is called
    for component, component_indices in enumerate(cc.getComponents()):
        component_nodes = [mapper[node] for node in component_indices]
        network_df.loc[network_df['Node'].isin(component_nodes), ['CC', 'CC Size']] = [str(component), len(component_nodes)]

    # Some logic taken from https://pbpython.com/natural-breaks.html
    breaks = jenkspy.jenks_breaks(network_df['CC Size'], n_classes=num_breaks)

    network_df['breaks'] = pd.cut(network_df['CC Size'], bins=breaks, include_lowest=True,
                                  labels=[f'bin_{x}' for x in range(num_breaks)])

    return network_df


def build_json_from_igraph(g: ig.Graph, json_format: str):

    logger.info('Building cytoscape network from iGraph')
    nodes = []
    for node in g.vs:
        node_data = {
            "id": str(node.index),
            "label": str(node['RefSeqID'])
        }

        for k, v in node.attributes().items():
            if pd.notnull(v):
                node_data[k] = str(v)

        if json_format == 'cytoscape':
            nodes.append({"data": node_data})
        elif json_format == 'd3js':
            nodes.append(node_data)

    if json_format == 'cytoscape':
        edges = [
            {"data": {"source": str(edge.source), "target": str(edge.target), "distance": str(edge['distance'])
                      }} for edge in g.es]
    elif json_format == 'd3js':
        edges = [{"source": str(edge.source), "target": str(edge.target)} for edge in g.es
                 ]
    else:
        edges = []

    # Create a dictionary containing nodes and edges
    if json_format == 'cytoscape':
        cytoscape_data = {
            "elements": {
                "nodes": nodes,
                "edges": edges
            }
        }
    elif json_format == 'd3js':
        cytoscape_data = {
            "links": edges,
            "nodes": nodes
        }
    else:
        cytoscape_data = {}

    return cytoscape_data


def build_json_from_networkit(g: nk.Graph, mapper: dict, annotation_df: pd.DataFrame, json_format: str):

    annotation_faster = annotation_df.set_index('RefSeqID', drop=True)

    logger.info('Building cytoscape network from NetworKit')
    nodes = []
    for n in g.iterNodes():
        node_name = str(mapper[n])
        node_data = {
            "id": str(n),
            "label": node_name
        }

        node_annotations = annotation_faster.loc[node_name].to_dict()

        for k, v in node_annotations.items():
            if pd.notnull(v):
                node_data[k] = str(v)

        if json_format == 'cytoscape':
            nodes.append({"data": node_data})
        elif json_format == 'd3js':
            nodes.append(node_data)

    if json_format == 'cytoscape':
        edges = [
            {"data": {"source": str(u), "target": str(v), "weight": str(w)
                      }} for (u, v, w) in g.iterEdgesWeights()]
    elif json_format == 'd3js':
        edges = [{"source": str(u), "target": str(v), "weight": float(w)} for (u, v, w) in g.iterEdgesWeights()
                 ]
    else:
        edges = []

    # Create a dictionary containing nodes and edges
    if json_format == 'cytoscape':
        cytoscape_data = {
            "elements": {
                "nodes": nodes,
                "edges": edges
            }
        }
    elif json_format == 'd3js':
        cytoscape_data = {
            "links": edges,
            "nodes": nodes
        }
    else:
        cytoscape_data = {}

    return cytoscape_data


def export_cytoscape(graph: nk.Graph, breaks_df: pd.DataFrame, mapper_dict: dict, annotation_df: pd.DataFrame, output_dir: Path):

    # Write json to file
    cyto_fp = output_dir / 'graph.cyjs'

    logger.info(f'Writing cytoscape data to {cyto_fp}')
    with open(cyto_fp, 'w') as cyto_fh:
        # TODO Implement shared gene so Cytoscape can have dual edges
        json.dump(build_json_from_networkit(graph, mapper_dict, annotation_df, 'cytoscape'),
                  cyto_fh, sort_keys=True, indent=4)

    if not breaks_df.empty:

        for break_group, break_df in breaks_df.groupby(by='breaks'):

            df_indices = break_df.index.tolist()

            # Even IF indices don't correlate across all nodes, they should be internally consistent w.r.t. breaks + G

            subgraph = nk.graphtools.subgraphFromNodes(graph, df_indices, compact=False)

            sub_cyto_fp = output_dir / f'graph.{break_group}.cyjs'
            # mapper_dict should be identical - compact=False means no node indices have changed
            sub_cyto_data = build_json_from_networkit(subgraph, mapper_dict, annotation_df, 'cytoscape')
            logger.info(f'Breaks enabled, writing break group {break_group} to {sub_cyto_fp}')
            with open(sub_cyto_fp, 'w') as sub_cyto_fh:
                json.dump(sub_cyto_data, sub_cyto_fh, sort_keys=True, indent=4)


def export_upset(network_edges: pd.DataFrame, taxon_info: pd.DataFrame, output_dir: Path):

    # TODO Add in more than realms, as realms will have little or no connection

    network_edges_df = network_edges.copy()
    rank = 'realm (prediction)'

    # TODO Disentangle shared realm edges
    upset_fp = output_dir / 'UpSet_Realms.png'
    logger.info(f'Exporting UpSetPlot of realms to {upset_fp}')

    genome_lengths = taxon_info[['Size (Kb)']].to_dict(orient='index')
    genome_proteins = taxon_info[['Proteins']].to_dict(orient='index')
    phylogenetic = taxon_info[[rank]].to_dict(orient='index')  # For mapping

    # Format network for use with pyupset
    network_edges_df[f'source_{rank}'] = network_edges_df['source'].progress_apply(lambda x: phylogenetic[x][rank])
    network_edges_df[f'target_{rank}'] = network_edges_df['target'].progress_apply(lambda x: phylogenetic[x][rank])

    network_edges_df[f'source_size'] = network_edges_df['source'].progress_apply(lambda x: genome_lengths[x]['Size (Kb)'])
    network_edges_df[f'target_size'] = network_edges_df['target'].progress_apply(lambda x: genome_lengths[x]['Size (Kb)'])
    network_edges_df[f'avg_size'] = network_edges_df.progress_apply(lambda x: np.mean([x.source_size, x.target_size]),
                                                                    axis=1)

    network_edges_df[f'source_proteins'] = network_edges_df['source'].progress_apply(lambda x: genome_proteins[x]['Proteins'])
    network_edges_df[f'target_proteins'] = network_edges_df['target'].progress_apply(lambda x: genome_proteins[x]['Proteins'])
    network_edges_df[f'avg_proteins'] = network_edges_df.progress_apply(lambda x: np.nanmean([x.source_proteins,
                                                                                              x.target_proteins]),
                                                                        axis=1)

    network_edges_df['diff'] = network_edges_df[f'source_{rank}'] != network_edges_df[f'target_{rank}']  # True if not equal...
    # No longer a need to filter by minimum shared genes, as everything going into this has already been filtered
    # Can't union np.nan + str...
    ntw_df = network_edges_df.loc[(pd.notnull(network_edges_df[f'source_{rank}']) &
                                   pd.notnull(network_edges_df[f'source_{rank}']))]

    ntw_df["union"] = ntw_df[[f"source_{rank}", f"target_{rank}"]].apply("-".join, axis=1)

    from_membership = from_memberships(ntw_df.union.str.split('-'), data=ntw_df)

    upset = UpSet(from_membership, min_subset_size=1, show_counts=True)
    catplot_params = {'elements': 5, 'fliersize': 2, 'linewidth': 1}
    upset.add_catplot(value='distance', kind='box', color='blue', **catplot_params)  # strip?
    upset.add_catplot(value='avg_size', kind='box', color='red', **catplot_params)  # violin too small
    upset.add_catplot(value='shared', kind='box', color='green', **catplot_params)

    fig_id = plt.figure()

    plots = upset.plot(fig=fig_id)
    plots['intersections'].set_yscale('log')
    plots['extra1'].set(ylabel='Distance')
    plots['extra2'].set(ylabel='Genome Length (Kbp)')
    plots['extra3'].set(ylabel='Shared HMMs')
    # plots['extra2'].set_yscale('log')
    plots['extra3'].set_yscale('log')

    # upset.style_subsets(present=["cat1", "cat2"],
    #                     facecolor="blue",
    #                     label="shared")

    # d.intersections = df w/ t/f, and group totals
    # d.total = int, total of specific group
    # d.totals = series with totals

    plt.savefig(upset_fp, dpi=1200)
    plt.close()


def export_d3js_network(graph: nk.Graph, breaks_df: pd.DataFrame, mapper_dict: dict, annotation_df: pd.DataFrame,
                        output_dir: Path):
    # Previously removed largest CC to reduce size of network that user might need to open
    # Instead, distribute it evenly
    # Actually, instead, distribute it in chunks that contains similarly-sized chunks
    # largest_cc = max(nx.connected_components(uGraph), key=len)
    # ugh, just create a dataframe initially and iterate through that... none of this doing 5 things and combine

    d3js_fp = output_dir / 'd3_graph.json'

    logger.info(f'Writing d3js interactive network to {d3js_fp}')

    d3js_data = build_json_from_networkit(graph, mapper_dict, annotation_df, 'd3js')

    logger.info(f'Writing data to be used in d3js to {d3js_fp}')
    with open(d3js_fp, 'w') as d3js_fh:
        json.dump(d3js_data, d3js_fh, sort_keys=True, indent=4)

    json_data = json.dumps(d3js_data)
    loaded_r = json.loads(json_data)

    env = Environment(
        loader=PackageLoader('vcontact3', 'templates'),
        autoescape=select_autoescape(['html', 'xml'])
    )

    template = env.get_template('network.html')

    html_fp = output_dir / 'graph.html'
    logger.info(f'Writing HTML d3js to {html_fp}')
    with open(html_fp, 'w') as html_fh:
        html_fh.write(template.render(json_data=loaded_r))

    if not breaks_df.empty:

        for break_group, break_df in breaks_df.groupby(by='breaks'):
            df_indices = break_df.index.tolist()
            subgraph = nk.graphtools.subgraphFromNodes(graph, df_indices, compact=False)

            subdata = build_json_from_networkit(subgraph,mapper_dict, annotation_df, 'd3js')

            # Write json to file
            subgraph_fp = output_dir / f'graph.{break_group}.d3.json'
            with open(subgraph_fp, 'w') as subgraph_fh:
                json.dump(subdata, subgraph_fh, sort_keys=True, indent=4)

            sub_json_data = json.dumps(subdata)
            sub_loaded_r = json.loads(sub_json_data)

            env = Environment(
                loader=PackageLoader('vcontact3', 'templates'),
                autoescape=select_autoescape(['html', 'xml'])
            )

            template = env.get_template('network.html')

            subhtml_fp = output_dir / f'graph.{break_group}.html'
            with open(subhtml_fp, 'w') as subhtml_fh:
                subhtml_fh.write(template.render(json_data=sub_loaded_r))


def export_tree(distance_df: pd.DataFrame, component_overview: pd.DataFrame, genomes_df: pd.DataFrame,
                output_dir, pc_profile: Path, min_genomes: int = 5, calc_silhouette=False, disable_rendering=False):

    """
    Export tree

    :param distance_df: pd.DataFrame()
    :param component_overview:
    :param genomes_df:
    :param output_dir:
    :param pc_profile: Pickled crosstab of profile
    :param min_genomes:
    :param calc_silhouette:
    :param disable_rendering:
    :return:
    """

    if calc_silhouette:
        logger.info(f'Calculating Silhouette enabled')

        pc_profile_fp = pc_profile
        with gzip.open(pc_profile_fp, 'rb') as pc_profile_fh:
            pc_profile_df = pickle.load(pc_profile_fh)

    color_adj_genomes = genomes_df.copy()
    rank_predictions = [f'{r} (prediction)' for r in all_ranks]

    for rp in rank_predictions:

        uniques = color_adj_genomes[rp].unique().tolist()

        if len(uniques) <= len(colors):
            shuffle(colors)
            color_mapping = {k: v for k, v in zip(color_adj_genomes[rp].unique().tolist(),
                                                  itertools.cycle(colors))}
            color_adj_genomes[f'{rp} color'] = color_adj_genomes[rp].map(color_mapping)
        else:
            # There's more than 24 potential colors
            shuffle(extended_colors)  # Or else entire columns will be the same 1st position color
            color_mapping = {k: v for k, v in zip(color_adj_genomes[rp].unique().tolist(),
                                                  itertools.cycle(extended_colors))}
            color_adj_genomes[f'{rp} color'] = color_adj_genomes[rp].map(color_mapping)

    realm_counts = Counter()

    # Break down into each component, as each tree *must be* connected
    for component, component_df in component_overview.groupby(by='component'):
        component_members = component_df.index.tolist()

        if len(component_members) < min_genomes:
            continue

        component_dist_df = distance_df.loc[component_members, component_members].copy()

        sp_matrix = component_dist_df.copy()
        sp_matrix = sp_matrix.sparse.to_coo().tocsr()

        # sp_matrix.setdiag(0)
        sp_matrix = sp_matrix.astype(np.float32)
        sp_matrix = sp_matrix.toarray()
        sp_matrix[sp_matrix == 0] = 1
        np.fill_diagonal(sp_matrix, 0)

        linkage_matrix = linkage(squareform(sp_matrix), method='average')

        # Create tree directly, instead of using to_tree(linkage)

        # Build scikit-bio tree from linkage matrix
        skbiotree = sktree.from_linkage_matrix(linkage_matrix=linkage_matrix, id_list=component_members)
        tree = Tree().from_skbio(skbio_tree=skbiotree)

        # Rename for ICTV
        # for n in tree.traverse():
        #
        #     if n.is_leaf():
        #         n.add_feature('taxid', n.name)
        #         record = phylogenetic_df.loc[n.name]
        #
        #         accession = record['Accession']
        #         if pd.notnull(accession):
        #             n.name = accession

        if calc_silhouette:

            # Inspired by ETE3 Toolkit clustervalidation.py
            for n in tqdm(tree.traverse(), unit=' nodes'):

                # Need to get silhouette for the node

                if n.is_root():
                    n.add_feature('silhouette', 0)
                    continue

                if n.is_leaf():  # No score
                    continue

                n_members = n.get_leaf_names()
                df_avg = pc_profile_df.loc[n_members].mean(axis=0)
                n_profile = df_avg.values

                silhouette = []

                for sis in n.get_sisters():

                    if sis.is_leaf():
                        sis_profile = pc_profile_df.loc[sis.name].values  # Should just be one

                    else:  # is internal, therefore need average
                        sis_members = sis.get_leaf_names()
                        sis_avg = pc_profile_df.loc[sis_members].mean(axis=0)
                        sis_profile = sis_avg.values

                    for i in n.iter_leaves():

                        if i.is_leaf():
                            i_profile = pc_profile_df.loc[i.name].values
                        else:  # is internal, therefore need average
                            i_members = i.get_leaf_names()
                            i_avg = pc_profile_df.loc[i_members].mean(axis=0)
                            i_profile = i_avg.values

                        # Get distances
                        # TODO module 'pandas._libs.sparse' has no attribute 'sparse_eq_uint8'
                        a = spearman_dist(i_profile, n_profile) * 2  # Intracluster distance
                        b = spearman_dist(i_profile, sis_profile)  # Intercluster distance

                        if (b - a) == 0.0:
                            s = 0.0
                        else:
                            s = (b - a) / max(a, b)

                        silhouette.append(s)

                silhouette = np.mean(silhouette)

                n.add_feature('silhouette', f'{silhouette:.3f}')

        realms = genomes_df.loc[genomes_df['RefSeqID'].isin(component_members), 'realm (prediction)'].unique()

        if len(realms) == 1:
            namer = f'{realms[0].replace(" ", "_")}'
        else:
            logger.warning(f'Multiple realms detected: {",".join(realms)}. This may not be an issue.')
            namer = "_".join(realms).replace(" ", "_")

        if namer not in realm_counts:
            realm_counts[namer] = 1
        else:
            realm_counts[namer] += 1

        # Save tree
        newick_dir = output_dir / namer  # Will be v3_output/exports/trees/realm_name...
        if not newick_dir.exists():
            newick_dir.mkdir(parents=True, exist_ok=True)

        tree_fp = newick_dir / f'{namer}_{realm_counts[namer]}_tree.nw'
        logger.info(f'Writing newick-formatted tree to {tree_fp}')
        tree.write(format=1, outfile=tree_fp, features=[])

        if not disable_rendering:

            ts = TreeStyle()
            ts.branch_vertical_margin = 5  # Spread out a little bit
            ts.show_leaf_name = False
            ts.show_branch_length = True
            ts.show_branch_support = False
            # ts.force_topology = True  # Prints out a warning

            # vs TextFace(node.name, tight_text=True) ?? add_face_to_node(F, node, column=0, position="branch-right")
            nameFace = AttrFace("name",
                                fsize=8)

            def my_layout(node):

                if node.is_leaf():
                    node.img_style["size"] = 2
                    node.img_style["hz_line_width"] = 1
                    node.img_style["vt_line_width"] = 1
                    node.img_style["hz_line_color"] = "#000000"  # cccccc = Gray80
                    add_face_to_node(nameFace, node, 0, aligned=True)  # Names

                else:  # Internal

                    node.img_style["hz_line_width"] = 1
                    node.img_style["vt_line_width"] = 1
                    node.img_style["hz_line_color"] = "#000000"

                    try:
                        node_silhouette = float(node.silhouette)

                        # If silhouette is good, creates a green bubble
                        if node_silhouette > 0:  # f'{val:.2f}'
                            validationFace = TextFace(f" {node_silhouette:.2f} ",
                                                      "Verdana", 10, "#000000")  # Red?
                            node.img_style["fgcolor"] = "#056600"

                        elif node_silhouette == 0:
                            validationFace = TextFace(f" {node_silhouette:.2f}  ",
                                                      "Verdana", 10, "#000000")
                            node.img_style["fgcolor"] = "#4D3300"  # Brown

                        elif node_silhouette < 0:
                            validationFace = TextFace(f" {node_silhouette:.2f}  ",
                                                      "Verdana", 10, "#000000")
                            node.img_style["fgcolor"] = "#940000"

                        else:
                            if np.isnan(node_silhouette):
                                validationFace = TextFace(f" {node_silhouette} ",
                                                          "Verdana", 10, "#940000")
                                node.img_style["fgcolor"] = "#CCCCCC"

                        node.img_style["shape"] = "circle"
                        if -1 <= node_silhouette <= 1:
                            node.img_style["size"] = 10 + int((abs(node_silhouette) * 5) ** 2)  # 15, *10

                        add_face_to_node(validationFace, node, 0)  # Can use 0 because other else is for leaves

                    except AttributeError:
                        pass

            # Add title
            title = ''
            taxons = ['realm (prediction)']  # Should this account for possible unknowns?
            leaves = tree.get_leaf_names()
            taxon_counts = color_adj_genomes.loc[color_adj_genomes['RefSeqID'].isin(leaves), taxons]

            for taxon in taxons:
                title += f'{taxon}: ' if not title else f' | {taxon}: '
                for taxon_name, taxon_count in taxon_counts[taxon].value_counts().items():
                    title += f'{taxon_name} ({taxon_count})'

            ts.title.add_face(TextFace(title, fsize=14), column=0)

            # Append taxonomy blocks
            for n in tree.traverse():

                if n.is_leaf():

                    for pos, rank in enumerate(rank_predictions, start=1):
                        color = color_adj_genomes.loc[color_adj_genomes['RefSeqID'] == n.name][f'{rank} color'].iloc[0]

                        n.add_face(RectFace(20, 20, color, color), pos, position='aligned')
                        # label={'text': rank, 'fontsize': 8, 'color': 'black'}

            for pos, rank in enumerate(rank_predictions, start=1):
                # labelFace = RectFace(20, 0, '#fff', '#eee',
                #                      label={'text': rank.split('-')[-1].capitalize(), 'fontsize': 8, 'color': 'black'})
                labelFace = TextFace(rank.split('-')[-1].capitalize())
                labelFace.rotation = -90
                ts.aligned_foot.add_face(labelFace, pos)

            ts.layout_fn = my_layout
            tree_pdf_fp = newick_dir / f'{namer}_{realm_counts[namer]}_tree.pdf'
            tree.render(str(tree_pdf_fp), tree_style=ts, w=int(1920), units="px")  # h=int(1080)


def export_completeness(crosstab_df: pd.DataFrame, assignments_df: pd.DataFrame,
                        output_dir: Path, completeness_members: int = 5):
    """
    :crosstab_df: DataFrame containing cross-tabulation of user genes with core genes
    :assignments_df: DataFrame containing genome assignments
    :output_dir: Path to the output directory
    :completeness_members: Number of genomes required for completeness calculation (default: 5)
    """

    logger.info(f'Exporting completeness')

    # Instead of for loop with checks every time, do it once here
    counts = assignments_df['genus (prediction)'].value_counts()
    valid_values = counts[counts >= completeness_members].index
    filtered_assignments_df = assignments_df[assignments_df['genus (prediction)'].isin(valid_values)]

    coverage = {}

    csr_crosstab = csr_matrix(crosstab_df.sparse.to_coo())
    boolean_csr_crosstab = csr_crosstab > 0
    csc_crosstab = boolean_csr_crosstab.tocsc()  # Easier filtering

    # Need when pd sparse matrix is built
    original_indices = crosstab_df.index.to_numpy()
    original_columns = crosstab_df.columns.to_numpy()

    previous_seen = set()
    for target, target_df in tqdm(filtered_assignments_df.groupby(f'genus (prediction)'),
                                  total=len(filtered_assignments_df['genus (prediction)'].unique()),
                                  unit=' genera'):

        target_members = target_df['RefSeqID'].tolist()
        total_members = len(target_members)

        # Keep it in sparse, trying to use dataframes is a crutch and causing confusion
        target_mask = np.isin(original_indices, target_members)
        target_csc_matrix = csc_crosstab[target_mask]

        # Filter columns: Identify columns with sums greater than 1
        gene_sums = np.asarray(target_csc_matrix.sum(axis=0)).flatten()
        non_singleton_genes = gene_sums > 1

        filtered_target_csc_matrix = target_csc_matrix[:, non_singleton_genes]

        # Ensure we can map back to original data
        selected_indices = [original_indices[i] for i in np.where(target_mask)[0]]
        selected_columns = [original_columns[i] for i in np.where(non_singleton_genes)[0]]

        # Check to see if references are present
        # Filter on refs and get their genus-exclusive markers
        ref_members = target_df.loc[target_df['Reference'] == True]['RefSeqID'].tolist()
        total_refs = len(ref_members)

        if ref_members:  # Novel group
            # Handle both 100% references and >0% references, or else would avoid calculating completeness
            # For references, this might have some be >100% or <100% "complete", but that also shows what degree of
            # variation can be in the population, i.e. if refs are +/-10%, user can also leverage that information

            # refs refer to references, core genes refers to refs + core genes

            ref_mask = np.isin(target_members, ref_members)
            ref_csc_matrix = filtered_target_csc_matrix[ref_mask, :]

            current_indices = [selected_indices[i] for i in np.where(ref_mask)[0]]

            # Identify core genes among references
            num_rows = ref_csc_matrix.shape[0]  # TODO Should this technically be num_ref_rows?
            # Count non-zero elements in each column
            column_nonzeros = np.array(ref_csc_matrix.getnnz(axis=0))
            # Find columns with non-zero counts equal to the number of rows
            core_genes_indices = np.where(column_nonzeros == num_rows)[0]

            core_genes_csc_matrix = ref_csc_matrix[:, core_genes_indices]

            core_genes_columns = [selected_columns[i] for i in core_genes_indices]

            # Regardless of 100% refs or not, still need to calculate sums and averages for confidence intervals
            core_ref_genes_sums = core_genes_csc_matrix.sum(axis=1)
            core_ref_genes_avg = core_ref_genes_sums.sum() / len(core_ref_genes_sums)

            # Get lengths and avg of ref sequences
            ref_gene_sums = ref_csc_matrix.sum(axis=1)
            ref_genes_avg = ref_gene_sums.sum() / len(ref_gene_sums)

            # Get confidence interval - we need to estimate (as even refs have variation) the relative completeness
            confidence_interval = stats.t.interval(0.95, len(ref_gene_sums.A1) - 1, loc=ref_genes_avg,
                                                   scale=stats.sem(ref_gene_sums.A1, ddof=1))

            for i in range(filtered_target_csc_matrix.shape[0]):
                row = filtered_target_csc_matrix.getrow(i)

                # Core genes present
                row_core_genes = row[:, core_genes_indices]
                core_gene_cov = np.sum(row_core_genes) / len(core_genes_indices) * 100

                # Get all genes
                row_genes = np.sum(row)

                member = selected_indices[i]

                # Probably should have this earlier, or removed after testing
                if member in previous_seen:
                    logger.error(f'{member} already seen!')
                previous_seen.update([member])

                status = 'Ref' if member in ref_members else 'Co-Ref'

                coverage[member] = {
                    'Status': status,
                    'Group': target,
                    'Core Genes': len(core_genes_indices),
                    'Core Gene Coverage': core_gene_cov,  # "Should" only be a single value?
                    'Genes (calc)': row_genes,
                    'Genes (ref)': target_df.loc[target_df['RefSeqID'] == selected_indices[i], 'Proteins'].item(),
                    'PC Coverage (95% CI low)': round(row_genes / confidence_interval[0], 3) * 100,
                    'PC Coverage (95% CI high)': round(row_genes / confidence_interval[1], 3) * 100
                }

        else:

            # Since this is unknown, need to estimate the maximum length... despite realizing it's completely unverified
            target_genes_sum = filtered_target_csc_matrix.sum(axis=1)
            target_genes_avg = target_genes_sum.sum() / len(target_genes_sum)

            confidence_interval = stats.t.interval(0.95, len(target_genes_sum.A1) - 1, loc=target_genes_avg,
                                                   scale=stats.sem(target_genes_sum.A1, ddof=1))
            # Heuristic approach
            observed_max = target_genes_sum.max()
            buffered_range = (observed_max * 0.95, observed_max * 1.05)

            # Low should be at most 5% less than the max, representing the fact we know that the upper limit is at
            # LEAST that much - take highest of the two
            lower_estimate = max(confidence_interval[0], buffered_range[0])

            # Upper *could* be 5% higher, but in this case, we have at least 5 genomes to try and draw a maximum size
            # from. Do the existing genomes suggest an even higher limit?
            upper_estimate = max(confidence_interval[1], buffered_range[1])

            # For "core" genes, at least try to find genes present across the entire genus
            num_rows = filtered_target_csc_matrix.shape[0]
            column_nonzeros = np.array(filtered_target_csc_matrix.getnnz(axis=0))
            core_genes_indices = np.where(column_nonzeros == num_rows)[0]

            # core_genes_csc_matrix = filtered_target_csc_matrix[:, core_genes_indices]
            # core_genes_columns = [selected_columns[i] for i in core_genes_indices]
            # Don't need core genes sum or average, as we're not calculating CI, and therefore no core_genes_csc_matrix
            # TODO columns could be used to export core genes for user

            for i in range(filtered_target_csc_matrix.shape[0]):
                row = filtered_target_csc_matrix.getrow(i)

                row_genes = np.sum(row)

                # Core genes present
                row_core_genes = row[:, core_genes_indices]
                core_gene_cov = np.sum(row_core_genes) / len(core_genes_indices) * 100

                member = selected_indices[i]
                coverage[member] = {
                    'Status': 'Novel',
                    'Group': target,
                    'Core Genes': len(core_genes_indices),
                    'Core Gene Coverage': core_gene_cov,  # "Should" only be a single value?
                    'Genes (calc)': row_genes,
                    'Genes (ref)': target_df.loc[target_df['RefSeqID'] == selected_indices[i], 'Proteins'].item(),
                    'PC Coverage (95% CI low)': round(row_genes / lower_estimate, 3) * 100,
                    'PC Coverage (95% CI high)': round(row_genes / upper_estimate, 3) * 100
                }

    coverage_df = pd.DataFrame.from_dict(coverage, orient='index')

    coverage_df.to_csv(output_dir / 'completeness.csv', index=True)


def export_profile(crosstab_df, assignments_df, output_dir, target_ranks, min_members):

    if not target_ranks:
        return

    logger.info(f'Exporting profiles for {target_ranks}...')

    for target_rank in target_ranks:

        for target_r, target_df in tqdm(assignments_df.groupby(f'{target_rank} (prediction)'),
                                        total=len(assignments_df[f'{target_rank} (prediction)'].unique()),
                                        desc=target_rank):

            fname = f'{target_rank}_{target_r.replace("|", "-")}'

            if len(target_df) >= min_members:

                target_members = target_df['RefSeqID'].tolist()

                target_crosstab_df = crosstab_df.loc[target_members].copy()
                target_crosstab_df = target_crosstab_df.loc[:, (target_crosstab_df != 0).any(axis=0)]  # Drop no PCs

                target_crosstab_df = target_crosstab_df.astype(bool)  # "Heatmap", only need yes or no
                target_crosstab_df = target_crosstab_df.astype(int)

                # Drop singletons
                singletons = target_crosstab_df.sum(axis=0) <= 1
                target_crosstab_df = target_crosstab_df.loc[:, ~singletons]

                # Write CSV
                target_crosstab_df.to_csv(output_dir / f'{fname}.csv', index=True)

                # Add
                target_index = all_ranks.index(target_rank)
                targeted_colors = []

                for target_idx in all_ranks[target_index:]:
                    targeted_rank_df = target_df[['RefSeqID', f'{target_idx} (prediction)']]
                    targeted_rank = targeted_rank_df[f'{target_idx} (prediction)'].tolist()

                    lut = dict(zip(set(targeted_rank), sns.hls_palette(len(set(targeted_rank)), l=0.5, s=0.8)))
                    targeted_rank_df[target_idx] = targeted_rank_df[f'{target_idx} (prediction)'].map(lut)

                    targeted_color = dict(zip(targeted_rank_df['RefSeqID'], targeted_rank_df[target_idx]))

                    targeted_color = pd.Series(targeted_color)
                    targeted_color = targeted_color.rename(target_idx)
                    targeted_colors.append(targeted_color)

                targeted_colors_df = pd.concat(targeted_colors, axis=1)

                g = sns.clustermap(data=target_crosstab_df, row_cluster=True, col_cluster=True,
                                   cmap='coolwarm', method='average', row_colors=targeted_colors_df,
                                   )

                g.cax.set_visible(False)  # It's 0 and 1

                if target_crosstab_df.shape[1] > 50:  # TODO Will need to figure out
                    g.ax_heatmap.set_xticks([])
                    g.ax_heatmap.set_xticklabels([])

                num_rows = target_crosstab_df.shape[0]
                if num_rows <= 10:
                    font_size = 8
                elif num_rows <= 50:
                    font_size = 6
                elif num_rows <= 100:
                    font_size = 4
                else:
                    font_size = 2

                for label in g.ax_heatmap.get_yticklabels():
                    label.set_fontsize(font_size)

                g.ax_heatmap.set_yticks(np.arange(num_rows) + 0.5)
                g.ax_heatmap.set_yticklabels(target_crosstab_df.index, rotation=0)

                # Write SVG
                plt.savefig(output_dir / f'{fname}.svg', format='svg', bbox_inches='tight')


def export_unified_tree():
    pass
