"""
Object containing the profiles of the genomes and computation of the
similarity network of genomes. Designed to handle PC and HMM-based profiles
"""

# TODO Add in tree-based incorporation of genomes that are already close to references, e.g. they share 90% of PCs

import logging
import shutil
from pathlib import Path
import pickle
import gzip
import warnings
import h5py

import numpy as np
import pandas as pd
from tqdm import tqdm

from .config import NUM_OF_DIGITS_PC_ID
from .utilities import execute_stdout

from scipy.stats.contingency import crosstab as scipy_crosstab

warnings.filterwarnings("ignore")

logger = logging.getLogger(__name__)

pd.set_option("display.max_rows", 100)
pd.set_option("display.max_columns", 10)
pd.set_option("display.width", 1000)

tqdm.pandas()


class Profiles(object):
    """

    Presence/absence matrix/profile. Used to build similarity network and subsequent agglomerative clusters
    Builds JUST THE PROFILES

    Attributes:  # TODO Update with relevant ones

        method: how profile is constructed
        ref_db: Dict of reference database, contains keys corresponding to
            minimum info
        name: Name of the profile object, usually kind + 'profile'  # TODO Consider removing
        method: With what tool the initial clustering will be done by  # TODO Consider removing
        proteins: File path to proteins file in FASTA format
        threads: How many processors to use
        evalue: Threshold to filter method
        genomes_overview: pd.DataFrame(), genome_id key, with profile-only information (i.e. generated from the profile)
        merged_faa: Path to concatenated FASTA-formatted reference + user proteins
        all_genomes: set() of all genomes, including both user and reference, generated from genomes_overview

        ref_genomes: Path to reference Genomes Report feather dataframe

        ref_g2g: Path to reference gene-to-genome feather dataframe --> protein_id, genome_id
        ref_c2g: Path to reference cluster-to-genome feather dataframe  --> protein_id, genome_id, Representative, Member, cluster_id
        g2g: pd.DataFrame() with "raw" ref+user g2g concatenated
        updated_g2g: pd.DataFrame() with ref+user g2g updated with reference clusters and extended to user sequences
        filtered_g2g: pd.DataFrame() built from updated_g2g, that removes genomes < 2 clusters
        merged_g2g: pd.DataFrame() built from filtered_g2g, with only "genome_id" and "cluster_id"
        filtered: dict with genomes filtered out (due to some threshold),
            with subdicts genome contains 1 or fewer cluster_ids MMSeqs2
            cluster in fewer than 1 genome

        out_dir: Path to output directory
        ntw_fp: Path to genome network
        ntw_mod_fp: Path to module network
        ntw_nodes: Nodes in genome network
        ntw_mod_nodes: Nodes in module network

    """

    def __init__(
            self,
            user_genes: Path,  # "usr" saves 1 letter, but takes 1 more second to translate
            user_g2g: Path,  # protein_id, genome_id, keywords
            user_len: Path,  # genome_id, length (in Kb)
            ref_db: dict,
            ref_fp: Path,
            ref_domain: str,
            identities: list,
            output_dir: Path,
            threads: int,
            force=False,
            keep_temp: bool = False,
            mmseqs_fp=None,
            verbose: bool = True,
    ):
        """
        Initialize Profiles object with the following attributes
        :param user_genes: File path to user genes file in FASTA format
        :param user_g2g: File path to user gene-to-genome file format
        :param ref_db: Reference DB info dict w/ 'genomes', 'proteins',
            'hmm' keys and (optionally) 'vog'
        :param ref_version: Version of reference DB
        :param ref_domain: Domain of reference DB
        :param identities: List of identities to use
        :param output_dir: Output directory
        :param threads: How many processors to use
        :param force: Overwrite output files
        :param mmseqs_fp: Path to mmseqs2 binary
        """

        # Set settings
        self.identities = identities
        self.user_aa_fp = user_genes
        self.user_g2g_fp = user_g2g
        self.threads = threads
        self.mmseqs_fp = str(mmseqs_fp)
        verbose = not verbose  # if isatty = False = noninteractive --> negate --> tqdm disable=True

        # Bring in user lengths
        self.user_len = user_len
        self.user_len_df = pd.read_parquet(self.user_len).set_index('genome_id', drop=True)

        # TODO How many of these do we need to package?

        # Each rank of the taxonomy will have its own optimal values, and therefore will require all of them to be
        # calculated...

        # References - these are the minimums required to process
        ref_version = ref_fp.stem
        ref_dir = ref_fp.parent
        self.ref_db = ref_db[ref_version]["Domains"][ref_domain]  # Just a ref to the DB before identities
        self.ref_aa_fp = ref_dir / self.ref_db["proteins"]  # Path
        self.ref_g2g_fp = ref_dir / self.ref_db["gene2genome"]  # Path
        self.ref_genomes_fp = ref_dir / self.ref_db["Genomes Report"]
        # All of the above are non-identity variables

        self.force = force
        self.out_dir = output_dir

        user_g2g_df = pd.read_parquet(self.user_g2g_fp)
        ref_g2g_df = pd.read_parquet(self.ref_g2g_fp)

        self.ref_genomes = ref_g2g_df.genome_id.unique().tolist()
        self.user_genomes = user_g2g_df.genome_id.unique().tolist()

        logger.info(f'Unique genes identified in user data:\t{len(user_g2g_df.protein_id.unique().tolist())}')
        logger.info(f'Unique genomes identified in user data:\t{len(self.user_genomes)}')

        user_g2g_df = user_g2g_df.replace("None", np.nan)  # If annotated with None, don't want counted as annotated

        ref_overview = ref_g2g_df.groupby(
            "genome_id"
        ).nunique()  # nunique converts genome_id into the number of unique
        ref_overview["genome_id"] = ref_overview.index.tolist()
        ref_overview = ref_overview.rename(
            columns={"protein_id": "Proteins"}
        )
        ref_overview["Reference"] = True  # ref_overview now = (genome_id), Proteins, genome_id, Reference

        self.ref_genomes_df = pd.read_parquet(ref_dir / self.ref_db["Genomes Report"])
        ref_overview = ref_overview.merge(self.ref_genomes_df[['Size (Kb)', 'RefSeqID']].set_index('RefSeqID', drop=True),
                                          how="left", left_index=True, right_index=True)

        # Get equivalent for user
        user_overview = user_g2g_df.groupby("genome_id").nunique()
        user_overview["genome_id"] = user_overview.index.tolist()
        # protein_id -> Proteins; keywords -> Annotated
        user_overview.columns = ["Proteins", "Annotated", 'genome_id']
        user_overview["Reference"] = False

        # Merge user lengths with genome overviews
        user_overview = user_overview.merge(self.user_len_df, how="left", left_index=True, right_index=True)

        # Serves as foundation for collecting all genome analyses
        self.genomes_overview = pd.concat([ref_overview, user_overview], sort=True)
        # (genome_id), keywords, Proteins, Reference, genome_id
        self.all_genomes = self.genomes_overview.index.tolist()

        # Create merged gene-to-genome
        self.g2g_df = pd.concat([user_g2g_df, ref_g2g_df])  # protein_id, genome_id, keywords

        logger.info(f'After merging user + reference data:\n'
                    f'\t{len(self.g2g_df.protein_id.unique().tolist())} unique genes'
                    f'\t{len(self.g2g_df.genome_id.unique().tolist())} unique genomes')

        # Up to this point, everything can be handled OUTSIDE of identities
        # As soon as the clustering is performed, all its results need to be handled within the identities

        self.identities = {}
        self.identities["name"] = f"HMMprofile"

        # TODO Incorporate skipping identities already completed

        for identity in identities:
            identity = str(identity)
            # RefDB has "db", "cluster", "cluster tsv" and "cluster2genome" and "unique genes" keys
            logger.info(f'Processing identity: {identity}')
            ref_db_id = self.ref_db["identities"][identity]
            ref_c2g_df = pd.read_parquet(ref_dir / ref_db_id["cluster2genome"])

            # Outputs
            if identity not in self.identities:
                self.identities[identity] = {}

            self.identities[identity]["name"] = f"HMMprofile.{identity}"

            # Create merged clusters
            logger.info('Merging and updating PCs associated with this identity')
            merged_db, merged_clu = self.run_mmseqs2(identity, ref_dir)

            # Updated clusters = "Representative" "Member"
            # DEPENDING on MMSeqs2 to ensure Representatives are managed
            merged_clu_df = self.parse_mmseqs2(merged_db, merged_clu)  # Representative, Member

            # TODO Want to add more user feedback to this series of merge, repair, build

            # Merge reference data with new clustering
            self.identities[identity]["updated_c2g"] = merged_clu_df.merge(
                ref_c2g_df, how='left', on=['Member', 'Representative']
            )

            updated_c2g_df = self.identities[identity]["updated_c2g"]  # Shorthand

            # Since user sequences have never been clustered, this merge "updates" the former gene-to-genome into a
            # cluster-to-genome
            updated_c2g_df = updated_c2g_df.merge(
                user_g2g_df, how='left', left_on='Member', right_on='protein_id',
                suffixes=['_l', '_r'])
            # Merging user g2g onto merged c2g allows updating ref clusters
            # The user "component" will have NaN in protein_id_l, genome_id_l and cluster_id
            # User genomes (genome_id) will only be found in merged genome_id_r

            # Repair the merge
            updated_c2g_df['protein_id'] = updated_c2g_df['protein_id_l'].combine_first(updated_c2g_df['protein_id_r'])
            updated_c2g_df['genome_id'] = updated_c2g_df['genome_id_l'].combine_first(updated_c2g_df['genome_id_r'])

            updated_c2g_df.drop(
                columns=[col for col in updated_c2g_df.columns if "_id_" in col],
                inplace=True,
            )

            # Find the maximum reference cluster ID
            id_max = updated_c2g_df["cluster_id"].dropna().apply(lambda x: int(x.replace("CLU", ""))).max()

            # Identify members without a cluster_id but have a representative whose members do include a cluster_id
            # In other words, fill in clusters who have new members as a result of user sequences
            logger.info(f'Rectifying new cluster IDs during merger of reference database and user sequences')

            clusters_added = 0
            clusters_modified = 0

            # Prepare a numpy array to store cluster updates
            cluster_updates = np.empty(len(updated_c2g_df), dtype=object)

            # Group by Representative and process each group
            for rep, rep_df in tqdm(updated_c2g_df.groupby(by='Representative'), disable=verbose):
                is_null = rep_df['cluster_id'].isnull()
                is_not_null = ~is_null

                if is_null.sum() == 0:
                    continue
                elif is_null.all():  # New cluster without cluster_id
                    id_max += 1
                    cluster = f"CLU{str(id_max).zfill(NUM_OF_DIGITS_PC_ID)}"
                    cluster_updates[rep_df.index] = cluster
                    clusters_added += 1
                elif is_not_null.any():  # There are references
                    cluster_id = rep_df.loc[is_not_null, 'cluster_id'].iloc[0]  # Get the first non-null cluster_id
                    cluster_updates[rep_df.index] = cluster_id
                    clusters_modified += 1
                else:
                    logger.error(f'Issue with {rep}. Merging strategy not a new cluster with no representative NOR an '
                                 f'extended [pre-existing] cluster NOR no new members.')

            # Apply updates in one go
            updated_c2g_df['cluster_id'] = np.where(cluster_updates == None, updated_c2g_df['cluster_id'],
                                                    cluster_updates)

            logger.info(f'There were {clusters_added} clusters added, {clusters_modified} clusters extended from merge')

            # Update genomes_view with Cluster counts
            sel_cols = ["cluster_id", "genome_id"]
            mmseqs_clu_counts = updated_c2g_df[sel_cols].groupby(
                "genome_id").count()  # (genome_id), cluster_id = #prots

            self.identities[identity]["updated_c2g"] = updated_c2g_df

            # Build identity-specific genome overview
            # Adds in "extra" genome_id (same as index)
            merged_overview_df = self.genomes_overview.merge(
                mmseqs_clu_counts,
                left_index=True,
                right_index=True,
                suffixes=["RefOnly", "WithUser"],
            ).rename(columns={'cluster_id': 'ClustersRefOnly'})

            # NOTE
            # cluster_idRefOnly -> ClustersRefOnly
            # cluster_idWithUser -> ClustersWithUser

            # new columns:
            # Annotated, Member, Proteins, Reference, Representative,
            # ClustersRefOnly, ClustersWithUser

            # Fixed a bug: change to use ClustersWithUser instead of
            # cluster_id (rename to ClustersRefOnly)

            # No longer removing singletons. If no shared genes, no relationship. Drops out?

            ref_shared_with_user = set()
            user_shared_with_ref = set()
            set_ref_genomes = set(self.ref_genomes)
            set_user_genomes = set(self.user_genomes)

            # Iterate over clusters and find intersections
            for cluster_id, cluster_df in tqdm(updated_c2g_df.groupby('cluster_id'),
                                               total=updated_c2g_df['cluster_id'].nunique(), disable=verbose):
                cluster_df_ids = set(cluster_df['genome_id'].unique())

                # Find intersections
                refs_contained = cluster_df_ids & set_ref_genomes
                user_contained = cluster_df_ids & set_user_genomes

                # Update the sets
                if refs_contained and user_contained:
                    ref_shared_with_user.update(refs_contained)
                    user_shared_with_ref.update(user_contained)

            # Create shares_clusters column and update values
            merged_overview_df['shares_clusters'] = np.nan
            merged_overview_df.loc[list(ref_shared_with_user), 'shares_clusters'] = 'withUser'
            merged_overview_df.loc[list(user_shared_with_ref), 'shares_clusters'] = 'withRef'

            # Fill NaN values based on the Reference column
            merged_overview_df.loc[merged_overview_df['Reference'] == True, 'shares_clusters'] = merged_overview_df[
                'shares_clusters'].fillna('ref only')
            merged_overview_df.loc[merged_overview_df['Reference'] == False, 'shares_clusters'] = merged_overview_df[
                'shares_clusters'].fillna('user only')

            self.identities[identity]["genomes_overview"] = merged_overview_df

            logger.info(f"Building profiles for {identity}...")
            self.identities[identity]["crosstab_fp"] = self.build_crosstab(identity)

            # What's in identity
            # crosstab_fp: crosstab at the identity level
            # genomes_overview: a dataframe
            # updated_c2g: a dataframe

            if not keep_temp:
                logger.info(f"Cleaning up temporary MMSeqs2 files for {identity}...")
                for to_rm in self.out_dir.glob('*mmseq*'):
                    if to_rm.is_dir():
                        shutil.rmtree(to_rm)
                    else:
                        to_rm.unlink()

    def __repr__(self):
        mes = (
            "Profile object consisting of the following:\n"
            "{identities} clustering identities\n"
            "{num_genomes} Genomes\n{num_matrix_genomes} Matrix Genomes\n{num_matrix_HMMs} Matrix Clusters"
        )
        return mes.format(
            identities=len(self.identities),
            num_genomes=len(self.overviews[self.identities[0]]['genomes_overview'].cluster_id.unique()),
            num_matrix_genomes=len(self.overviews[self.identities[0]]['updated_c2g'].genome_id.unique()),
            # num_matrix_HMMs=len(self.overviews[self.identities[0]['genomes_overview'].cluster_id.unique())
            num_matrix_HMMs='placeholder',
        )

    def run_mmseqs2(self, ident: float, ref_loc: Path):
        """
        Merge reference and user sequences. Run MMSeqs2 createdb + clusterupdate
        :return:
        """
        logger.debug("Running MMSeqs2 createdb + clusterupdate")

        merged_basename = f"{self.identities[ident]['name']}.merged"
        merged_faa = self.out_dir / f"{merged_basename}.faa"

        if not merged_faa.exists():

            with open(merged_faa, 'wb') as merged_faa_fh:
                for f in [ref_loc / self.ref_db["proteins"], self.user_aa_fp]:  # This is prior to identity
                    with open(f, 'rb') as fh:
                        shutil.copyfileobj(fh, merged_faa_fh)

        self.identities[ident]["merged_faa"] = merged_faa

        merged_db = self.out_dir / f"{merged_basename}.mmseq"
        if not merged_db.exists():
            createdb_cmd = [self.mmseqs_fp, "createdb", str(merged_faa), str(merged_db)]
            res = execute_stdout(createdb_cmd)

        updated_db = self.out_dir / f"{merged_basename}.updated.mmseq"
        updated_clu = self.out_dir / f"{merged_basename}.updated.mmseq_clu"
        updated_tmp = self.out_dir / f"{merged_basename}.updated.mmseq_clu_tmp"

        # mmseqs clusterupdate oldDB newDB cluDB_old newDB_updated cluDB_updated tmp
        # mmseqs clusterupdate DB_trimmed DB_new DB_trimmed_clu DB_new_updated DB_update_clu tmp
        if not all(x.exists() for x in [updated_db, updated_clu, updated_tmp]):
            clusterupdate_cmd = [
                self.mmseqs_fp,
                "clusterupdate",
                str(ref_loc / self.ref_db['identities'][ident]["db"]),  # oldDB/DB_trimmed
                str(merged_db),  # newDB/DB_new
                str(ref_loc / self.ref_db['identities'][ident]["cluster"]),  # cluDB_old/DB_trimmed_clu
                str(updated_db),  # newDB_updated/DB_new_updated
                str(updated_clu),  # cluDB_updated/DB_update_clu
                str(updated_tmp),  # tmp
                "--min-seq-id",
                str(ident)
                # TODO Be explicit with --threads INT
            ]

            # There are no longer cluster params, removing access from user to streamline
            res = execute_stdout(clusterupdate_cmd)

            if res.returncode != 0:
                logger.error('MMSeqs2 failed to update database clusterings with user sequences')

        return updated_db, updated_clu

    def parse_mmseqs2(self, db: Path, clu: Path):

        """
        :param
        :returns cluster_df with Representative, Member"""

        # mmseqs createtsv new_DB new_DB new_DB_clu new_DB_clu_X.tsv
        tsv = self.out_dir / f"{clu.name}.tsv"
        logger.debug(f'Saving cluster db (i.e. cluster-to-genome) to {str(tsv)}...')
        createtsv_cmd = [self.mmseqs_fp, "createtsv", str(db), str(db), str(clu), str(tsv)]

        res = execute_stdout(createtsv_cmd)

        if res.returncode != 0:
            logger.error(
                "There was an issue with creating TSV from the"
                " Reference + User clusters"
            )

        logger.debug(f'Saved cluster db (i.e. cluster-to-genome)')

        cluster_df = pd.read_csv(
            tsv,
            delimiter="\t",
            header=None,
            names=["Representative", "Member"],
            index_col=None,
        )

        return cluster_df

    def build_crosstab(self, ident: float):
        """
        Create matrix of genome X PC/HMM
        :param ident: Identity of the clustering
        :return:
        """
        logger.info("Building matrix of genome(s) X PC/HMMs")

        # TODO https://ramiro.org/notebook/pandas-crosstab-groupby-pivot/ --> use groupby or pivot?
        # Alternatively, from scipy.stats.contingency import crosstab

        # WARNING It is ~10-20X faster to build the csr matrix from a coo
        #   matrix that is built directly (i.e. just
        # assigned), but this is 1 line and more easily digestible
        scipy_res = scipy_crosstab(
            self.identities[ident]["updated_c2g"].genome_id,
            self.identities[ident]["updated_c2g"].cluster_id,
            sparse=True  # The only other alternative is to cast dtype for the function as int8 or int16
        )  # Ln 195: count = coo_matrix((np.ones(len(indices[0]), dtype='uint16'),
        # OR Ln 200: count = np.zeros(shape, dtype='uint16')
        # THIS result WILL BE int64, so will need to downcast it before downstream to avoid memory issues

        # TODO genomes_overview has NaN for reference Annotated, users have 1.0 for "None"
        # Build dataframe from scipy contingency matrix
        rows, cols = scipy_res.elements  # NOTE: MUST be 1.10.x
        arr = scipy_res.count
        arr = arr.astype('uint16')  # Prevent memory ballooning when underlying numpy inherits dtype (@ int64!)

        # Imposing a HARD LIMIT on the number of genes vConTACT will accept: 65535

        # Theoretically, the maximum number of genes that can be shared is to oneself
        # Need to be careful casting dtypes to lower values
        diag = arr.max()
        # diag = crosstab.values.sum(axis=1)
        if diag <= 255:
            limit = 'uint8'  # 1
        elif diag <= 65535:
            limit = 'uint16'  # 2
        else:
            logger.error('vConTACT3 is not suitable for processing genomes with more than 65535 genes.')
            exit(1)

        # These arrays are MBs for 100Ks
        arr.data[arr.data > 1] = 1
        arr = arr.astype(limit)  # Resolver will inherit dtype, so check here NOW so can avoid as_array (i.e. todense)

        # Since this is a sparse matrix, it takes almost no memory (6 MB), then going astype --> 1284 MB!
        # crosstab = pd.DataFrame.sparse.from_spmatrix(arr, index=rows, columns=cols)  # Inherits dtype
        # crosstab = pd.DataFrame(arr.toarray(), index=rows, columns=cols)  # Inherits dtype
        # crosstab = pd.DataFrame.sparse.from_spmatrix(arr, index=rows, columns=cols)  # Inherits dtype
        # crosstab = crosstab.to_dense()
        # 5137013736 --> 642164356 (int64 --> uint16)
        # int8 = 128to127
        # uint8 = 0to255
        # If more than 255 PCs are found in a single genome, this won't be good
        # No longer an issue due to added block, above

        crosstab_fp = self.out_dir / f"{self.identities[ident]['name']}_crosstab.h5"

        with h5py.File(crosstab_fp, 'w') as f:  # shared = CSR, distance = COO
            # Save sparse matrix components
            f.create_dataset('crosstab_data', data=arr.data, compression='gzip')
            f.create_dataset('crosstab_row', data=arr.row, compression='gzip')
            f.create_dataset('crosstab_col', data=arr.col, compression='gzip')
            f.create_dataset('crosstab_shape', data=arr.shape, compression='gzip')

            # Save row and column labels + Convert strings to byte strings
            f.create_dataset('crosstab_row_labels', data=rows.astype('S'), compression='gzip')
            f.create_dataset('crosstab_col_labels', data=cols.astype('S'), compression='gzip')

        # We are no longer saving the crosstab within the profile - it's simply too large for multiple identities

        return crosstab_fp
