import os

# DB_PATH = os.environ['VCONTACT3_DATA_PATH']
#
# #
# # Ref database paths
# #
# TAX_DIR = os.path.join(DB_PATH, "taxonomy/")
# TAXONOMY_FILE = os.path.join(TAX_DIR, "ref_taxonomy.tsv")
# REF_GENOME_DIR = os.path.join(DB_PATH, "genomes/")
# REF_GENOME_MSH = os.path.join(DB_PATH, 'genomes.msh')

# Minhash config
SPECIES_MSH_DIRNAME = 'minhash'
QUERY_EACH_GENOME_AS_FILE = False

# Fastani config
# FASTANI_GENOMES = REF_GENOME_DIR
FASTANI_GENOMES_EXT = "_genomic.fna.gz"
ANI_SPECIES_THRESHOLD = 0.95
# Information about alignment Fraction to resolve fastANI results
AF_THRESHOLD = 0.8
ANI_SUMMARY_COLS =['user_genome', 'reference_genome', 'ani',
                   'af', 'reference_taxonomy']
ANI_CLOSEST_COLS =['user_genome', 'reference_genome', 'ani',
                   'af', 'reference_taxonomy',
                   'satisfies_circumscription_criteria']

# MISC
NUM_OF_DIGITS_PC_ID = 7  # max PC/HMM index 9999999


# Profiles
ADJUSTED_PC_COL = 'Adjusted VOG'
MAX_SIG = 1000  # MAX value for VC network edge weight
# minimal shared PC, building PC similarity network is a bottleneck step
#   removing those only in very few genomes can help speed up.
MOD_SHARED_MIN = 3

# Colors
# https://graphicdesign.stackexchange.com/questions/3682/where-can-i-find-a-large-palette-set-of-contrasting-colors-for-coloring-many-d
colors = ["#023fa5", "#7d87b9", "#bec1d4", "#d6bcc0", "#bb7784", "#8e063b", "#4a6fe3", "#8595e1",
          "#b5bbe3", "#e6afb9", "#e07b91", "#d33f6a", "#11c638", "#8dd593", "#c6dec7", "#ead3c6",
          "#f0b98d", "#ef9708", "#0fcfc0", "#9cded6", "#d5eae7", "#f3e1eb", "#f6c4e1", "#f79cd4"]
extended_colors = ["#B2B2B2", "#FFFF00", "#1CE6FF", "#FF34FF", "#FF4A46", "#008941", "#006FA6", "#A30059",
                   "#FFDBE5", "#7A4900", "#0000A6", "#63FFAC", "#B79762", "#004D43", "#8FB0FF", "#997D87",
                   "#5A0007", "#809693", "#FEFFE6", "#1B4400", "#4FC601", "#3B5DFF", "#4A3B53", "#FF2F80",
                   "#61615A", "#BA0900", "#6B7900", "#00C2A0", "#FFAA92", "#FF90C9", "#B903AA", "#D16100",
                   "#DDEFFF", "#000035", "#7B4F4B", "#A1C299", "#300018", "#0AA6D8", "#013349", "#00846F",
                   "#372101", "#FFB500", "#C2FFED", "#A079BF", "#CC0744", "#C0B9B2", "#C2FF99", "#001E09",
                   "#00489C", "#6F0062", "#0CBD66", "#EEC3FF", "#456D75", "#B77B68", "#7A87A1", "#788D66",
                   "#885578", "#FAD09F", "#FF8A9A", "#D157A0", "#BEC459", "#456648", "#0086ED", "#886F4C",
                   "#34362D", "#B4A8BD", "#00A6AA", "#452C2C", "#636375", "#A3C8C9", "#FF913F", "#938A81",
                   "#575329", "#00FECF", "#B05B6F", "#8CD0FF", "#3B9700", "#04F757", "#C8A1A1", "#1E6E00",
                   "#7900D7", "#A77500", "#6367A9", "#A05837", "#6B002C", "#772600", "#D790FF", "#9B9700",
                   "#549E79", "#FFF69F", "#201625", "#72418F", "#BC23FF", "#99ADC0", "#3A2465", "#922329",
                   "#5B4534", "#FDE8DC", "#404E55", "#0089A3", "#CB7E98", "#A4E804", "#324E72", "#6A3A4C"]
color_mapper = {
    'Genus': '#4277b6',  # Blue
    'Subfamily': '#d33f6a',  # Lipstick pink
    'Family': '#5fa641',  # Green
    'Order': '#463397',  # Violet
    'Class': '#91218c',  # Purplish-Red
    'Phylum': '#df8461',  # Yellowish-Pink
    'Kingdom': '#2b3514',  # Olive Green
    'Realm': '#ba1c30',  # Red
    'Beyond': '#000000',  # Black
}

hue_mapper = {
    'Genus': {
        1.0: '#4277b6',
        0.95: '#5484bd',
        0.90: '#6792c4',
        0.80: '#7a9fcb',
        0.70: '#8dadd3',
        0.60: '#a0bbda',
        0.50: '#b3c8e1',
        0.40: '#c6d6e9',
        0.30: '#d9e3f0',
    },
    'Subfamily': {
        1.0: '#d33f6a',
        0.95: '#d75278',
        0.90: '#db6587',
        0.80: '#e07896',
        0.70: '#e48ba5',
        0.60: '#e99fb4',
        0.50: '#edb2c3',
        0.40: '#f1c5d2',
        0.30: '#f6d8e1',
    },
    'Family': {
        1.0: '#5fa641',
        0.95: '#6fae54',
        0.90: '#7eb766',
        0.80: '#8fc07a',
        0.70: '#9fc98d',
        0.60: '#afd2a0',
        0.50: '#bfdbb3',
        0.40: '#cfe4c6',
        0.30: '#dfedd9',
    },
    'Order': {
        1.0: '#463397',
        0.95: '#5847a1',
        0.90: '#6a5bab',
        0.80: '#7d70b6',
        0.70: '#9084c0',
        0.60: '#a299cb',
        0.50: '#b5add5',
        0.40: '#c7c1df',
        0.30: '#dad6ea',
    },
    'Class': {
        1.0: '#91218c',
        0.95: '#9c3797',
        0.90: '#a74da3',
        0.80: '#b263ae',
        0.70: '#bd79ba',
        0.60: '#c890c5',
        0.50: '#d3a6d1',
        0.40: '#debcdc',
        0.30: '#e9d2e8',
    },
    'Phylum': {
        1.0: '#df8461',
        0.95: '#e29070',
        0.90: '#e59c80',
        0.80: '#e8a890',
        0.70: '#ebb5a0',
        0.60: '#efc1b0',
        0.50: '#f2cdbf',
        0.40: '#f5dacf',
        0.30: '#f8e6df',
    },
    'Kingdom': {
        1.0: '#2b3514',
        0.95: '#40492b',
        0.90: '#555d42',
        0.80: '#6a715a',
        0.70: '#7f8572',
        0.60: '#959a89',
        0.50: '#aaaea1',
        0.40: '#bfc2b8',
        0.30: '#d4d6d0',
    },
    'Realm': {
        1.0: '#ba1c30',
        0.95: '#c03244',
        0.90: '#c74959',
        0.80: '#ce606e',
        0.70: '#d57682',
        0.60: '#dc8d97',
        0.50: '#e3a4ac',
        0.40: '#eabac0',
        0.30: '#f1d1d5',
    }
}

realm_mapper = {
    'Duplodnaviria': '#000000',  # gray
    'Adnaviria': '#000075',  # navy, archaeal filamentous
    'Monodnaviria': '#f032e6',  # magenta, all ssDNA w/ RCR, includes linear/circular, and some dsDNA
    'Riboviria': '#e6194B',  # red
    'Varidnaviria': '#4363d8',  # blue, non-tailed dsDNA, JRC in MCP
    'Ribozyviria': '#FFBF00'  # blue, non-tailed dsDNA, JRC in MCP
}

identities = [0.3, 0.4, 0.5, 0.6, 0.7]
ranks = ['genus', 'subfamily', 'family', 'order']
ranks = ranks[::-1]
all_ranks = ['realm', 'phylum', 'class', 'order', 'family', 'subfamily', 'genus']

realms = ['Riboviria', 'Ribozyviria', 'Monodnaviria', 'Adnaviria', 'Varidnaviria', 'Duplodnaviria']
not_implemented = set(['Riboviria', 'Ribozyviria'])  # Want to use some set logic later
implemented = set(['Adnaviria', 'Varidnaviria', 'Duplodnaviria', 'Monodnaviria'])