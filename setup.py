from setuptools import setup
from pathlib import Path

curr_dir = Path(__file__).parent
long_description = (curr_dir / "README.md").read_text()

# https://docs.python.org/3/distutils/sourcedist.html
# https://github.com/pypa/sampleproject/blob/master/setup.py
setup(
    name='vConTACT3',
    version='3.0.0.b82',
    description='Viral CONTig Automatic Clustering and Taxonomy 3',
    long_description=long_description,
    long_description_content_type='text/markdown',
    license='GPL-3',
    url='https://bitbucket.org/MAVERICLab/vcontact3/',
    author='Benjamin Bolduc, Jiarong Guo',
    author_email='bolduc.10@osu.edu',
    keywords=['iVirus', 'virus', 'metagenomics', 'viral taxonomy'],
    # Not using find_packages for packages because we're a simple project
    packages=['vcontact3'],
    python_requires='>=3.9,<3.11',
    # https://packaging.python.org/discussions/install-requires-vs-requirements/
    install_requires=[
        'numpy>=1.23.5',
        'pandas>=2.1.1',
        'scikit-learn==1.5.0',
        'seaborn>=0.12.1',
        'matplotlib>=3.7.1',
        'setuptools==70.0.0',
        'scipy>=1.10.1',
        'jenkspy>=0.3.2',
        'networkx>=3.1',
        'upsetplot>=0.7.0',
        'tqdm==4.66.3',
        'ete3>=3.1.2',
        'jinja2==3.1.4',
        'scikit-bio>=0.5.8',
        'pyrodigal>=2.3.0',
        'pyrodigal-gv>=0.3.1',
        'joblib>=1.2.0',
        'igraph>=0.10.4',  # python-igraph for conda
        'psutil>=5.9.5',
        'biopython>=1.81',
        'swifter>=1.3.4',
        'pyarrow>=14.0.1',
        'dill>=0.3.6',
        'tables>=3.8.0',  # pytables for conda
        # hdf5 or h5py?
        'markupsafe>=2.0.1',
        'bioinfokit>=2.1.3',
        'networkit>=11.0'
    ],
    package_data={
        'vcontact3': [
            'vcontact3/templates/'
        ]
    },
    project_urls={
        'Bug Reports': 'https://bitbucket.org/MAVERICLab/vcontact3/issues',
        'Funding': '',
        'Source': 'https://bitbucket.org/MAVERICLab/vcontact3/'
    },
    classifiers=[
        "Intended Audience :: Science/Research",
        "Natural Language :: English",
        "Topic :: Scientific/Engineering :: Bio-Informatics",
        "License :: OSI Approved :: GPL3 License",
        "Programming Language :: Python :: 3",
    ],
    scripts=['bin/vcontact3']
)
