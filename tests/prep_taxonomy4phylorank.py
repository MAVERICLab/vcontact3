#!/usr/bin/env python
import sys
import os
import pandas as pd

infile = '../vcontact3/data/viral_refseq_99_report.feather'
outfile = 'taxid2lineage.tsv'

#prefix_lst = ['d', 'l', 'k', 'o', 'f', 'u', 'g', 's']
#cols = ['TaxID', 'NCBI-superkingdom', 'NCBI-clade', 'NCBI-kingdom', 'NCBI-order', 'NCBI-family', 'NCBI-subfamily', 'NCBI-genus', 'NCBI-species']

prefix_lst = ['P', 'C', 'O', 'F', 'f', 'G', 'S']
cols = ['TaxID', 'NCBI-clade', 'NCBI-kingdom', 'NCBI-order', 'NCBI-family', 'NCBI-subfamily', 'NCBI-genus', 'NCBI-species']


refseq_df = pd.read_feather(infile)
refseq_df_sel = refseq_df[cols]

with open(outfile, 'w') as fw:
    for idx, r in refseq_df_sel.iterrows():
        s = r.iloc[1:]
        s = s.fillna('Other')
        s = [i.split(':', 1)[0] for i in s]
        taxid = r.iloc[0]
        l = ['__'.join(i) for i in zip(prefix_lst, s)]
        lineage = ';'.join(l)
        fw.write(f'{taxid}\t{lineage}\n')
