df = pd.read_csv('ref_taxonomy.tsv', sep='\t', header=None)
df.columns = ['id', 'taxonid', 'domain', 'phylum', 'class', 'order', 'family', 'genus', 'species']
df = df.drop('taxonid', axis=1)

level_list = ['domain', 'phylum', 'class', 'order', 'family', 'genus', 'species']
prefix_list = ['d__', 'p__', 'c__', 'o__', 'f__', 'g__', 's__']
prefix_d = dict((level, prefix) for level, prefix in zip(level_list, prefix_list))
for lev in df.columns.drop(['id'])
    prefix = d_prefix[lev]
    df[lev] = [f'{prefix}{i}' for i in df[lev]]

lineage_list = ['; '.join(list(j)) for j in zip(*[df[i] for i in df.columns.drop('id')])]


df2 = pd.DataFrame({'id': df['id'], 'lineage': lineage_list})
df2.to_csv('taxonomy.tsv', sep='\t', index=False)
