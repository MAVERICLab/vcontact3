import sys
import os
import shutil

import glob
import json
import logging
import pickle
import gzip

import pandas as pd
import pytest

TESTDIR = os.path.dirname(__file__)
os.environ['VCONTACT3_DATA_PATH'] = f'{TESTDIR}/ref_data'

import vcontact3
from vcontact3.species import species
#from vcontact3 import imports, profiles, viral_clusters, performance

pd.set_option('display.max_rows', 1000)
pd.set_option('display.max_columns', 1000)

WKDIR = f'{TESTDIR}/wkdir'
#EXPECTED_DIR = f'{TESTDIR}/expected_output'
EXPECTED_DIR = f'{TESTDIR}/wkdir'

@pytest.fixture
def db_info():
    DB_PATH = os.path.join(TESTDIR, '../vcontact3/data')
    db_info_fp = os.path.join(DB_PATH, 'db_info.json')
    with open(db_info_fp, 'r') as db_info_fh:
        db_info = json.load(db_info_fh)
    return db_info

@pytest.fixture
def skani_example_data1():
    #seqfiles = glob.glob(f'{TESTDIR}/ref/*_genomic.fna.gz')
    seqfiles=[f'{TESTDIR}/inputs/test.fa',]
    #return {'seqfiles': seqfiles, 'no_mash': False, 'max_d': 0.2, 'out_dir': f'{WKDIR}/species.out', 'mash_k': 21, 'mash_v': 1.0, 'mash_s': 1000, 'min_af': 0.8, 'mash_db': None, 'extension': None, 'cpus': 1}
    return {'seqfiles': seqfiles, 'out_dir': f'{WKDIR}/species.skani.genome_as_seq.out', 'skip_votu': False, 'each_genome_as_file': False, 'extension': None, 'cpus': 1}

@pytest.fixture
def skani_example_data2():
    seqfiles = glob.glob(f'{TESTDIR}/inputs/one-seq-per-file/*.fa')
    return {'seqfiles': seqfiles, 'out_dir': f'{WKDIR}/species.skani.genome_as_file.out', 'skip_votu': False, 'each_genome_as_file': True, 'extension': '.fa', 'cpus': 1}

@pytest.fixture
def fastani_example_data():
    #seqfiles = glob.glob(f'{TESTDIR}/ref/*_genomic.fna.gz')
    seqfiles=[f'{TESTDIR}/inputs/test.fa',]
    return {'seqfiles': seqfiles, 'no_mash': False, 'max_d': 0.2,
            'out_dir': f'{WKDIR}/species.out', 'mash_k': 21, 'mash_v': 1.0,
            'mash_s': 1000, 'min_af': 0.8, 'mash_db': None,
            'extension': None, 'cpus': 1}

def test_fastani(fastani_example_data):
    df = species(**fastani_example_data)
    assert df.iloc[0,0] == 'seq1'
    assert df['lineage'].iloc[5] == (
            'd__Viruses;p__Other;c__Other;o__Caudovirales;'
            'f__Podoviridae;g__Friunavirus;s__Acinetobacter phage Petty'
    )

def test_species_genome_as_seq(skani_example_data1):
    if os.path.exists(skani_example_data1['out_dir']):
        shutil.rmtree(skani_example_data1['out_dir'])

    df = species(**skani_example_data1)

    assert len(set(df['votu_repr'])) == 3

    sel = df['genome'] == 'GCF_000916495.1'
    assert df.loc[sel, 'lineage'].iloc[0] == (
            'd__Viruses; p__Other; c__Other; o__Caudovirales;'
            ' f__Podoviridae; g__Friunavirus; s__Acinetobacter phage Petty'
    )

def test_species_genome_as_file(skani_example_data2):
    if os.path.exists(skani_example_data2['out_dir']):
        shutil.rmtree(skani_example_data2['out_dir'])

    skani_example_data2['skip_votu'] == False
    df = species(**skani_example_data2)

    assert len(set(df['votu_repr'])) == 3

    sel = df['genome'] == 'GCF_000916495.1'
    assert df.loc[sel, 'lineage'].iloc[0] == (
            'd__Viruses; p__Other; c__Other; o__Caudovirales;'
            ' f__Podoviridae; g__Friunavirus; s__Acinetobacter phage Petty'
    )



def test_gene_call():
    nucleotides_fp = f'{EXPECTED_DIR}/species.out/vOTU_uncl.fa'
    output_dir = f'{WKDIR}/imports.out'
    os.makedirs(output_dir, exist_ok=True)
    proteins_fp, gene2genome_fp = \
            imports.call_genes(nucleotides_fp, output_dir, force=True)

def test_profile(db_info):
    latest_version = max(db_info["VOGDB"].keys())
    db_domain= 'prokaryotes'
    print(f'Using reference database: {db_domain} (version {latest_version})')
    output_dir = os.path.join(WKDIR, 'profiles.out')
    os.makedirs(output_dir, exist_ok=True)
    profile = profiles.Profiles(
        threads=1, method='MMSeqs2',
        proteins=os.path.join(EXPECTED_DIR, 'imports.out/vOTU_uncl.faa'),
        gene2genome=os.path.join(EXPECTED_DIR,
                                 'imports.out/vOTU_uncl.g2g.csv'),
        ref_db=db_info, ref_version=latest_version,
        ref_domain=db_domain, output_dir=output_dir,
        evalue=1e-10, skip_module_ntw=True)

    profile_pkl = os.path.join(WKDIR, 'profile.pkl.gz')
    with gzip.open(profile_pkl, 'wb') as profile_gzip_fh:
        mes = 'Saving profiles to {}..'
        print(mes.format(profile_pkl))
        pickle.dump(profile, profile_gzip_fh, pickle.HIGHEST_PROTOCOL)

    print(profile)

def test_viral_clusters():
    profile_pkl = os.path.join(EXPECTED_DIR, 'profile.pkl.gz')
    with gzip.open(profile_pkl) as profile_gzip_fh:
        profile = pickle.load(profile_gzip_fh)

    cluster_one_parameters = {
        '--min-density': 0.3,
        '--min-size': 2,
        '--max-overlap': 0.9,
        '--penalty': 2.0,
        '--haircut': 0.55,
        '--merge-method': 'single',
        '--similarity': 'match',
        '--seed-method': 'nodes'
    }
    tools = {'MMSeqs2': 'mmseqs',
             'Diamond': 'diamond',
             'HMMsearch': 'hmmsearch',
             'BLASTP': 'blastall',
             'ClusterONE': os.path.join(TESTDIR, '../bin/cluster1.jar'),
             'Prodigal': 'prodigal'}
    vcs_mode = 'ClusterONE'
    output_dir = os.path.join(WKDIR, 'viral_clusters.out')
    os.makedirs(output_dir, exist_ok=True)
    vc = viral_clusters.ViralCluster(profile, output_dir=output_dir,
            tools=tools, method=vcs_mode,
            method_opts=cluster_one_parameters, force=True)

    viral_clstr_pkl = os.path.join(WKDIR, 'vc.pkl.gz')
    with gzip.open(viral_clstr_pkl, 'wb') as viral_clstr_gzip_fh:
        pickle.dump(vc, viral_clstr_gzip_fh)

    print(vc.clusters.head())
    print(vc)

def test_red_refinement(db_info):
    pass

def test_performance():
    vc_pkl = os.path.join(EXPECTED_DIR, 'vc.pkl.gz')
    with gzip.open(vc_pkl) as gzip_fh:
        vc = pickle.load(gzip_fh)
    output_dir = os.path.join(WKDIR, 'performance.out')
    vc_perf = performance.Performance(vc, output_dir,
                                         taxon_dist_optimize=True)

    vc_perf_pkl = os.path.join(WKDIR, 'vc_perf.pkl.gz')
    with gzip.open(vc_perf_pkl, 'wb') as gzip_fh:
        pickle.dump(vc_perf, gzip_fh)

def test_gene_modules():
    pass