import os
import sys
import pytest
import shutil



def pytest_addoption(parser):
    parser.addoption('--database', action='store', 
                     help='Path to vcontact3 database')
    parser.addoption('--threads', default=1, action='store',
                     help='Number of threads to use')


@pytest.fixture
def database(request):
    return request.config.getoption('--database')

@pytest.fixture
def threads(request):
    return request.config.getoption('--threads')


def pytest_sessionfinish(session, exitstatus):
    #shutil.rmtree('output')
    pass


