#!/usr/bin/env python
import os
import sys
import dendropy


def dm2tree(csv, f, fmt='newick'):
    with open(csv) as fh:
        pdm = dendropy.PhylogeneticDistanceMatrix.from_csv(src=fh)
        tree = pdm.upgma_tree()
        tree.write(path=f, schema=fmt)

if __name__ == '__main__':
    if len(sys.argv) != 3:
        mes = '*** Usage: python {} <distmat.csv> <tree.nwk>\n'
        sys.stderr.write(mes.format(sys.argv[0]))
        sys.exit(1)

    csv = sys.argv[1]
    f = sys.argv[2]
    dm2tree(csv, f)
